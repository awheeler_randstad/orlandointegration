﻿Imports System.Text

Public Class OpenInvoiceVersion2

    Private _legalEntity As OpenInvoiceVersion2LegalEntity
    Private _customer As List(Of OpenInvoiceVersion2Customer)
    Private _openInvoiceAmount As List(Of OpenInvoiceVersion2OpenInvoice)
    Private _summary As List(Of OpenInvoiceVersion2Summary)

    'Public Sub New()

    '    _legalEntity = New clsLegalEntity
    '    _customer = New List(Of clsCustomer)
    '    _openInvoiceAmount = New List(Of clsOpenInvoice)
    '    _summary = New List(Of clsSummary)

    'End Sub

    Public FileNumber As String

    Public Property LegalEntity() As OpenInvoiceVersion2LegalEntity
        Get
            Return _legalEntity
        End Get
        Set(ByVal value As OpenInvoiceVersion2LegalEntity)
            _legalEntity = value
        End Set
    End Property

    Public Property Customers() As List(Of OpenInvoiceVersion2Customer)
        Get
            Return _customer
        End Get
        Set(ByVal value As List(Of OpenInvoiceVersion2Customer))
            _customer = value
        End Set
    End Property

    Public Property OpenInvoices() As List(Of OpenInvoiceVersion2OpenInvoice)
        Get
            Return _openInvoiceAmount
        End Get
        Set(ByVal value As List(Of OpenInvoiceVersion2OpenInvoice))
            _openInvoiceAmount = value
        End Set
    End Property

    Public Property Summary() As List(Of OpenInvoiceVersion2Summary)
        Get
            Return _summary
        End Get
        Set(ByVal value As List(Of OpenInvoiceVersion2Summary))
            _summary = value
        End Set
    End Property

End Class

Public Class OpenInvoiceVersion2LegalEntity

    Public SupplierId As String
    Public LegalEntityCode As String
    Public LegalEntityName As String

    Public Function GetDelimetedRecord() As String

        Return String.Format("{0};{1};{2}", SupplierId, LegalEntityCode, LegalEntityName)

    End Function

    Public Function GetHeaderRecord() As String

        Return "SupplierId;LegalEntityCode;LegalEntityName"

    End Function




End Class

Public Class OpenInvoiceVersion2Customer

    Public SupplierId As String
    Public LegalEntityCode As String
    Public CustomerId As String
    Public CustomerInvoiceName As String
    Public CustomerBillingAddress As OpenInvoiceVersion2CustomerAddress

    '    Public CustomerBillingAddressStateProvince As String
    '    Public CustomerBillingAddressPostalCode As String
    '   Public CustomerBillingAddressMunicipality As String
    '  Public CustomerBillingAddressBuildingNumber As String
    ' Public CustomerBillingAddressStreetAddress As String
    Public CustomerContractAddress As OpenInvoiceVersion2CustomerAddress
    '    Public CustomerContractAddressStateProvince As String
    '   Public CustomerContractAddressPostalCode As String
    '  Public CustomerContractAddressMunicipality As String
    ' Public CustomerContractAddressBuildingNumber As String
    'Public CustomerContractAddressStreetAddress As String
    Public CustomerTelephoneNumber As String
    Public CustomerMobileTelephoneNumber As String
    Public CustomerEmailAddress As String
    Public LocalClientIdentifier As String
    Public CustomerLocalRating As String
    Public AssignAbilityStatus As String
    Public PaymentTerm As String
    Public PaymentMethod As String
    Public CustomerLanguageCode As String
    Public CustomerDunningFlow As String
    Public CustomerNote As String
    Public CustomerDetailNote As String
    Public CustomerAdditionalField As String

    Public Function GetDelimetedRecord() As String

        Dim sb As New StringBuilder

        sb.Append(SupplierId)
        sb.Append(IncludeDelimiter(LegalEntityCode))

        sb.Append(IncludeDelimiter(CustomerId))
        sb.Append(IncludeDelimiter(CustomerInvoiceName))

        sb.Append(IncludeDelimiter(CustomerBillingAddress.CountryCode))
        sb.Append(IncludeDelimiter(CustomerBillingAddress.StateProvince))
        sb.Append(IncludeDelimiter(CustomerBillingAddress.PostalCode))
        sb.Append(IncludeDelimiter(CustomerBillingAddress.Municipality))
        sb.Append(IncludeDelimiter(CustomerBillingAddress.BuildingNumber))
        sb.Append(IncludeDelimiter(CustomerBillingAddress.StreetAddress))

        sb.Append(IncludeDelimiter(CustomerContractAddress.CountryCode))
        sb.Append(IncludeDelimiter(CustomerContractAddress.StateProvince))
        sb.Append(IncludeDelimiter(CustomerContractAddress.PostalCode))
        sb.Append(IncludeDelimiter(CustomerContractAddress.Municipality))
        sb.Append(IncludeDelimiter(CustomerContractAddress.BuildingNumber))
        sb.Append(IncludeDelimiter(CustomerContractAddress.StreetAddress))

        sb.Append(IncludeDelimiter(CustomerTelephoneNumber))
        sb.Append(IncludeDelimiter(CustomerMobileTelephoneNumber))
        sb.Append(IncludeDelimiter(CustomerEmailAddress))
        sb.Append(IncludeDelimiter(LocalClientIdentifier))
        sb.Append(IncludeDelimiter(CustomerLocalRating))
        sb.Append(IncludeDelimiter(AssignAbilityStatus))
        sb.Append(IncludeDelimiter(PaymentTerm))
        sb.Append(IncludeDelimiter(PaymentMethod))
        sb.Append(IncludeDelimiter(CustomerLanguageCode))
        sb.Append(IncludeDelimiter(CustomerDunningFlow))
        sb.Append(IncludeDelimiter(CustomerNote))
        sb.Append(IncludeDelimiter(CustomerDetailNote))
        sb.Append(IncludeDelimiter(CustomerAdditionalField))

        Return sb.ToString

    End Function

    Public Function GetHeaderRecord() As String

        Dim sb As New StringBuilder

        sb.Append("SupplierId")
        sb.Append(IncludeDelimiter("LegalEntityCode"))

        sb.Append(IncludeDelimiter("CustomerId"))
        sb.Append(IncludeDelimiter("CustomerInvoiceName"))

        sb.Append(IncludeDelimiter("CustomerBillingAddressCountryCode"))
        sb.Append(IncludeDelimiter("CustomerBillingAddressStateProvince"))
        sb.Append(IncludeDelimiter("CustomerBillingAddressPostalCode"))
        sb.Append(IncludeDelimiter("CustomerBillingAddressMunicipality"))
        sb.Append(IncludeDelimiter("CustomerBillingAddressBuildingNumber"))
        sb.Append(IncludeDelimiter("CustomerBillingAddressStreetAddress"))

        sb.Append(IncludeDelimiter("CustomerContractAddressCountryCode"))
        sb.Append(IncludeDelimiter("CustomerContractAddressStateProvince"))
        sb.Append(IncludeDelimiter("CustomerContractAddressPostalCode"))
        sb.Append(IncludeDelimiter("CustomerContractAddressMunicipality"))
        sb.Append(IncludeDelimiter("CustomerContractAddressBuildingNumber"))
        sb.Append(IncludeDelimiter("CustomerContractAddressStreetAddress"))

        sb.Append(IncludeDelimiter("CustomerTelephoneNumber"))
        sb.Append(IncludeDelimiter("CustomerMobileTelephoneNumber"))
        sb.Append(IncludeDelimiter("CustomerEmailAddress"))
        sb.Append(IncludeDelimiter("LocalClientIdentifier"))
        sb.Append(IncludeDelimiter("CustomerLocalRating"))
        sb.Append(IncludeDelimiter("AssignAbilityStatus"))
        sb.Append(IncludeDelimiter("PaymentTerm"))
        sb.Append(IncludeDelimiter("PaymentMethod"))
        sb.Append(IncludeDelimiter("CustomerLanguageCode"))
        sb.Append(IncludeDelimiter("CustomerDunningFlow"))
        sb.Append(IncludeDelimiter("CustomerNote"))
        sb.Append(IncludeDelimiter("CustomerDetailNote"))
        sb.Append(IncludeDelimiter("CustomerAdditionalField"))

        Return sb.ToString

    End Function

    Private Function IncludeDelimiter(thisValue As String)
        Return ";" + thisValue
    End Function


End Class

Public Class OpenInvoiceVersion2CustomerAddress

    Public CountryCode As String
    Public StateProvince As String
    Public PostalCode As String
    Public Municipality As String
    Public BuildingNumber As String
    Public StreetAddress As String

End Class

Public Class OpenInvoiceVersion2OpenInvoice

    Public SupplierId As String
    Public LegalEntityCode As String
    Public CustomerId As String
    Public InvoiceId As String
    Public InvoiceDescription As String
    Public DocumentDate As Date
    Public OriginalOpenAmount As AmountType
    'Public OriginalOpenAmountCurrency As String
    Public OriginalOpenVATAmount As AmountType
    'Public OriginalOpenVATAmountCurrency As String
    Public OpenInvoiceAmount As AmountType
    'Public OpenInvoiceAmountCurrency As String
    Public OpenInvoiceVATAmount As AmountType
    'Public OpenInvoiceVATAmountCurrency As String
    Public DueDate As Date

    Public SellInvoice As SellInvoiceType
    Public DocumentType As OpenInvoiceDocumentType
    Public ParentGuarentee As String
    Public InvoiceImageFileName As String

    Public OptionalField1 As String
    Public OptionalField2 As String
    Public OptionalField3 As String
    Public OptionalField4 As String
    Public OptionalField5 As String
    Public OptionalField6 As String
    Public OptionalField7 As String
    Public OptionalField8 As String

    Private _inDispute As Boolean = False
    Public Property InDispute() As String
        Get
            Dim retval As String = "false"
            If (_inDispute) Then
                retval = "true"
            End If
            Return retval
        End Get
        Set(ByVal value As String)
            If (value.Trim.ToLower = "true" OrElse value.Trim = "1") Then
                _inDispute = True
            Else
                _inDispute = False
            End If

        End Set
    End Property

    Public Function GetDelimetedRecord() As String

        Dim dateFormat As String = "yyyy-MM-ddZ"

        Dim sb As New StringBuilder

        sb.Append(SupplierId)
        sb.Append(IncludeDelimiter(LegalEntityCode))
        sb.Append(IncludeDelimiter(CustomerId))

        sb.Append(IncludeDelimiter(InvoiceId))
        sb.Append(IncludeDelimiter(InvoiceDescription))
        sb.Append(IncludeDelimiter(DocumentDate.ToString(DateFormat)))
        sb.Append(IncludeDelimiter(OriginalOpenAmount.Value))
        sb.Append(IncludeDelimiter(OriginalOpenAmount.currency))
        sb.Append(IncludeDelimiter(OriginalOpenVATAmount.Value))
        sb.Append(IncludeDelimiter(OriginalOpenVATAmount.currency))
        sb.Append(IncludeDelimiter(OpenInvoiceAmount.Value))
        sb.Append(IncludeDelimiter(OpenInvoiceAmount.currency))
        sb.Append(IncludeDelimiter(OpenInvoiceVATAmount.Value))
        sb.Append(IncludeDelimiter(OpenInvoiceVATAmount.currency))

        sb.Append(IncludeDelimiter(DueDate.ToString(dateFormat)))
        sb.Append(IncludeDelimiter(InDispute))
        sb.Append(IncludeDelimiter(SellInvoice.ToString))
        sb.Append(IncludeDelimiter(DocumentType.ToString))
        sb.Append(IncludeDelimiter(ParentGuarentee))
        sb.Append(IncludeDelimiter(InvoiceImageFileName))

        sb.Append(IncludeDelimiter(OptionalField1))
        sb.Append(IncludeDelimiter(OptionalField2))
        sb.Append(IncludeDelimiter(OptionalField3))
        sb.Append(IncludeDelimiter(OptionalField4))
        sb.Append(IncludeDelimiter(OptionalField5))
        sb.Append(IncludeDelimiter(OptionalField6))
        sb.Append(IncludeDelimiter(OptionalField7))
        sb.Append(IncludeDelimiter(OptionalField8))

        Return sb.ToString

    End Function

    Public Function GetHeaderRecord() As String

        Dim sb As New StringBuilder

        sb.Append("SupplierId")
        sb.Append(IncludeDelimiter("LegalEntityCode"))
        sb.Append(IncludeDelimiter("CustomerId"))

        sb.Append(IncludeDelimiter("InvoiceId"))
        sb.Append(IncludeDelimiter("InvoiceDescription"))
        sb.Append(IncludeDelimiter("DocumentDateTime"))
        sb.Append(IncludeDelimiter("OriginalOpenAmount"))
        sb.Append(IncludeDelimiter("OriginalOpenAmountCurrency"))
        sb.Append(IncludeDelimiter("OriginalOpenVATAmount"))
        sb.Append(IncludeDelimiter("OriginalOpenVATAmountCurrency"))
        sb.Append(IncludeDelimiter("OpenInvoiceAmount"))
        sb.Append(IncludeDelimiter("OpenInvoiceAmountCurrency"))
        sb.Append(IncludeDelimiter("OpenInvoiceVATAmount"))
        sb.Append(IncludeDelimiter("OpenInvoiceVATAmountCurrency"))

        sb.Append(IncludeDelimiter("DueDate"))
        sb.Append(IncludeDelimiter("InDispute"))
        sb.Append(IncludeDelimiter("SellInvoice"))
        sb.Append(IncludeDelimiter("DocumentType"))
        sb.Append(IncludeDelimiter("ParentGuarantee"))
        sb.Append(IncludeDelimiter("InvoiceImageFileName"))

        sb.Append(IncludeDelimiter("OptionalField1"))
        sb.Append(IncludeDelimiter("OptionalField2"))
        sb.Append(IncludeDelimiter("OptionalField3"))
        sb.Append(IncludeDelimiter("OptionalField4"))
        sb.Append(IncludeDelimiter("OptionalField5"))
        sb.Append(IncludeDelimiter("OptionalField6"))
        sb.Append(IncludeDelimiter("OptionalField7"))
        sb.Append(IncludeDelimiter("OptionalField8"))

        Return sb.ToString

    End Function

    Private Function IncludeDelimiter(thisValue As String)
        Return ";" + thisValue
    End Function


End Class

Public Class OpenInvoiceVersion2Summary

    '    Message;SupplierId;CH_001
    'Message;FileNumber;20170923053757
    'Message;MessageId;1223fs5-234dasdas3-3df8sfds
    'Message;MessageTimeStamp;2017-05-05T16:20:34+01:00
    'Message;OpenInvoiceAmountFormatVersion;2.2.0
    'Message;OpenInvoiceAmountDate;2017-05-05T12:00:00Z
    'Total;TotalNoOfOpenInvoices;1
    'Total;TotalAmountOfOpenInvoices;104.20

    'Public Category As String
    'Public Key As String
    'Public Value As String

    Public Sub New(category As String, key As String, value As String)
        _Category = category
        _Key = key
        _Value = value
    End Sub

    Public Function GetDelimetedRecord() As String

        Return String.Format("{0};{1};{2}", _Category, _Key, _Value)

    End Function

    Public Function GetHeaderRecord() As String

        Return "Category;Key;Value"

    End Function

    Private _Category As String
    Public Property Category() As String
        Get
            Return _Category
        End Get
        Set(ByVal value As String)
            _Category = value
        End Set
    End Property

    Private _Key As String
    Public Property Key() As String
        Get
            Return _Key
        End Get
        Set(ByVal value As String)
            _Key = value
        End Set
    End Property

    Private _Value As String
    Public Property Value() As String
        Get
            Return _Value
        End Get
        Set(ByVal value As String)
            _Value = value
        End Set
    End Property
End Class

