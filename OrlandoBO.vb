﻿Imports System.Xml
Imports System.IO
Imports System.Xml.Serialization
Imports System.Text
Imports OrlandoBO.WSOrlandoGateway
Imports System.Net
Imports System.Security.Authentication
Imports System.Text.RegularExpressions

Public Class OrlandoBO
    Inherits MinVesta.FlexiSuite.BOM.AbstractBO

    Dim _ApplicationSystemSettings As Dictionary(Of String, String)
    Dim systemSettings As Dictionary(Of String, String)

    Const _Tls12 As System.Security.Authentication.SslProtocols = DirectCast(&HC00, SslProtocols)
    Const Tls12 As SecurityProtocolType = DirectCast(_Tls12, SecurityProtocolType)



#Region "Contructors"

    Public Sub New()

        ' do nothing here
        _ApplicationSystemSettings = New Dictionary(Of String, String)


    End Sub

    Public Sub New(appSystemSettings As Dictionary(Of String, String))

        _ApplicationSystemSettings = New Dictionary(Of String, String)

        _ApplicationSystemSettings.Add("OrlandoGateway", appSystemSettings("OrlandoGateway"))
        _ApplicationSystemSettings.Add("OrlandoSoapService", appSystemSettings("OrlandoSoapService"))
        _ApplicationSystemSettings.Add("OrlandoUserName", appSystemSettings("OrlandoUserName"))
        _ApplicationSystemSettings.Add("OrlandoPassword", appSystemSettings("OrlandoPassword"))

        'This is de login for the orlando documentation:
        'randstad_au
        'i@6Jaw32)NQNkaS=UD

    End Sub

#End Region


    Sub test()

        ' x.OrderClassification.orderStatus = randstadorlando.StaffingOrderTypeOrderClassificationOrderStatus.new

        'x.Header.Description(0).Value()
        'x.ReferenceInformation.UserArea.ReferenceInformationAdditional.  . 



    End Sub

#Region "StaffingOrderType"

    Public Function UpdateVacancyToOrlando() As String

        Return UpdateVacancyToOrlando(BOMObjectLibrary("ENTITYXML"))

    End Function

    Public Function UpdateVacancyToOrlando(EntityXML As String) As String     'string username, string password, string emailaddress, Guid CRMSecurityKey, string hrXMLVersion, int iProfileId)

        ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        Dim ds = New DataSet()
        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '      // read it
        ds.ReadXml(textReader)

        Dim drOrderDetails As DataRow = Nothing
        Dim warningString = New StringBuilder()


        Dim _StaffingOrderType As New WSOrlandoGateway.StaffingOrderType

        '    '  SELECT 
        'v.OrdDet_OrderId AS OrderID
        ',v.OrdDet_OrderCode AS OrderCode
        ',v.OrdDet_PrimaryClientContactAgenc AS PrimaryClientContactAgencyID
        ',v.OrdDet_ReportToClientContactId AS ReportsToClientContactAgencyID
        ',v.OrdDet_MLOrderCreationReasonCode AS OrderReasonCode
        ',v.OrdReq_OriginalPositionsRequeste AS NumberOfPositionRequested 
        ',v.OrdDet_MLWorkTypeCode AS WorkTypeCode
        ',v.JobTitle_Description AS JobTitle
        ',v.OrdDet_JobDescription AS JobDescription
        ',cca.ClientContactAgencyId AS ClientContactAgencyCode
        ',cca.ClientContact_FullName AS ClientContactFullName
        ',cca.ClientContact_FirstName AS ClientContactFirstName
        ',cca.ClientContact_Surname AS ClientContactSurname
        ',rca.ClientContactAgencyId AS ReportsToClientContactAgencyCode
        ',rca.ClientContact_FullName AS ReportsToClientContactFullName
        ',rca.ClientContact_FirstName AS ReportsToClientContactFirstName
        ',rca.ClientContact_Surname AS ReportsToClientContactSurname
        ',csl.Consultant_FullName AS ConsultantFullName
        ',csl.Consultant_FirstName AS ConsultantFirstName
        ',csl.Consultant_MiddleName AS ConsultantMiddleName
        ',csl.Consultant_Surname AS ConsultantSurname
        ',csl.Consultant_KnownAs AS ConsultantKnow

        '      ,'AU_001' AS StaffingSupplierID
        ','05' AS StaffingSupplierOtgUnitID
        ','Randstad' AS UserAreaLabel
        ','AU' AS UserAreaCountryCode
        ','Staffing' AS UserAreaServiceConcept

        '      ,cca.ClientAgencyId AS ClientAgencyID
        ',cca.ClientId AS ClientID


        ',v.OrdReq_ExpectedEndDate AS ExpectedEndDate
        ',v.OrdReq_ExpectedStartDate ExpectedStartDate
        ',v.OrdReq_OrderRequestDate AS OrderRequestDate


        systemSettings = GetSystemSettings("SUBSCRIBER")

        Try
            drOrderDetails = ds.Tables(0).Rows(0)

            ' now set all the properties from the dataset
            With _StaffingOrderType

                ' the known mandatory items
                .OrderId = CreateEntityIdType(drOrderDetails, "OrderRequestCode") ' "OrderRequestCode")
                .PositionQuantity = GetStringFromCell(drOrderDetails, "NumberOfPositionRequested")
                .MultiVendorDistribution = False


                ' and the ReferenceInformation
                .ReferenceInformation = New WSOrlandoGateway.StaffingOrderTypeReferenceInformation
                With .ReferenceInformation

                    .MasterOrderId = CreateEntityIdType(drOrderDetails, "OrderCode")

                    .StaffingCustomerId = CreateEntityIdType(drOrderDetails, "ClientCode")

                    .StaffingCustomerOrgUnitId = CreateEntityIdType(drOrderDetails, "ClientAgencyCode")

                    .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drOrderDetails, "CountryDesc")))
                    .StaffingSupplierOrgUnitId = CreateEntityIdType(drOrderDetails, "StaffingSupplierOrgUnitID")

                    .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
                    .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
                    .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drOrderDetails, "UserAreaLabel")
                    .UserArea.ReferenceInformationAdditional.CountryCode = ConvertCountryToCode(GetStringFromCell(drOrderDetails, "CountryDesc"))

                    If GetStringFromCell(drOrderDetails, "WorkTypeCode").ToLower = "temp" Then
                        .UserArea.ReferenceInformationAdditional.ServiceConcept = GetConceptArea(GetStringFromCell(drOrderDetails, "UserAreaServiceConcept"))
                    Else
                        .UserArea.ReferenceInformationAdditional.ServiceConcept = WSOrlandoGateway.ServiceConceptType.SearchSelection
                    End If

                    .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.StatusReferenceInformationAdditionalTypeMessageDetails
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageId = GetStringFromCell(drOrderDetails, "ReferenceMessageID")
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageTimestamp = GetDateFromCell(drOrderDetails, "ReferenceMessageDate")



                End With

                ' and the CustomerReportingRequirements
                .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType

                With .CustomerReportingRequirements

                    .ManagerName = GetStringFromCell(drOrderDetails, "ClientContactFullName")
                    .SupervisorName = GetStringFromCell(drOrderDetails, "ReportsToClientContactFullName")

                    .CustomerReferenceNumber = GetStringFromCell(drOrderDetails, "CustomerReferenceNumber")
                    .DepartmentCode = GetStringFromCell(drOrderDetails, "DepartmentCode")
                    .DepartmentName = GetStringFromCell(drOrderDetails, "DepartmentName")
                    .CostCenterCode = GetStringFromCell(drOrderDetails, "CostCenterCode")
                    .CostCenterName = GetStringFromCell(drOrderDetails, "CostCenterName")
                    .ProjectCode = GetStringFromCell(drOrderDetails, "ProjectCode")

                End With

                ' and OrderClassification
                .OrderClassification = New WSOrlandoGateway.StaffingOrderTypeOrderClassification
                With .OrderClassification
                    ' we need to be able to identify new vs and update and closed
                    .orderType = "RFQ"

                    ' we need to detemrine if the record has been sent once before... the only way I know this is by the integration date.
                    If GetLongFromCell(drOrderDetails, "OpenVacancies") = 0 Then
                        .orderStatus = WSOrlandoGateway.StaffingOrderTypeOrderClassificationOrderStatus.closed
                    Else
                        If GetDateFromCell(drOrderDetails, "IntegrationDate") = Date.MinValue Then
                            .orderStatus = WSOrlandoGateway.StaffingOrderTypeOrderClassificationOrderStatus.new
                        Else
                            ' original code
                            '.orderStatus = WSOrlandoGateway.StaffingOrderTypeOrderClassificationOrderStatus.xvacancy
                            ' revised status from the testing document released on 29/7/2014
                            .orderStatus = WSOrlandoGateway.StaffingOrderTypeOrderClassificationOrderStatus.revised
                        End If
                    End If



                End With

                '' and OrderContact
                .OrderContact = New WSOrlandoGateway.StaffingOrderTypeOrderContact
                With .OrderContact
                    .contactType = "created by"
                    .ContactInfo = New WSOrlandoGateway.ContactInfoType
                    .ContactInfo.PersonName = New WSOrlandoGateway.PersonNameType
                    .ContactInfo.PersonName.FormattedName = GetStringFromCell(drOrderDetails, "ConsultantFullName")
                    .ContactInfo.ContactId = CreateEntityIdType(drOrderDetails, "OwningPersonalityID")

                    .ContactInfo.ContactMethod = New WSOrlandoGateway.ContactMethodType
                    .ContactInfo.ContactMethod.InternetEmailAddress = GetStringFromCell(drOrderDetails, "ConsultantEmail")

                End With


                ' and StaffingPosition
                .StaffingPosition = New WSOrlandoGateway.StaffingPositionType
                With .StaffingPosition
                    .PositionReason = WSOrlandoGateway.StaffingPositionTypePositionReason.Other


                    .PositionHeader = New WSOrlandoGateway.StaffingPositionHeaderType
                    .PositionHeader.PositionId = New WSOrlandoGateway.PositionIdentifierType
                    .PositionHeader.PositionId.Id = GetStringFromCell(drOrderDetails, "JobTitleCode")
                    .PositionHeader.PositionTitle = GetStringFromCell(drOrderDetails, "JobTitle")

                    Select Case GetStringFromCell(drOrderDetails, "WorkTypeCode").ToLower
                        Case Is = "temp"
                            .PositionHeader.PositionType = "temporary staffing"
                        Case Is = "perm"
                            .PositionHeader.PositionType = "recruitment and selection"
                        Case Else
                    End Select

                    .PositionHeader.PositionDescription = ConvertRTFoText(GetStringFromCell(drOrderDetails, "JobDescription"))

                    .PositionDateRange = New WSOrlandoGateway.StaffingPositionTypePositionDateRange

                    .PositionDateRange.StartDate = GetDateFromCell(drOrderDetails, "ExpectedStartDate")
                    .PositionDateRange.ExpectedEndDate = GetDateFromCell(drOrderDetails, "ExpectedEndDate")
                    .PositionDateRange.ExpectedEndDateSpecified = IsCellValueSet(drOrderDetails, "ExpectedEndDate")

                    .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType

                    .CustomerReportingRequirements.ManagerName = GetStringFromCell(drOrderDetails, "ClientContactFullName")
                    .CustomerReportingRequirements.SupervisorName = GetStringFromCell(drOrderDetails, "ReportsToClientContactFullName")

                    .CustomerReportingRequirements.CustomerReferenceNumber = GetStringFromCell(drOrderDetails, "CustomerReferenceNumber")
                    .CustomerReportingRequirements.DepartmentCode = GetStringFromCell(drOrderDetails, "DepartmentCode")
                    .CustomerReportingRequirements.DepartmentName = GetStringFromCell(drOrderDetails, "DepartmentName")
                    .CustomerReportingRequirements.CostCenterCode = GetStringFromCell(drOrderDetails, "CostCenterCode")
                    .CustomerReportingRequirements.CostCenterName = GetStringFromCell(drOrderDetails, "CostCenterName")
                    .CustomerReportingRequirements.ProjectCode = GetStringFromCell(drOrderDetails, "ProjectCode")


                    .WorkSite = New WSOrlandoGateway.StaffingWorkSiteType

                    If GetStringFromCell(drOrderDetails, "SiteAddressSuburb") = String.Empty Then
                        If GetStringFromCell(drOrderDetails, "PostalAddressSuburb") = String.Empty Then
                            Throw New Exception(String.Format("Warning: There is no Address data for the order {0}", GetStringFromCell(drOrderDetails, "OrderCode")))
                        Else
                            .WorkSite.WorkSiteName = GetStringFromCell(drOrderDetails, "PostalAddressSuburb")
                            .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressLine1"),
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressLine2"),
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressLine3"),
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressSuburb"),
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressState"),
                                                                                               GetStringFromCell(drOrderDetails, "PostalAddressPostCode"),
                                                                                               ConvertCountryToCode(GetStringFromCell(drOrderDetails, "PostalAddressCountry")))
                        End If
                    Else
                        .WorkSite.WorkSiteName = GetStringFromCell(drOrderDetails, "SiteAddressSuburb")
                        .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressLine1"),
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressLine2"),
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressLine3"),
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressSuburb"),
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressState"),
                                                                                           GetStringFromCell(drOrderDetails, "SiteAddressPostCode"),
                                                                                           ConvertCountryToCode(GetStringFromCell(drOrderDetails, "SiteAddressCountry")))
                    End If

                    .StaffingShift = New WSOrlandoGateway.StaffingShiftType
                    .StaffingShift.Id = CreateEntityIdType(drOrderDetails, "StaffingShiftType")
                    .StaffingShift.Hours = GetLongFromCell(drOrderDetails, "StaffingShiftHours")


                End With

                ' and UserArea
                .UserArea = New WSOrlandoGateway.UserAreaStaffingOrderType
                With .UserArea
                    .StaffingOrderAdditional = New WSOrlandoGateway.StaffingOrderAdditionalType


                    If GetDateFromCell(drOrderDetails, "OrderRequestDate") > GetDateFromCell(drOrderDetails, "ExpectedStartDate") Then

                        .StaffingOrderAdditional.OrderDate = FormatOrlandoDate(GetDateFromCell(drOrderDetails, "ExpectedStartDate"))
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString
                    Else
                        .StaffingOrderAdditional.OrderDate = FormatOrlandoDate(GetDateFromCell(drOrderDetails, "OrderRequestDate"))
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString

                    End If
                    '//.StaffingOrderAdditional.OrderDate = FormatOrlandoDate(GetDateFromCell(drOrderDetails, "OrderRequestDate"))

                    .StaffingOrderAdditional.CustomerName = GetStringFromCell(drOrderDetails, "ClientName")

                    .StaffingOrderAdditional.CustomerPostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressLine1"),
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressLine2"),
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressLine3"),
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressSuburb"),
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressState"),
                                                                                            GetStringFromCell(drOrderDetails, "SiteAddressPostCode"),
                                                                                            ConvertCountryToCode(GetStringFromCell(drOrderDetails, "SiteAddressCountry")))



                End With

            End With

            ' once we are all done convert it to a string
            Dim StaffingOrderTypeXML As String = ConvertObjectToXML(_StaffingOrderType)

            ' connect to Orlando Gateway
            'Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
            ' connect to Orlando Gateway
            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drOrderDetails, "ConnectionUserName"), GetStringFromCell(drOrderDetails, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessStaffingOrder(_StaffingOrderType)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdateVacancyToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in UpdateVacancyToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in UpdateVacancyToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval

    End Function

#End Region

#Region "HumanResopurceType"

    Public Function UpdateHumanResourceToOrlando() As String

        Return UpdateHumanResourceToOrlando(BOMObjectLibrary("ENTITYXML"))

    End Function

    Public Function UpdateHumanResourceToOrlando(EntityXML As String) As String

        Dim _retval As String = String.Empty

        ' this method uses the more traditional integration process
        Dim ds = New DataSet()
        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '      // read it
        ds.ReadXml(textReader)

        Dim drCandidateDetails As DataRow = Nothing
        Dim warningString = New StringBuilder()

        Dim _HumanResourceType As New WSOrlandoGateway.HumanResourceType

        '      SELECT TOP 1
        'c.CandidateId AS CandidateID
        ',c.CandidateCode AS CandidateCode
        ',c.Candidate_Email AS EmailAddress
        ',c.DateOfBirth AS DateOfBirth
        ',c.FullName AS FullName
        ',c.FirstName AS FirstName
        ',c.MiddleName AS MiddleName
        ',c.Surname AS Surname
        ',c.KnownAs AS KnownAs
        ',c.MLSalutationCode AS Salutation
        ',c.MLGenderCode AS Gender
        ',c.PermAdr_AddressLine1 AS PermAddressLine1
        ',c.PermAdr_AddressLine2 AS PermAddressLine2
        ',c.PermAdr_AddressLine3  AS PermAddressLine3
        ',c.PermAdr_Suburb AS  PermSuburb
        ',c.PermAdr_State AS PermState
        ',c.PermAdr_Zipcode AS PermPostcode
        ',c.PermAdr_Country AS PermCountry
        ',c.CurrentAdr_AddressLine1 AS CurrentAddressLine1 
        ',c.CurrentAdr_AddressLine2 AS CurrentAddressLine2
        ',c.CurrentAdr_AddressLine3 AS CurrentAddressLine3
        ',c.CurrentAdr_Suburb AS CurrentSuburb
        ',c.CurrentAdr_State AS CurrentState
        ',c.CurrentAdr_Zipcode AS CurrentPostcode 
        ',c.CurrentAdr_Country AS CurrentCountry 
        ','AU_001' AS StaffingSupplierID
        ','05' AS StaffingSupplierOrgUnitID
        ','Randstad' AS UserAreaLabel
        ','AU' AS UserAreaCountryCode
        ','Staffing' AS UserAreaServiceConcept
        ',c.CreatedOn 
        'FROM EDW.dbo.D_Candidate c
        'WHERE c.CandidateId = @lCandidateID 

        systemSettings = GetSystemSettings("SUBSCRIBER")

        Try
            drCandidateDetails = ds.Tables(0).Rows(0)

            ' now set all the properties from the dataset
            With _HumanResourceType

                ' the known mandatory items
                .HumanResourceId = CreateEntityIdType(drCandidateDetails, "HumanResourceID") ' "OrderRequestCode")

                .HumanResourceStatus = New WSOrlandoGateway.HumanResourceTypeHumanResourceStatus
                .HumanResourceStatus.status = GetStringFromCell(drCandidateDetails, "ApplicantStatus")

                .Profile = ""
                'Dim _Preferences As String()
                .Preferences = ""  ' _Preferences

                ' and the ReferenceInformation
                .ReferenceInformation = New WSOrlandoGateway.HumanResourceTypeReferenceInformation
                With .ReferenceInformation

                    .StaffingCustomerId = CreateEntityIdType(drCandidateDetails, "ClientCode")
                    .StaffingCustomerOrgUnitId = CreateEntityIdType(drCandidateDetails, "ClientAgencyCode")

                    .OrderId = CreateEntityIdType(drCandidateDetails, "OrderRequestCode") ' "OrderRequestCode")

                    .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drCandidateDetails, "CountryDesc")))
                    .StaffingSupplierOrgUnitId = CreateEntityIdType(drCandidateDetails, "StaffingSupplierOrgUnitID")

                    .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
                    .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
                    .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drCandidateDetails, "UserAreaLabel")
                    .UserArea.ReferenceInformationAdditional.CountryCode = ConvertCountryToCode(GetStringFromCell(drCandidateDetails, "CountryDesc"))

                    .UserArea.ReferenceInformationAdditional.ServiceConcept = GetConceptArea(GetStringFromCell(drCandidateDetails, "UserAreaServiceConcept"))

                    .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.StatusReferenceInformationAdditionalTypeMessageDetails
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageId = GetStringFromCell(drCandidateDetails, "ReferenceMessageID")
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageTimestamp = GetDateFromCell(drCandidateDetails, "ReferenceMessageDate")

                End With

                .ResourceInformation = New WSOrlandoGateway.HumanResourceTypeResourceInformation
                With .ResourceInformation

                    .PersonName = CreatePersonNameType(GetStringFromCell(drCandidateDetails, "FirstName"),
                                                       GetStringFromCell(drCandidateDetails, "MiddleName"),
                                                       GetStringFromCell(drCandidateDetails, "Surname"),
                                                       GetStringFromCell(drCandidateDetails, "KnownAs"),
                                                       GetStringFromCell(drCandidateDetails, "Salutation"))


                    .EntityContactInfo = CreateContactMethodType(GetStringFromCell(drCandidateDetails, "EmailAddress"),
                                                                 GetStringFromCell(drCandidateDetails, "PhoneNumber"),
                                                                GetStringFromCell(drCandidateDetails, "MobileNumber"))



                    .PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressLine1"),
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressLine2"),
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressLine3"),
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressSuburb"),
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressState"),
                                                                                            GetStringFromCell(drCandidateDetails, "CurrentAddressPostCode"),
                                                                                            ConvertCountryToCode(GetStringFromCell(drCandidateDetails, "CurrentAddressCountry")))


                End With

                ' and UserArea
                .UserArea = New WSOrlandoGateway.UserAreaHumanResourceType
                With .UserArea
                    .HumanResourceAdditional = New WSOrlandoGateway.HumanResourceAdditionalType

                    Select Case GetStringFromCell(drCandidateDetails, "Gender").ToLower
                        Case Is = "male"
                            .HumanResourceAdditional.Gender = WSOrlandoGateway.GenderType.male
                        Case Is = "female"
                            .HumanResourceAdditional.Gender = WSOrlandoGateway.GenderType.female
                        Case Else
                            .HumanResourceAdditional.Gender = WSOrlandoGateway.GenderType.unknown
                    End Select
                    .HumanResourceAdditional.GenderSpecified = True


                    If GetDateFromCell(drCandidateDetails, "DateOfBirth") > DateTime.MinValue Then
                        .HumanResourceAdditional.BirthDate = GetDateFromCell(drCandidateDetails, "DateOfBirth")
                        .HumanResourceAdditional.BirthDateSpecified = True
                    End If

                    .HumanResourceAdditional.RegistrationDate = GetDateFromCell(drCandidateDetails, "CreatedOn") '   FormatOrlandoDate(DateAndTime.Now)
                    .HumanResourceAdditional.RegistrationDateSpecified = True

                End With

            End With


            ' once we are all done convert it to a string
            Dim HumanResourceTypeXML As String = ConvertObjectToXML(_HumanResourceType)

            ' connect to Orlando Gateway
            ' Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings )
            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drCandidateDetails, "ConnectionUserName"), GetStringFromCell(drCandidateDetails, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessHumanResource(_HumanResourceType)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString

                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdateHumanResourceToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in UpdateHumanResourceToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in UpdateHumanResourceToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval

    End Function

#End Region

#Region "AssignmentType"

    Public Function UpdateBookingToOrlando() As String

        Return UpdateBookingToOrlando(BOMObjectLibrary("ENTITYXML"))

    End Function

    Public Function UpdateBookingToOrlando(EntityXML As String) As String

        ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        Dim ds = New DataSet()
        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '      // read it
        ds.ReadXml(textReader)

        Dim drAssignmentDetails As DataRow = Nothing
        Dim warningString = New StringBuilder()

        Dim _AssignmentType As New WSOrlandoGateway.AssignmentType

        systemSettings = GetSystemSettings("SUBSCRIBER")

        Try
            drAssignmentDetails = ds.Tables(0).Rows(0)

            ' now set all the properties from the dataset
            With _AssignmentType

                ' the known mandatory items
                .AssignmentId = CreateEntityIdType(drAssignmentDetails, "AssignmentID") ' "OrderRequestCode")
                .assignmentStatus = "active"

                ' and the ReferenceInformation
                .ReferenceInformation = New WSOrlandoGateway.AssignmentTypeReferenceInformation
                With .ReferenceInformation

                    Dim _OrderIDList(0) As WSOrlandoGateway.EntityIdType
                    _OrderIDList(0) = CreateSimpleEntityIdType(GetStringFromCell(drAssignmentDetails, "OrderRequestCode"))
                    .OrderId = _OrderIDList

                    '  .MasterOrderId = CreateEntityIdType(drOrderDetails, "OrderCode")

                    .StaffingCustomerId = CreateEntityIdType(drAssignmentDetails, "ClientCode")

                    .StaffingCustomerOrgUnitId = CreateEntityIdType(drAssignmentDetails, "ClientAgencyCode")

                    .HumanResourceId = CreateEntityIdType(drAssignmentDetails, "HumanResourceID")

                    Dim _PositionIDList(0) As WSOrlandoGateway.EntityIdType
                    _PositionIDList(0) = CreateSimpleEntityIdType(GetStringFromCell(drAssignmentDetails, "OrderRequestCode"))
                    _PositionIDList(0) = CreateSimpleEntityIdType(GetStringFromCell(drAssignmentDetails, "JobTitleCode"))
                    .PositionId = _PositionIDList

                    .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drAssignmentDetails, "CountryDesc")))
                    .StaffingSupplierOrgUnitId = CreateEntityIdType(drAssignmentDetails, "StaffingSupplierOrgUnitID")

                    .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
                    .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
                    .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drAssignmentDetails, "UserAreaLabel")
                    .UserArea.ReferenceInformationAdditional.CountryCode = ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "CountryDesc"))

                    .UserArea.ReferenceInformationAdditional.ServiceConcept = GetConceptArea(GetStringFromCell(drAssignmentDetails, "UserAreaServiceConcept"))


                    .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.StatusReferenceInformationAdditionalTypeMessageDetails
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageId = GetStringFromCell(drAssignmentDetails, "ReferenceMessageID")
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageTimestamp = GetDateFromCell(drAssignmentDetails, "ReferenceMessageDate")



                End With

                ' and the CustomerReportingRequirements
                .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType

                With .CustomerReportingRequirements

                    .ManagerName = GetStringFromCell(drAssignmentDetails, "ClientContactFullName")
                    .SupervisorName = GetStringFromCell(drAssignmentDetails, "ReportsToClientContactFullName")

                    .CustomerReferenceNumber = GetStringFromCell(drAssignmentDetails, "CustomerReferenceNumber")
                    .DepartmentCode = GetStringFromCell(drAssignmentDetails, "DepartmentCode")
                    .DepartmentName = GetStringFromCell(drAssignmentDetails, "DepartmentName")
                    .CostCenterCode = GetStringFromCell(drAssignmentDetails, "CostCenterCode")
                    .CostCenterName = GetStringFromCell(drAssignmentDetails, "CostCenterName")
                    .ProjectCode = GetStringFromCell(drAssignmentDetails, "ProjectCode")

                End With


                Dim _RatesList As New List(Of WSOrlandoGateway.RatesType)
                '' so the currency needs to be determine on country, its all got to be hourly
                If ds.Tables(1).Rows.Count > 0 Then
                    For Each _Row As DataRow In ds.Tables(1).Rows
                        _RatesList.Add(CreateRateType("pay", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", Convert.ToDouble(_Row("PayRate")), "time interval", _Row("EffectiveDate")))
                        _RatesList.Add(CreateRateType("bill", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", Convert.ToDouble(_Row("ChargeRate")), "time interval", _Row("EffectiveDate")))
                    Next
                Else
                    _RatesList.Add(CreateRateType("pay", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", 0, "time interval", GetDateFromCell(drAssignmentDetails, "ExpectedStartDate")))
                    _RatesList.Add(CreateRateType("bill", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", 0, "time interval", GetDateFromCell(drAssignmentDetails, "ExpectedStartDate")))
                End If

                .Rates = _RatesList.ToArray

                'Dim _RatesList(1) As WSOrlandoGateway.RatesType
                '_RatesList(0) = CreateRateType("pay", "AUD", "hourly", 25.0, "time interval", DateAndTime.Now())
                '_RatesList(1) = CreateRateType("bill", "AUD", "hourly", 35.0, "time interval", DateAndTime.Now())

                '.Rates = _RatesList



                .StaffingShift = New WSOrlandoGateway.StaffingShiftType
                .StaffingShift.Id = CreateEntityIdType(drAssignmentDetails, "StaffingShiftType")
                .StaffingShift.Hours = GetLongFromCell(drAssignmentDetails, "StaffingShiftHours")

                .AssignmentDateRange = New WSOrlandoGateway.AssignmentTypeAssignmentDateRange

                .AssignmentDateRange.StartDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").ToString

                If GetDateFromCell(drAssignmentDetails, "ExpectedEndDate") < GetDateFromCell(drAssignmentDetails, "ExpectedStartDate") Then
                    .AssignmentDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").AddDays(1).ToString
                    .AssignmentDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                Else
                    .AssignmentDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedEndDate").ToString
                    .AssignmentDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                End If



                .AssignmentDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedEndDate").ToString
                .AssignmentDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")

                '               .AssignmentDateRange.ActualEndDate = GetDateFromCell(drAssignmentDetails, "ActualEndDate").ToString
                '            .AssignmentDateRange.ActualEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ActualEndDate")


                .UserArea = New WSOrlandoGateway.UserAreaAssignmentType

                With .UserArea

                    .AssignmentAdditional = New WSOrlandoGateway.AssignmentAdditionalType
                    .AssignmentAdditional.StaffingPosition = New WSOrlandoGateway.StaffingPositionType


                    If GetDateFromCell(drAssignmentDetails, "LastUpdatedOn") > GetDateFromCell(drAssignmentDetails, "ExpectedStartDate") Then

                        .AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").ToString
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString
                    Else
                        .AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LastUpdatedOn").ToString
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString

                    End If

                    .AssignmentAdditional.InclusiveRate = True
                    .AssignmentAdditional.InvoiceFrequency = WSOrlandoGateway.AssignmentAdditionalTypeInvoiceFrequency.Weekly


                    With .AssignmentAdditional

                        With .StaffingPosition
                            .PositionHeader = New WSOrlandoGateway.StaffingPositionHeaderType
                            .PositionHeader.PositionId = New WSOrlandoGateway.PositionIdentifierType
                            .PositionHeader.PositionId.Id = GetStringFromCell(drAssignmentDetails, "JobTitleCode")
                            .PositionHeader.PositionTitle = GetStringFromCell(drAssignmentDetails, "JobTitle")
                            .PositionHeader.PositionType = "temporary staffing"

                            .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType
                            .CustomerReportingRequirements.ManagerName = GetStringFromCell(drAssignmentDetails, "ClientContactFullName")
                            .CustomerReportingRequirements.SupervisorName = GetStringFromCell(drAssignmentDetails, "ReportsToClientContactFullName")
                            .CustomerReportingRequirements.CustomerReferenceNumber = GetStringFromCell(drAssignmentDetails, "CustomerReferenceNumber")
                            .CustomerReportingRequirements.DepartmentCode = GetStringFromCell(drAssignmentDetails, "DepartmentCode")
                            .CustomerReportingRequirements.DepartmentName = GetStringFromCell(drAssignmentDetails, "DepartmentName")
                            .CustomerReportingRequirements.CostCenterCode = GetStringFromCell(drAssignmentDetails, "CostCenterCode")
                            .CustomerReportingRequirements.CostCenterName = GetStringFromCell(drAssignmentDetails, "CostCenterName")
                            .CustomerReportingRequirements.ProjectCode = GetStringFromCell(drAssignmentDetails, "ProjectCode")

                            .PositionReason = New WSOrlandoGateway.StaffingPositionTypePositionReason
                            .PositionReason = WSOrlandoGateway.StaffingPositionTypePositionReason.Other

                            .PositionDateRange = New WSOrlandoGateway.StaffingPositionTypePositionDateRange
                            .PositionDateRange.StartDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").ToString

                            If GetDateFromCell(drAssignmentDetails, "ExpectedEndDate") < GetDateFromCell(drAssignmentDetails, "ExpectedStartDate") Then
                                .PositionDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").AddDays(1).ToString
                                .PositionDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                            Else
                                .PositionDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedEndDate").ToString
                                .PositionDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                            End If

                            .WorkSite = New WSOrlandoGateway.StaffingWorkSiteType

                            If GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb") = String.Empty Then
                                If GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb") = String.Empty Then
                                    Throw New Exception(String.Format("Warning: There is no Address data for the order {0}", GetStringFromCell(drAssignmentDetails, "OrderCode")))
                                Else
                                    .WorkSite.WorkSiteName = GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb")
                                    .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine1"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine2"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine3"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressState"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressPostCode"),
                                                                                                       ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "PostalAddressCountry")))
                                End If
                            Else
                                .WorkSite.WorkSiteName = GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb")
                                .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine1"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine2"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine3"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressState"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressPostCode"),
                                                                                                   ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "SiteAddressCountry")))
                            End If

                            .StaffingShift = New WSOrlandoGateway.StaffingShiftType
                            .StaffingShift.Id = CreateEntityIdType(drAssignmentDetails, "StaffingShiftType")
                            .StaffingShift.Hours = GetLongFromCell(drAssignmentDetails, "StaffingShiftHours")

                        End With
                    End With

                End With
            End With

            Dim _AssignmentRequestElement As New WSOrlandoGateway.AssignmentRequestElement
            _AssignmentRequestElement.Assignment = _AssignmentType

            ' once we are all done convert it to a string
            Dim _AssignmentRequestElementXML As String = ConvertObjectToXML(_AssignmentRequestElement)

            ' connect to Orlando Gateway
            ' Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
            ' connect to Orlando Gateway
            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drAssignmentDetails, "ConnectionUserName"), GetStringFromCell(drAssignmentDetails, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessAssignment(_AssignmentRequestElement)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdateBookingToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in UpdateBookingToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in UpdateBookingToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval

    End Function

    Public Function UpdatePlacementToOrlando() As String

        Return UpdatePlacementToOrlando(BOMObjectLibrary("ENTITYXML"))

    End Function

    Public Function UpdatePlacementToOrlando(EntityXML As String) As String

        ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        Dim ds = New DataSet()
        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '      // read it
        ds.ReadXml(textReader)

        Dim drAssignmentDetails As DataRow = Nothing
        Dim warningString = New StringBuilder()

        Dim _AssignmentType As New WSOrlandoGateway.AssignmentType

        systemSettings = GetSystemSettings("SUBSCRIBER")

        Try
            drAssignmentDetails = ds.Tables(0).Rows(0)

            ' now set all the properties from the dataset
            With _AssignmentType

                ' the known mandatory items
                .AssignmentId = CreateEntityIdType(drAssignmentDetails, "AssignmentID") ' "OrderRequestCode")
                .assignmentStatus = "active"

                ' and the ReferenceInformation
                .ReferenceInformation = New WSOrlandoGateway.AssignmentTypeReferenceInformation
                With .ReferenceInformation

                    Dim _OrderIDList(0) As WSOrlandoGateway.EntityIdType
                    _OrderIDList(0) = CreateSimpleEntityIdType(GetStringFromCell(drAssignmentDetails, "OrderRequestCode"))
                    .OrderId = _OrderIDList

                    '  .MasterOrderId = CreateEntityIdType(drOrderDetails, "OrderCode")

                    .StaffingCustomerId = CreateEntityIdType(drAssignmentDetails, "ClientCode")

                    .StaffingCustomerOrgUnitId = CreateEntityIdType(drAssignmentDetails, "ClientAgencyCode")

                    .HumanResourceId = CreateEntityIdType(drAssignmentDetails, "HumanResourceID")

                    Dim _PositionIDList(0) As WSOrlandoGateway.EntityIdType
                    _PositionIDList(0) = CreateSimpleEntityIdType(GetStringFromCell(drAssignmentDetails, "OrderRequestCode"))
                    .PositionId = _PositionIDList

                    .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drAssignmentDetails, "CountryDesc")))
                    .StaffingSupplierOrgUnitId = CreateEntityIdType(drAssignmentDetails, "StaffingSupplierOrgUnitID")

                    .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
                    .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
                    .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drAssignmentDetails, "UserAreaLabel")
                    .UserArea.ReferenceInformationAdditional.CountryCode = ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "CountryDesc"))

                    .UserArea.ReferenceInformationAdditional.ServiceConcept = GetConceptArea(GetStringFromCell(drAssignmentDetails, "UserAreaServiceConcept"))


                    .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.StatusReferenceInformationAdditionalTypeMessageDetails
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageId = GetStringFromCell(drAssignmentDetails, "ReferenceMessageID")
                    .UserArea.ReferenceInformationAdditional.MessageDetails.MessageTimestamp = GetDateFromCell(drAssignmentDetails, "ReferenceMessageDate")



                End With

                ' and the CustomerReportingRequirements
                .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType

                With .CustomerReportingRequirements

                    .ManagerName = GetStringFromCell(drAssignmentDetails, "ClientContactFullName")
                    .SupervisorName = GetStringFromCell(drAssignmentDetails, "ReportsToClientContactFullName")

                    .CustomerReferenceNumber = GetStringFromCell(drAssignmentDetails, "CustomerReferenceNumber")
                    .DepartmentCode = GetStringFromCell(drAssignmentDetails, "DepartmentCode")
                    .DepartmentName = GetStringFromCell(drAssignmentDetails, "DepartmentName")
                    .CostCenterCode = GetStringFromCell(drAssignmentDetails, "CostCenterCode")
                    .CostCenterName = GetStringFromCell(drAssignmentDetails, "CostCenterName")
                    .ProjectCode = GetStringFromCell(drAssignmentDetails, "ProjectCode")

                End With


                Dim _RatesList As New List(Of WSOrlandoGateway.RatesType)

                If ds.Tables(1).Rows.Count > 0 Then
                    For Each _Row As DataRow In ds.Tables(1).Rows
                        _RatesList.Add(CreateRateType("pay", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "yearly", Convert.ToDouble(_Row("PayRate")), "time interval", _Row("EffectiveDate")))
                        '   _RatesList.Add(CreateRateType("bill", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", Convert.ToDouble(_Row("ChargeRate")), "time interval", _Row("EffectiveDate")))
                    Next
                Else
                    _RatesList.Add(CreateRateType("pay", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "yearly", 0, "time interval", GetDateFromCell(drAssignmentDetails, "PlacementStartDate")))
                    ' _RatesList.Add(CreateRateType("bill", ConvertCountryToCurrencyCode(GetStringFromCell(drAssignmentDetails, "CountryDesc")), "hourly", 0, "time interval", GetDateFromCell(drAssignmentDetails, "ExpectedStartDate")))
                End If

                .Rates = _RatesList.ToArray


                '               Dim _RatesList(1) As WSOrlandoGateway.RatesType

                '_RatesList(0) = CreateRateType("pay", "AUD", "yearly", 1.0, "time interval", DateAndTime.Now())
                ''  _RatesList(1) = CreateRateType("bill", "AUD", "hourly", 1.0, "time interval", DateAndTime.Now())

                '.Rates = _RatesList

                .StaffingShift = New WSOrlandoGateway.StaffingShiftType
                .StaffingShift.Id = CreateEntityIdType(drAssignmentDetails, "StaffingShiftType")
                .StaffingShift.Hours = GetLongFromCell(drAssignmentDetails, "StaffingShiftHours")

                .AssignmentDateRange = New WSOrlandoGateway.AssignmentTypeAssignmentDateRange

                .AssignmentDateRange.StartDate = GetDateFromCell(drAssignmentDetails, "PlacementStartDate").ToString
                .AssignmentDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                If .AssignmentDateRange.ExpectedEndDateSpecified Then
                    .AssignmentDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedEndDate").ToString
                End If

                .UserArea = New WSOrlandoGateway.UserAreaAssignmentType


                With .UserArea

                    .AssignmentAdditional = New WSOrlandoGateway.AssignmentAdditionalType
                    .AssignmentAdditional.StaffingPosition = New WSOrlandoGateway.StaffingPositionType

                    If GetDateFromCell(drAssignmentDetails, "LastUpdatedOn") > GetDateFromCell(drAssignmentDetails, "PlacementStartDate") Then

                        .AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "PlacementStartDate").ToString
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString
                    Else
                        .AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LastUpdatedOn").ToString
                        '.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LineItemCreatedOn").ToString

                    End If

                    '//.AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LastUpdatedOn").ToString
                    .AssignmentAdditional.InclusiveRate = True
                    .AssignmentAdditional.InvoiceFrequency = WSOrlandoGateway.AssignmentAdditionalTypeInvoiceFrequency.Weekly


                    With .AssignmentAdditional

                        With .StaffingPosition
                            .PositionHeader = New WSOrlandoGateway.StaffingPositionHeaderType
                            .PositionHeader.PositionId = New WSOrlandoGateway.PositionIdentifierType
                            .PositionHeader.PositionId.Id = GetStringFromCell(drAssignmentDetails, "JobTitleCode")
                            .PositionHeader.PositionTitle = GetStringFromCell(drAssignmentDetails, "JobTitle")
                            .PositionHeader.PositionType = "recruitment and selection"

                            .CustomerReportingRequirements = New WSOrlandoGateway.CustomerReportingRequirementsType
                            .CustomerReportingRequirements.ManagerName = GetStringFromCell(drAssignmentDetails, "ClientContactFullName")
                            .CustomerReportingRequirements.SupervisorName = GetStringFromCell(drAssignmentDetails, "ReportsToClientContactFullName")
                            .CustomerReportingRequirements.CustomerReferenceNumber = GetStringFromCell(drAssignmentDetails, "CustomerReferenceNumber")
                            .CustomerReportingRequirements.DepartmentCode = GetStringFromCell(drAssignmentDetails, "DepartmentCode")
                            .CustomerReportingRequirements.DepartmentName = GetStringFromCell(drAssignmentDetails, "DepartmentName")
                            .CustomerReportingRequirements.CostCenterCode = GetStringFromCell(drAssignmentDetails, "CostCenterCode")
                            .CustomerReportingRequirements.CostCenterName = GetStringFromCell(drAssignmentDetails, "CostCenterName")
                            .CustomerReportingRequirements.ProjectCode = GetStringFromCell(drAssignmentDetails, "ProjectCode")

                            .PositionReason = New WSOrlandoGateway.StaffingPositionTypePositionReason
                            .PositionReason = WSOrlandoGateway.StaffingPositionTypePositionReason.Other

                            .PositionDateRange = New WSOrlandoGateway.StaffingPositionTypePositionDateRange
                            .PositionDateRange.StartDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").ToString

                            If GetDateFromCell(drAssignmentDetails, "ExpectedEndDate") < GetDateFromCell(drAssignmentDetails, "ExpectedStartDate") Then
                                .PositionDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedStartDate").AddDays(1).ToString
                                .PositionDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                            Else
                                .PositionDateRange.ExpectedEndDate = GetDateFromCell(drAssignmentDetails, "ExpectedEndDate").ToString
                                .PositionDateRange.ExpectedEndDateSpecified = IsCellValueSet(drAssignmentDetails, "ExpectedEndDate")
                            End If

                            .WorkSite = New WSOrlandoGateway.StaffingWorkSiteType

                            If GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb") = String.Empty Then
                                If GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb") = String.Empty Then
                                    Throw New Exception(String.Format("Warning: There is no Address data for the order {0}", GetStringFromCell(drAssignmentDetails, "OrderCode")))
                                Else
                                    .WorkSite.WorkSiteName = GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb")
                                    .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine1"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine2"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressLine3"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressSuburb"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressState"),
                                                                                                       GetStringFromCell(drAssignmentDetails, "PostalAddressPostCode"),
                                                                                                       ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "PostalAddressCountry")))
                                End If
                            Else
                                .WorkSite.WorkSiteName = GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb")
                                .WorkSite.PostalAddress = CreatePostalAddressType(WSOrlandoGateway.PostalAddressTypeType.streetAddress,
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine1"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine2"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressLine3"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressSuburb"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressState"),
                                                                                                   GetStringFromCell(drAssignmentDetails, "SiteAddressPostCode"),
                                                                                                   ConvertCountryToCode(GetStringFromCell(drAssignmentDetails, "SiteAddressCountry")))
                            End If

                            .StaffingShift = New WSOrlandoGateway.StaffingShiftType
                            .StaffingShift.Id = CreateEntityIdType(drAssignmentDetails, "StaffingShiftType")
                            .StaffingShift.Hours = GetLongFromCell(drAssignmentDetails, "StaffingShiftHours")

                        End With
                    End With

                    'With .UserArea
                    '    .AssignmentAdditional = New WSOrlandoGateway.AssignmentAdditionalType

                    '    .AssignmentAdditional.AssignmentDate = GetDateFromCell(drAssignmentDetails, "LastUpdatedOn").ToString
                    '    .AssignmentAdditional.InclusiveRate = True
                    '    .AssignmentAdditional.InvoiceFrequency = WSOrlandoGateway.AssignmentAdditionalTypeInvoiceFrequency.Weekly

                    '    .AssignmentAdditional.StaffingPosition = New WSOrlandoGateway.StaffingPositionType
                    '    .AssignmentAdditional.StaffingPosition.PositionHeader = New WSOrlandoGateway.StaffingPositionHeaderType
                    '    .AssignmentAdditional.StaffingPosition.PositionHeader.PositionId = New WSOrlandoGateway.PositionIdentifierType
                    '    .AssignmentAdditional.StaffingPosition.PositionHeader.PositionId.Id = GetStringFromCell(drAssignmentDetails, "JobTitleCode")
                    '    .AssignmentAdditional.StaffingPosition.PositionHeader.PositionTitle = GetStringFromCell(drAssignmentDetails, "JobTitle")







                End With
            End With

            Dim _AssignmentRequestElement As New WSOrlandoGateway.AssignmentRequestElement
            _AssignmentRequestElement.Assignment = _AssignmentType

            ' once we are all done convert it to a string
            Dim _AssignmentRequestElementXML As String = ConvertObjectToXML(_AssignmentRequestElement)

            ' connect to Orlando Gateway
            ' Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
            ' connect to Orlando Gateway
            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drAssignmentDetails, "ConnectionUserName"), GetStringFromCell(drAssignmentDetails, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessAssignment(_AssignmentRequestElement)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdatePlacementToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in UpdatePlacementToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in UpdatePlacementToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval

    End Function


#End Region


#Region "Timecard"

    Public Function UpdateTimecardToOrlando() As String

        Return UpdateTimecardToOrlando(BOMObjectLibrary("ENTITYXML"))

    End Function

    Public Function UpdateTimecardToOrlando(EntityXML As String) As String     'string username, string password, string emailaddress, Guid CRMSecurityKey, string hrXMLVersion, int iProfileId)

        ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        Dim ds = New DataSet()
        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '      // read it
        ds.ReadXml(textReader)

        Dim drTimecardHeader As DataRow = Nothing
        Dim warningString = New StringBuilder()


        Dim _TimeCardType As New WSOrlandoGateway.TimeCardType

        systemSettings = GetSystemSettings("SUBSCRIBER")


        Try
            drTimecardHeader = ds.Tables(0).Rows(0)

            ' now set all the properties from the dataset
            With _TimeCardType

                ' the known mandatory items
                .Id = CreateEntityIdType(drTimecardHeader, "TimesheetCode")

                .ReportedResource = New WSOrlandoGateway.TimeCardTypeReportedResource
                With .ReportedResource
                    .Item = New WSOrlandoGateway.TimeCardPersonType
                    With .Item
                        .Id = CreateEntityIdType(drTimecardHeader, "CandidateCode")
                        .PersonName = CreatePersonNameType(GetStringFromCell(drTimecardHeader, "CandidateFirstName"),
                                                    GetStringFromCell(drTimecardHeader, "CandidateMiddleName"),
                                                    GetStringFromCell(drTimecardHeader, "CandidateSurname"),
                                                    GetStringFromCell(drTimecardHeader, "CandidateKnownAs"),
                                                    GetStringFromCell(drTimecardHeader, "CandidateSalutation"))

                    End With
                End With

                ' this doesnt really make sense to me... but here goes
                ' the reported time is for the timeframe details of the timecard... but its an arrray
                ' the reportedime object has a list of items!!! being the time intervals... this is an array
                Dim _ReportedTimeList As New List(Of WSOrlandoGateway.TimeCardTypeReportedTime)
                Dim _ReportedTimeItem = New WSOrlandoGateway.TimeCardTypeReportedTime

                '                _ReportedTimeItem.Id = CreateEntityIdType(drTimecardHeader, "AssignmentID")
                _ReportedTimeItem.Id = CreateEntityIdType(drTimecardHeader, "TimesheetID", 5)

                _ReportedTimeItem.PeriodStartDate = drTimecardHeader("PeriodStartDate")
                _ReportedTimeItem.PeriodEndDate = drTimecardHeader("PeriodEndDate")

                Dim _TimeIntervalList As New List(Of WSOrlandoGateway.TimeCardTypeReportedTimeTimeInterval)
                Dim _DurationCheck As Double = 0
                '  If ds.Tables(1).Rows.Count > 0 Then
                For Each _Row As DataRow In ds.Tables(1).Rows

                    ' we cant send reversal through... as this is 
                    If GetDoubleFromCell(_Row, "Quantity") <> 0 Then

                        Dim _ReportedTimeIntervalType = CreateReportedTimeIntervalType(GetLongFromCell(_Row, "ProductSummaryID"),
                                                                                GetStringFromCell(_Row, "ProductCode"),
                                                                                GetDateFromCell(_Row, "PaymentDate"),
                                                                                Math.Round(GetDoubleFromCell(_Row, "Quantity"), 2).ToString,
                                                                                GetStringFromCell(_Row, "OrlandoProductCategory"))

                        ' see if we already have somethig for this date
                        Dim _ExisitingReportedTimeIntervalType = _TimeIntervalList.Find(Function(x) x.type = _ReportedTimeIntervalType.type And x.StartDate = _ReportedTimeIntervalType.StartDate)
                        If Not IsNothing(_ExisitingReportedTimeIntervalType) Then
                            _ExisitingReportedTimeIntervalType.Duration += _ReportedTimeIntervalType.Duration
                        Else
                            _TimeIntervalList.Add(_ReportedTimeIntervalType)
                        End If
                        _DurationCheck += _ReportedTimeIntervalType.Duration
                    End If

                Next
                ' End If

                If _DurationCheck = 0 Then ' we have no quantities in the transaction data... we need to use the F_Tansactions table
                    ' but for the moment I'll exception it
                    Throw New Exception(String.Format("Warning: There is no quantity data for the Timesheet {0}", GetStringFromCell(drTimecardHeader, "TimesheetCode")))
                End If

                _ReportedTimeItem.TimeInterval = _TimeIntervalList.ToArray
                '_ReportedTimeItem.Items = _TimeIntervalList.ToArray
                _ReportedTimeList.Add(_ReportedTimeItem)

                .ReportedTime = _ReportedTimeList.ToArray


                ' and the ReferenceInformation
                .UserArea = New WSOrlandoGateway.UserAreaTimeCardType
                With .UserArea
                    .TimeCardReferenceInformation = New WSOrlandoGateway.TimeCardReferenceInformationType
                    With .TimeCardReferenceInformation

                        .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drTimecardHeader, "CountryDesc")))
                        .StaffingCustomerId = CreateEntityIdType(drTimecardHeader, "ClientCode")

                        .AssignmentId = CreateEntityIdType(drTimecardHeader, "AssignmentID")

                        .StaffingSupplierOrgUnitId = CreateEntityIdType(drTimecardHeader, "StaffingSupplierOrgUnitID")

                        '                        .StaffingCustomerOrgUnitId = CreateEntityIdType(drTimecardHeader, "ClientAgencyID")


                        .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
                        .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
                        .UserArea.ReferenceInformationAdditional.CountryCode = ConvertCountryToCode(GetStringFromCell(drTimecardHeader, "CountryDesc"))


                        .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.StatusReferenceInformationAdditionalTypeMessageDetails
                        .UserArea.ReferenceInformationAdditional.MessageDetails.MessageId = GetStringFromCell(drTimecardHeader, "ReferenceMessageID")
                        .UserArea.ReferenceInformationAdditional.MessageDetails.MessageTimestamp = GetDateFromCell(drTimecardHeader, "ReferenceMessageDate")

                        .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drTimecardHeader, "UserAreaLabel")
                        .UserArea.ReferenceInformationAdditional.ServiceConcept = GetConceptArea(GetStringFromCell(drTimecardHeader, "UserAreaServiceConcept"))

                    End With
                End With


            End With

            ' once we are all done convert it to a string
            Dim TimecardTypeXML As String = ConvertObjectToXML(_TimeCardType)

            ' connect to Orlando Gateway
            'Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
            ' connect to Orlando Gateway




            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drTimecardHeader, "ConnectionUserName"), GetStringFromCell(drTimecardHeader, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessTimeCard(_TimeCardType)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdateTimecardToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in UpdateTimecardToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in UpdateTimecardToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval

    End Function

#End Region


#Region "Invoice"

    Public Function UpdateInvoiceToOrlando() As String
        Return UpdateInvoiceToOrlando(BOMObjectLibrary("ENTITYXML"))
    End Function

    Public Function UpdateInvoiceToOrlando(EntityXML As String) As String     'string username, string password, string emailaddress, Guid CRMSecurityKey, string hrXMLVersion, int iProfileId)

        '        ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        '        Dim ds = New DataSet()
        '        Dim textReader = New XmlTextReader(New System.IO.StringReader(EntityXML))
        '        '      // read it
        '        ds.ReadXml(textReader)

        '        Dim drInvoiceHeader As DataRow = Nothing
        '        Dim warningString = New StringBuilder()

        '        Dim _InvoiceType As New WSOrlandoGateway.InvoiceType

        '        systemSettings = GetSystemSettings("SUBSCRIBER")

        '        Try

        '            drInvoiceHeader = ds.Tables(0).Rows(0)

        '            ' now set all the properties from the dataset
        '            With _InvoiceType

        '                ' the known mandatory items
        '                .Header = New WSOrlandoGateway.InvoiceHeaderType

        '                Dim _InvoiceTotal As Double = 0
        '                Dim _TaxTotal As Double = 0
        '                ' get the totals

        '                For Each _row As DataRow In ds.Tables(0).Rows
        '                    _InvoiceTotal += GetDoubleFromCell(_row, "TotalRevenue")
        '                    _TaxTotal += GetDoubleFromCell(_row, "ChargeTax")
        '                Next


        '                With .Header

        '                    '                    No problem, yesterday was a holiday for us anyway ;)
        '                    'I just checked and found that the TaxBaseAmount issue is still open I believe. 
        '                    'Also I noticed with the credit notes that the TaxBaseAmount is negative and the Tax Amount positive. 
        '                    'It should be like this: Quantity = negative, OK
        '                    '                    Price amount = positive
        '                    'Tax Base Amount = negative
        '                    '                    Tax Amount = negative
        '                    'Tax percentage quantity = positive (10%)
        '                    'Could you implement it like this?



        '                    .InvoiceId = CreateEntityIdType(drInvoiceHeader, "InvoiceNumber")


        '                    .DocumentDateTime = Date.ParseExact(GetStringFromCell(drInvoiceHeader, "InvoiceDate"), "yyyyMMdd", Globalization.CultureInfo.InstalledUICulture)
        '                    .TotalCharges = New WSOrlandoGateway.Amount
        '                    .TotalCharges.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(drInvoiceHeader, "CountryDesc"))
        '                    .TotalCharges.Value = Math.Round(_InvoiceTotal, 2)

        '                    .TotalTax = New WSOrlandoGateway.Amount
        '                    .TotalTax.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(drInvoiceHeader, "CountryDesc"))
        '                    .TotalTax.Value = Math.Round(_TaxTotal, 3)

        '                    .DueDate = DateAdd(DateInterval.Day, 14, Convert.ToDateTime(.DocumentDateTime))

        '                    .HeaderReferenceInformation = New WSOrlandoGateway.InvoiceHeaderTypeHeaderReferenceInformation
        '                    With .HeaderReferenceInformation
        '                        .StaffingCustomerId = CreateEntityIdType(drInvoiceHeader, "ClientCode")

        '                        '.StaffingCustomerOrgUnitId = CreateEntityIdType(drInvoiceHeader, "ClientAgencyID")

        '                        .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drInvoiceHeader, "CountryDesc")))

        '                        '. .UserArea = New WSOrlandoGateway.UserAreaReferenceInformationType
        '                        ' .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.ReferenceInformationAdditionalType
        '                        ' .UserArea.ReferenceInformationAdditional.Label = GetStringFromCell(drInvoiceHeader, "UserAreaLabel")
        '                        .CountryCode = ConvertCountryToCode(GetStringFromCell(drInvoiceHeader, "CountryDesc"))


        '                        .MessageDetails = New WSOrlandoGateway.InvoiceHeaderTypeHeaderReferenceInformationMessageDetails
        '                        .MessageDetails.MessageId = GetStringFromCell(drInvoiceHeader, "ReferenceMessageID")
        '                        .MessageDetails.MessageTimestamp = GetDateFromCell(drInvoiceHeader, "ReferenceMessageDate")

        '                    End With

        '                End With

        '                ' an array of line iems now...
        '                '.Line

        '                Dim _LiteItemList As New List(Of WSOrlandoGateway.InvoiceLineType)

        '                For Each _itemrow As DataRow In ds.Tables(0).Rows

        '                    Dim _InvoiceLine As New WSOrlandoGateway.InvoiceLineType

        '                    _InvoiceLine.LineNumber = CreateSimpleEntityIdType(String.Format("{0}_{1}", GetStringFromCell(_itemrow, "SplitID"), GetStringFromCell(drInvoiceHeader, "Consultant_SK")))
        '                    _InvoiceLine.PeriodStartDat = .Header.DocumentDate
        '                    _InvoiceLine.PeriodEndDate = .Header.DocumentDate


        '                    ' AJW... we need to use the ProductCategoryCode
        '                    '/Time, Allowance, Expense, PAYROLL

        '                    ' reason code can only be "services", "hours" or "expenses"
        '                    'Select Case GetStringFromCell(_itemrow, "ProductTypeCode").ToLower
        '                    Select Case GetStringFromCell(_itemrow, "ProductCategoryCode").ToLower
        '                        Case Is = "earnings", "time"
        '                            _InvoiceLine.ReasonCode = "hours"
        '                        Case Is = "allowance", "expense"
        '                            _InvoiceLine.ReasonCode = "expenses"
        '                        Case Else
        '                            _InvoiceLine.ReasonCode = "services"
        '                    End Select
        '                    '  _InvoiceLine.ReasonCode = GetStringFromCell(drInvoiceHeader, "ProductTypeCode")

        '                    _InvoiceLine.Tax = New WSOrlandoGateway.TaxType
        '                    _InvoiceLine.Tax.TaxAmount = New WSOrlandoGateway.Amount
        '                    _InvoiceLine.Tax.TaxAmount.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(_itemrow, "CountryDesc"))
        '                    _InvoiceLine.Tax.TaxAmount.Value = GetDoubleFromCell(_itemrow, "ChargeTax") ' TotalRevenue")

        '                    _InvoiceLine.Tax.TaxBaseAmount = New WSOrlandoGateway.Amount
        '                    _InvoiceLine.Tax.TaxBaseAmount.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(_itemrow, "CountryDesc"))
        '                    _InvoiceLine.Tax.TaxBaseAmount.Value = Math.Round(GetDoubleFromCell(_itemrow, "TotalRevenue"), 2)
        '                    '_InvoiceLine.Tax.TaxBaseAmount.Value = Math.Round((GetDoubleFromCell(_itemrow, "TotalRevenue") '- GetDoubleFromCell(_itemrow, "ChargeTax")), 2)''
        '                    '                   _InvoiceLine.Tax.TaxBaseAmount.Value = GetDoubleFromCell(_itemrow, "ChargeTax")

        '                    _InvoiceLine.Tax.PercentQuantity = New WSOrlandoGateway.Quantity
        '                    _InvoiceLine.Tax.PercentQuantity.uom = "TAX" '--ConvertCountryToCurrencyEnum"(GetStringFromCell(_itemrow, "CountryDesc"))

        '                    If _InvoiceLine.Tax.TaxBaseAmount.Value <> 0 Then
        '                        Dim _TaxPercent As Double = Math.Round(Math.Abs(Convert.ToDouble((GetDoubleFromCell(_itemrow, "ChargeTax")) / Math.Abs(GetDoubleFromCell(_itemrow, "TotalRevenue"))) * 100), 2)
        '                        If (Math.Ceiling(_TaxPercent) - _TaxPercent) < 0.1 Then
        '                            _TaxPercent = Math.Ceiling(_TaxPercent)
        '                        End If

        '                        _InvoiceLine.Tax.PercentQuantity.Value = _TaxPercent '  Math.Round(Convert.ToDouble((GetDoubleFromCell(_itemrow, "TotalRevenue") / GetDoubleFromCell(_itemrow, "ChargeTax"))), 2)
        '                    Else
        '                        _InvoiceLine.Tax.PercentQuantity.Value = 0
        '                    End If

        '                    ' document reference
        '                    '''' Dim _DocumentRefereceList As New List(Of WSOrlandoGateway.DocumentReferenceType)
        '                    ''''Dim _DocumentReferece = New WSOrlandoGateway.DocumentReferenceType
        '                    ''''_DocumentReferece.DocumentId = New WSOrlandoGateway.DocumentIdType
        '                    '_DocumentReferece.DocumentId.Id = GetStringFromCell(_itemrow, "InvoiceNumber")

        '                    ''''_DocumentRefereceList.Add(_DocumentReferece)

        '                    '' and add it to the main object
        '                    '''' _InvoiceLine.DocumentReferences = _DocumentRefereceList.ToArray

        '                    ' Amount per quantity
        '                    _InvoiceLine.AmountPerQuantity = New WSOrlandoGateway.AmountPerQuantity
        '                    _InvoiceLine.AmountPerQuantity.Amount = New WSOrlandoGateway.Amount
        '                    _InvoiceLine.AmountPerQuantity.Amount.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(_itemrow, "CountryDesc"))
        '                    If GetDoubleFromCell(_itemrow, "TotalHoursBilled") <> 0 Then
        '                        _InvoiceLine.AmountPerQuantity.Amount.Value = Math.Round(Convert.ToDouble(GetDoubleFromCell(_itemrow, "TotalRevenue") / GetDoubleFromCell(_itemrow, "TotalHoursBilled")), 4)
        '                    Else
        '                        _InvoiceLine.AmountPerQuantity.Amount.Value = 0
        '                    End If


        '                    ' accepted values are "hour","day","week","2-weeks","4-weeks","month","item"
        '                    Dim _UOM As Long = GetLongFromCell(_itemrow, "UnitOfMeasure")
        '                    Select Case _UOM
        '                        Case Is = 1
        '                            _InvoiceLine.AmountPerQuantity.PerQuantity = "hour"
        '                        Case Is = 2
        '                            _InvoiceLine.AmountPerQuantity.PerQuantity = "day"
        '                        Case Is = 4
        '                            _InvoiceLine.AmountPerQuantity.PerQuantity = "month"
        '                        Case Else
        '                            _InvoiceLine.AmountPerQuantity.PerQuantity = "item"
        '                    End Select

        '                    If GetDoubleFromCell(_itemrow, "TotalHoursBilled") <> 0 Then
        '                        _InvoiceLine.ItemQuantity = Math.Round(GetDoubleFromCell(_itemrow, "TotalHoursBilled"), 2)
        '                    Else
        '                        _InvoiceLine.ItemQuantity = 0
        '                    End If

        '                    Dim _RateClassification As WSOrlandoGateway.TimeIntervalType = GetClassificationEnum(GetStringFromCell(_itemrow, "ProductCode"))
        '                    If Not IsNothing(_RateClassification) Then
        '                        _InvoiceLine.RateClassification = CreateSimpleEntityIdType(_RateClassification.ToString)
        '                    Else
        '                        _InvoiceLine.RateClassification = CreateEntityIdType(_itemrow, "ProductCode")
        '                    End If

        '                    _InvoiceLine.LineReferenceInformation = New WSOrlandoGateway.LineReferenceInformationType

        '                    _InvoiceLine.LineReferenceInformation.AssignmentId = CreateEntityIdType(_itemrow, "AssignmentID")

        '                    If GetStringFromCell(_itemrow, "OrderRequestCode") = String.Empty Then
        '                        _InvoiceLine.LineReferenceInformation.OrderId = CreateEntityIdType(_itemrow, "OrderCode")
        '                    Else
        '                        _InvoiceLine.LineReferenceInformation.OrderId = CreateEntityIdType(_itemrow, "OrderRequestCode")
        '                    End If
        '                    '_InvoiceLine.LineReferenceInformation.OrderId = CreateEntityIdType(_itemrow, "OrderRequestCode")
        '                    _InvoiceLine.LineReferenceInformation.StaffingSupplierOrgUnitId = CreateEntityIdType(_itemrow, "StaffingSupplierOrgUnitID")
        '                    _InvoiceLine.LineReferenceInformation.Label = GetStringFromCell(_itemrow, "UserAreaLabel")
        '                    _InvoiceLine.LineReferenceInformation.ServiceConcept = GetConceptArea(GetStringFromCell(_itemrow, "UserAreaServiceConcept"))

        '                    _LiteItemList.Add(_InvoiceLine)

        '                Next

        '                .Line = _LiteItemList.ToArray


        '            End With

        '            Dim InvoiceTypeXML As String = ConvertObjectToXML(_InvoiceType)

        '            ' connect to Orlando Gateway
        '            'Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
        '            ' connect to Orlando Gateway
        '            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drInvoiceHeader, "ConnectionUserName"), GetStringFromCell(drInvoiceHeader, "ConnectionPassword"))

        '            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

        '            AcknowledgementType = OrlandoGateway.ProcessInvoice(_InvoiceType)

        '            If Not IsNothing(AcknowledgementType) Then

        '                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
        '                    ' then we need to check any exception
        '                    If IsNothing(AcknowledgementType.Exception) Then
        '                        ' we are all good still
        '                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
        '                    Else
        '                        warningString.AppendFormat("|Error: Acknowlegement Exception in UpdateInvoiceToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
        '                    End If

        '                End If

        '                'If AcknowledgementType.Exception = AcceptRejectRule Then

        '            Else
        '                ' we need to log an exception here
        '                warningString.Append("|Error: Exception in UpdateInvoiceToOrlando().  Message: No acknowledgement recieved from server")
        '            End If

        '        Catch ex As Exception
        '            ' we need to log an exception here
        '            warningString.AppendFormat("|Error: Exception in UpdateInvoiceToOrlando().  Message: {0}", ex.Message)
        '        End Try

        '        ' if we have recorded warnings we need to return it
        '        If warningString.ToString <> String.Empty Then
        '            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        '        End If

        Return _retval

    End Function

#End Region

#Region "Open Invoice"

    Public Function ProcessOpenInvoiceToOrlando(dsrows() As DataRow) As String

        Dim _retval As String = String.Empty
        Dim drHeader As DataRow = Nothing

        Dim _OpenInvoiceType As New WSOrlandoGateway.OpenInvoiceAmount
        Dim warningString = New StringBuilder()

        systemSettings = GetSystemSettings("SUBSCRIBER")

        Try
            drHeader = dsrows(0)

            _OpenInvoiceType.OpenInvoiceAmountDate = DateAndTime.Now.Date
            _OpenInvoiceType.TotalNoOfOpenInvoice = dsrows.Length

            Dim _OpenInvoiceList As New List(Of WSOrlandoGateway.OpenInvoiceAmountTypeOpenInvoices)

            ' loop and create XML
            For Each _Row As DataRow In dsrows
                ' the key to each open invoice xml record is one file per client/division profile...
                ' the data set has the following fileds 
                '            ClientCode	ClientAgencyCode	StaffingSupplierOrgUnitID	UserAreaLabel	UserAreaServiceConcept	
                '           004283	    854283	            85	                        Randstad	    Staffing	

                ' so we loop for when the ClientCode,StaffingSupplier,UserAreaLabel and  UserAreasErviceConcept are the same... 
                Dim _OpenInvoice As New WSOrlandoGateway.OpenInvoiceAmountTypeOpenInvoices

                _OpenInvoice.InvoiceId = CreateEntityIdType(_Row, "InvoiceCode")
                _OpenInvoice.DocumentDateTime = GetDateFromCell(_Row, "Doc_Date")

                _OpenInvoice.OpenAmount = New WSOrlandoGateway.AmountType
                _OpenInvoice.OpenAmount.currency = ConvertCountryToCurrencyEnum(GetStringFromCell(_Row, "CountryDesc"))
                _OpenInvoice.OpenAmount.Value = GetDoubleFromCell(_Row, "Current_Amount")

                _OpenInvoice.DueDate = GetDateFromCell(_Row, "Due_Date")
                _OpenInvoice.LegalEntity = GetStringFromCell(_Row, "LegalEntity")


                _OpenInvoiceList.Add(_OpenInvoice)

            Next

            _OpenInvoiceType.OpenInvoices = _OpenInvoiceList.ToArray

            _OpenInvoiceType.ReferenceInformation = New WSOrlandoGateway.OpenInvoiceAmountTypeReferenceInformation

            With _OpenInvoiceType.ReferenceInformation
                .StaffingSupplierId = CreateSimpleEntityIdType(ConvertCountryToSupplierID(GetStringFromCell(drHeader, "CountryDesc")))

                .StaffingSupplierOrgUnitId = CreateEntityIdType(drHeader, "StaffingSupplierOrgUnitID")
                .StaffingCustomerId = CreateEntityIdType(drHeader, "ClientCode")

                .UserArea = New WSOrlandoGateway.OpenInvoiceAmountUserAreaType
                .UserArea.ReferenceInformationAdditional = New WSOrlandoGateway.OpenInvoiceAmountStatusReferenceInformationAdditionalType
                .UserArea.ReferenceInformationAdditional.MessageDetails = New WSOrlandoGateway.OpenInvoiceAmountStatusReferenceInformationAdditionalTypeMessageDetails
                With .UserArea.ReferenceInformationAdditional.MessageDetails
                    .MessageId = Guid.NewGuid.ToString
                    .MessageTimestamp = DateAndTime.Now
                End With

            End With

            Dim OpenInvoiceTypeXML As String = ConvertObjectToXML(_OpenInvoiceType)

            ' connect to Orlando Gateway
            'Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings)
            ' connect to Orlando Gateway
            Dim OrlandoGateway As WSOrlandoGateway.OrlandoSoapService = ConnectToiOrlandoService(systemSettings, GetStringFromCell(drHeader, "ConnectionUserName"), GetStringFromCell(drHeader, "ConnectionPassword"))

            Dim AcknowledgementType As WSOrlandoGateway.SimpleAcknowledgementType = Nothing

            AcknowledgementType = OrlandoGateway.ProcessOpenInvoiceAmount(_OpenInvoiceType)

            If Not IsNothing(AcknowledgementType) Then

                If AcknowledgementType.ReceiptAcknowledged = WSOrlandoGateway.ReceiptAcknowledgedEnumType.true Then
                    ' then we need to check any exception
                    If IsNothing(AcknowledgementType.Exception) Then
                        ' we are all good still
                        _retval = AcknowledgementType.ReferenceId.IdValue(0).Value.ToString
                    Else
                        warningString.AppendFormat("|Error: Acknowlegement Exception in ProcessOpenInvoiceToOrlando(). ErrorCode: {0}. Message: {1}", AcknowledgementType.Exception.ExceptionIdentifier, AcknowledgementType.Exception.ExceptionMessage)
                    End If

                End If

                'If AcknowledgementType.Exception = AcceptRejectRule Then

            Else
                ' we need to log an exception here
                warningString.Append("|Error: Exception in ProcessOpenInvoiceToOrlando().  Message: No acknowledgement recieved from server")
            End If

        Catch ex As Exception
            ' we need to log an exception here
            warningString.AppendFormat("|Error: Exception in ProcessOpenInvoiceToOrlando().  Message: {0}", ex.Message)
        End Try

        ' if we have recorded warnings we need to return it
        If warningString.ToString <> String.Empty Then
            Throw New Exception(String.Format("BOWarning:{0}", warningString.ToString))
        End If

        Return _retval



    End Function

    Public Function ProcessITPROpenInvoiceToOrlando(dsrows() As DataRow) As String

        Dim _retval As String = String.Empty
        Dim drHeader As DataRow = Nothing
        Dim warningString = New StringBuilder()

        Dim iRowCount As Integer = 0
        Dim dRow As DataRow = dsrows(iRowCount)

        Dim dateFormat As String = "yyyy-MM-ddZ"

        Dim ThisRegex As New Regex("[^a-zA-Z0-9]\s")

        Dim TotalNoOfOpenInvoices As Integer = 0
        Dim TotalAmountOfOpenInvoices As Double = 0

        '// build it
        Dim _OpenInvoice As New OpenInvoiceAmount
        _OpenInvoice.Supplier = New OpenInvoiceAmountTypeSupplier
        _OpenInvoice.Supplier.SupplierId = ConvertCountryToSupplierID(GetStringFromCell(dRow, "CountryDesc"))

        Dim LegalEntities = New List(Of OpenInvoiceAmountTypeLegalEntity)

        '        For Each dRow As DataRow In dsrows
        Do While iRowCount < dsrows.Length

            Dim LegalEntity = New OpenInvoiceAmountTypeLegalEntity
            'Dim LegalEntitySplit() As String = GetStringFromCell(dRow, "LegalEntityFullName").Split(".")
            LegalEntity.LegalEntityCode = GetStringFromCell(dRow, "LegalEntityCode")
            LegalEntity.LegalEntityName = GetStringFromCell(dRow, "LegalEntityName")

            Dim Customers As New List(Of OpenInvoiceAmountTypeLegalEntityCustomer)


            Do While (LegalEntity.LegalEntityCode = GetStringFromCell(dRow, "LegalEntityCode")) And iRowCount < dsrows.Length

                Dim Customer = New OpenInvoiceAmountTypeLegalEntityCustomer
                Customer.CustomerId = GetStringFromCell(dRow, "CustomerCode")

                Customer.CustomerInvoiceName = GetStringFromCell(dRow, "CustomerInvoiceName")

                Customer.CustomerBillingAddress = New CustomerBillingAddressType
                Dim InvoiceAddress As New StringBuilder

                '// the data can have empty vlaues for this 

                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress1") <> String.Empty Then
                    InvoiceAddress.Append(GetStringFromCell(dRow, "InvoiceAddressStreetAddress1"))
                End If
                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress2") <> String.Empty Then
                    InvoiceAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "InvoiceAddressStreetAddress2"))
                End If
                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress3") <> String.Empty Then
                    InvoiceAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "InvoiceAddressStreetAddress3"))
                End If

                If InvoiceAddress.Length >= 100 Then
                    Customer.CustomerBillingAddress.StreetAddress = ThisRegex.Replace(InvoiceAddress.ToString.Substring(0, 99), "")
                Else
                    Customer.CustomerBillingAddress.StreetAddress = ThisRegex.Replace(InvoiceAddress.ToString, "")
                End If
                Customer.CustomerBillingAddress.BuildingNumber = ""

                Dim InvoiceAddressSuburb As String = GetStringFromCell(dRow, "InvoiceAddressMunicipality")
                ' a few extra checks then here...  sometimes the suburb is put into address line 3... hooray!!!
                If (InvoiceAddressSuburb = String.Empty) Then
                    Dim ExtraAddressCheck = GetStringFromCell(dRow, "InvoiceAddressStreetAddress3")
                    If (ExtraAddressCheck = String.Empty) Then
                        InvoiceAddressSuburb = "No Muncipality Specified"
                    Else
                        InvoiceAddressSuburb = ExtraAddressCheck.ToUpper
                    End If
                End If

                If InvoiceAddressSuburb.Length >= 60 Then
                    Customer.CustomerBillingAddress.Municipality = InvoiceAddressSuburb.Substring(0, 59)            ' there are some that are incorrect 
                Else
                    Customer.CustomerBillingAddress.Municipality = InvoiceAddressSuburb
                End If

                Customer.CustomerBillingAddress.PostalCode = GetStringFromCell(dRow, "InvoiceAddressPostCode")
                Customer.CustomerBillingAddress.CountryCode = ConvertCountryToCode(GetStringFromCell(dRow, "InvoiceAddressCountry"))


                Customer.CustomerContractAddress = New CustomerContractAddressType
                Dim ContractAddress As New StringBuilder
                If GetStringFromCell(dRow, "ContractAddressStreetAddress1") <> String.Empty Then
                    ContractAddress.Append(GetStringFromCell(dRow, "ContractAddressStreetAddress1"))
                End If
                If GetStringFromCell(dRow, "ContractAddressStreetAddress2") <> String.Empty Then
                    ContractAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "ContractAddressStreetAddress2"))
                End If
                If GetStringFromCell(dRow, "ContractAddressStreetAddress3") <> String.Empty Then
                    ContractAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "ContractAddressStreetAddress3"))
                End If

                If ContractAddress.Length >= 100 Then
                    Customer.CustomerContractAddress.StreetAddress = ThisRegex.Replace(ContractAddress.ToString.Substring(0, 99), "")
                Else
                    Customer.CustomerContractAddress.StreetAddress = ThisRegex.Replace(ContractAddress.ToString, "")
                End If
                Customer.CustomerContractAddress.BuildingNumber = ""

                Dim ContractAddressSuburb As String = GetStringFromCell(dRow, "ContractAddressMunicipality")
                If ContractAddressSuburb.Length >= 60 Then
                    Customer.CustomerContractAddress.Municipality = ContractAddressSuburb.Substring(0, 59)            ' there are some that are incorrect 
                Else
                    Customer.CustomerContractAddress.Municipality = ContractAddressSuburb
                End If
                Customer.CustomerContractAddress.PostalCode = GetStringFromCell(dRow, "ContractAddressPostCode")
                Customer.CustomerContractAddress.CountryCode = ConvertCountryToCode(GetStringFromCell(dRow, "ContractAddressCountry"))


                Customer.LocalClientIdentifier = GetStringFromCell(dRow, "LocalClientIdentifier")
                Customer.CustomerLocalRating = GetStringFromCell(dRow, "CustomerLocalRating")
                'Customer.AssignAbilityStatus = 
                Customer.AssignAbilityStatusSpecified = False


                If (Customer.CustomerContractAddress.StreetAddress.ToLower.Contains("29 grange road")) Then
                    Customer.CustomerContractAddress.StreetAddress = "29 GRANGE ROAD"
                End If

                If (Customer.CustomerBillingAddress.StreetAddress.ToLower.Contains("29 grange road")) Then
                    Customer.CustomerBillingAddress.StreetAddress = "29 GRANGE ROAD"
                End If

                Dim Invoices = New List(Of OpenInvoiceAmountTypeLegalEntityCustomerOpenInvoice)

                Do While Customer.CustomerId = GetStringFromCell(dRow, "CustomerCode") And iRowCount < dsrows.Length

                    Dim Invoice = New OpenInvoiceAmountTypeLegalEntityCustomerOpenInvoice
                    Invoice.InvoiceId = GetStringFromCell(dRow, "InvoiceCode").Trim
                    Invoice.InvoiceDescription = String.Format("No description available for invoice no. {0}", Invoice.InvoiceId)

                    Invoice.DocumentDateTime = GetDateFromCell(dRow, "Doc_Date").ToString(dateFormat)

                    Dim _CurrencyCode As String = ConvertCountryToCurrencyCode(GetStringFromCell(dRow, "CountryDesc"))

                    Invoice.OriginalOpenAmount = New AmountType
                    Invoice.OriginalOpenAmount.currency = _CurrencyCode
                    Invoice.OriginalOpenAmount.Value = Math.Round(GetDoubleFromCell(dRow, "Original_Amount"), 2)

                    Invoice.OriginalOpenVATAmount = New AmountType
                    Invoice.OriginalOpenVATAmount.currency = _CurrencyCode
                    Invoice.OriginalOpenVATAmount.Value = Math.Round(GetDoubleFromCell(dRow, "OriginalOpenVATAmount"), 2)

                    Invoice.OpenInvoiceAmount = New AmountType
                    Invoice.OpenInvoiceAmount.currency = _CurrencyCode
                    'Invoice.OpenInvoiceAmount.Value = Math.Round(GetDoubleFromCell(dRow, "Open_Invoice_Amount"), 2)
                    Invoice.OpenInvoiceAmount.Value = Math.Round(GetDoubleFromCell(dRow, "Current_Amount"), 2)

                    Invoice.OpenInvoiceVATAmount = New AmountType
                    Invoice.OpenInvoiceVATAmount.currency = _CurrencyCode
                    '// we need to calc this...
                    If Invoice.OriginalOpenVATAmount.Value > 0 Then
                        Invoice.OpenInvoiceVATAmount.Value = Math.Round((GetDoubleFromCell(dRow, "Current_Amount") * 0.1), 2)
                    Else
                        Invoice.OpenInvoiceVATAmount.Value = 0
                    End If
                    'Invoice.OpenInvoiceVATAmount.Value = Math.Round(GetDoubleFromCell(dRow, "OpenInvoiceVATAmount"), 2)

                    Invoice.DueDate = GetDateFromCell(dRow, "Due_Date").ToString(dateFormat)

                    Invoice.InDispute = GetBooleanFromCell(dRow, "InDispute")
                    If GetStringFromCell(dRow, "SellInvoice").ToLower = "yes" Then
                        Invoice.SellInvoice = SellInvoiceType.economic
                    Else
                        Invoice.SellInvoice = SellInvoiceType.no
                    End If

                    ' // New requirement ITPR
                    Select Case GetStringFromCell(dRow, "Document_Type").ToLower
                        Case "returns"
                            Invoice.DocumentType = OpenInvoiceDocumentType.return
                        Case "payment"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case "credit notes"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case "debit notes"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case Else
                            Invoice.DocumentType = OpenInvoiceDocumentType.invoice
                    End Select


                    If Invoice.OpenInvoiceAmount.Value >= 0 Then
                        Invoice.DocumentType = OpenInvoiceDocumentType.invoice
                    Else
                        Invoice.DocumentType = OpenInvoiceDocumentType.return
                    End If


                    '<ParentGuarantee>ParentGuarantee</ParentGuarantee>
                    '<OptionalField1>OptionalField1</OptionalField1>
                    '<OptionalField2>OptionalField2</OptionalField2>

                    Invoices.Add(Invoice)

                    TotalNoOfOpenInvoices += 1
                    TotalAmountOfOpenInvoices += Invoice.OpenInvoiceAmount.Value


                    iRowCount += 1
                    If iRowCount < dsrows.Length Then
                        dRow = dsrows(iRowCount)
                    End If

                Loop

                Customer.OpenInvoices = Invoices.ToArray
                Customers.Add(Customer)

            Loop

            LegalEntity.Customers = Customers.ToArray
            LegalEntities.Add(LegalEntity)

        Loop

        _OpenInvoice.LegalEntities = LegalEntities.ToArray

        _OpenInvoice.OpenInvoiceAmountDate = GetDateFromCell(dRow, "ReferenceMessageDate").ToString(dateFormat)
        _OpenInvoice.OpenInvoiceAmountFormatVersion = "2.0.0"
        _OpenInvoice.TotalAmountOfOpenInvoices = Math.Round(TotalAmountOfOpenInvoices, 2)
        _OpenInvoice.TotalNoOfOpenInvoices = TotalNoOfOpenInvoices


        _OpenInvoice.MessageDetails = New OpenInvoiceAmountTypeMessageDetails
        _OpenInvoice.MessageDetails.MessageId = GetStringFromCell(dRow, "ReferenceMessageID")
        _OpenInvoice.MessageDetails.MessageTimeStamp = GetDateFromCell(dRow, "ReferenceMessageDate").ToString("u")


        ' we then convert this to xml, 
        Dim xmlOpenInvoices As String = ConvertObjectToXML(_OpenInvoice)

        ' Naming convention for XML file: {SupplierId}_oia_{DateTime}_{Index}.xml
        '{SupplierId}: Supplier Id, eg. “DE_001”
        '{DateTime}: date and time when the file is created with format “YYYYMMDDHHMISS”, eg. “20170923053757”
        '{Index}: index number of the file, starting from 1 and incremented by 1 for each generated XML file
        'Files name example: DE_001_oia_20170923053757_1.xml, DE_001_oia_20170923053757_2.xml

        '//Dim filename As String = String.Format("{0}\{1}_oia_{2}.xml", "c:\temp", ConvertCountryToSupplierID(GetStringFromCell(dRow, "CountryDesc")), DateAndTime.Now.ToString("yyyyMMddHHmmss"))

        'Dim file As System.IO.StreamWriter
        'file = My.Computer.FileSystem.OpenTextFileWriter(filename, False)
        'file.Write(xmlOpenInvoices)
        'file.Close()

        '_retval = filename

        Return xmlOpenInvoices

    End Function

    Public Function ProcessOpenInvoiceToOrlandoVersion2(dsrows() As DataRow) As OpenInvoiceVersion2
        Return ProcessOpenInvoiceToOrlandoVersion2(dsrows, String.Empty)
    End Function

    Public Function ProcessOpenInvoiceToOrlandoVersion2(dsrows() As DataRow, withOverRideSupplierID As String) As OpenInvoiceVersion2

        Dim drHeader As DataRow = Nothing
        Dim warningString = New StringBuilder()

        Dim iRowCount As Integer = 0
        Dim dRow As DataRow = dsrows(iRowCount)

        Dim dateFormat As String = "yyyy-MM-ddZ"

        Dim ThisRegex As New Regex("[^a-zA-Z0-9]\s")

        Dim TotalNoOfOpenInvoices As Integer = 0
        Dim TotalAmountOfOpenInvoices As Double = 0

        '// build it
        Dim _OpenInvoice As New OpenInvoiceVersion2
        Dim LegalEntity = New OpenInvoiceVersion2LegalEntity
        If withOverRideSupplierID = String.Empty Then
            LegalEntity.SupplierId = ConvertCountryToSupplierID(GetStringFromCell(dRow, "CountryDesc"))
        Else
            LegalEntity.SupplierId = withOverRideSupplierID
        End If
        'LegalEntity.SupplierId = ConvertCountryToSupplierID(GetStringFromCell(dRow, "CountryDesc"))
        LegalEntity.LegalEntityCode = GetStringFromCell(dRow, "LegalEntityCode")
        LegalEntity.LegalEntityName = GetStringFromCell(dRow, "LegalEntityName")

        _OpenInvoice.LegalEntity = LegalEntity

        Dim Customers As New List(Of OpenInvoiceVersion2Customer)
        Dim Invoices = New List(Of OpenInvoiceVersion2OpenInvoice)

        ' For Each dRow As DataRow In dsrows
        Do While iRowCount < dsrows.Length

            '//Dim Customers As New List(Of OpenInvoiceVersion2Customer)

            Do While (LegalEntity.LegalEntityCode = GetStringFromCell(dRow, "LegalEntityCode")) And iRowCount < dsrows.Length

                Dim Customer = New OpenInvoiceVersion2Customer
                Customer.SupplierId = LegalEntity.SupplierId
                Customer.LegalEntityCode = LegalEntity.LegalEntityCode
                Customer.CustomerId = GetStringFromCell(dRow, "CustomerCode")

                Customer.CustomerInvoiceName = GetStringFromCell(dRow, "CustomerInvoiceName")

                Customer.CustomerBillingAddress = New OpenInvoiceVersion2CustomerAddress
                Dim InvoiceAddress As New StringBuilder

                '// the data can have empty vlaues for this 

                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress1") <> String.Empty Then
                    InvoiceAddress.Append(GetStringFromCell(dRow, "InvoiceAddressStreetAddress1"))
                End If
                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress2") <> String.Empty Then
                    InvoiceAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "InvoiceAddressStreetAddress2"))
                End If
                If GetStringFromCell(dRow, "InvoiceAddressStreetAddress3") <> String.Empty Then
                    InvoiceAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "InvoiceAddressStreetAddress3"))
                End If

                If InvoiceAddress.Length >= 100 Then
                    Customer.CustomerBillingAddress.StreetAddress = ThisRegex.Replace(InvoiceAddress.ToString.Substring(0, 99), "")
                Else
                    Customer.CustomerBillingAddress.StreetAddress = ThisRegex.Replace(InvoiceAddress.ToString, "")
                End If
                Customer.CustomerBillingAddress.BuildingNumber = ""

                Dim InvoiceAddressSuburb As String = GetStringFromCell(dRow, "InvoiceAddressMunicipality")
                ' a few extra checks then here...  sometimes the suburb is put into address line 3... hooray!!!
                If (InvoiceAddressSuburb = String.Empty) Then
                    Dim ExtraAddressCheck = GetStringFromCell(dRow, "InvoiceAddressStreetAddress3")
                    If (ExtraAddressCheck = String.Empty) Then
                        InvoiceAddressSuburb = "No Muncipality Specified"
                    Else
                        InvoiceAddressSuburb = ExtraAddressCheck.ToUpper
                    End If
                End If

                If InvoiceAddressSuburb.Length >= 60 Then
                    Customer.CustomerBillingAddress.Municipality = InvoiceAddressSuburb.Substring(0, 59)            ' there are some that are incorrect 
                Else
                    Customer.CustomerBillingAddress.Municipality = InvoiceAddressSuburb
                End If

                Customer.CustomerBillingAddress.PostalCode = GetStringFromCell(dRow, "InvoiceAddressPostCode")
                Customer.CustomerBillingAddress.CountryCode = ConvertCountryToCode(GetStringFromCell(dRow, "InvoiceAddressCountry"))


                Customer.CustomerContractAddress = New OpenInvoiceVersion2CustomerAddress
                Dim ContractAddress As New StringBuilder
                If GetStringFromCell(dRow, "ContractAddressStreetAddress1") <> String.Empty Then
                    ContractAddress.Append(GetStringFromCell(dRow, "ContractAddressStreetAddress1"))
                End If
                If GetStringFromCell(dRow, "ContractAddressStreetAddress2") <> String.Empty Then
                    ContractAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "ContractAddressStreetAddress2"))
                End If
                If GetStringFromCell(dRow, "ContractAddressStreetAddress3") <> String.Empty Then
                    ContractAddress.AppendFormat(", {0}", GetStringFromCell(dRow, "ContractAddressStreetAddress3"))
                End If

                If ContractAddress.Length >= 100 Then
                    Customer.CustomerContractAddress.StreetAddress = ThisRegex.Replace(ContractAddress.ToString.Substring(0, 99), "")
                Else
                    Customer.CustomerContractAddress.StreetAddress = ThisRegex.Replace(ContractAddress.ToString, "")
                End If
                Customer.CustomerContractAddress.BuildingNumber = ""

                Dim ContractAddressSuburb As String = GetStringFromCell(dRow, "ContractAddressMunicipality")
                If ContractAddressSuburb.Length >= 60 Then
                    Customer.CustomerContractAddress.Municipality = ContractAddressSuburb.Substring(0, 59)            ' there are some that are incorrect 
                Else
                    Customer.CustomerContractAddress.Municipality = ContractAddressSuburb
                End If
                Customer.CustomerContractAddress.PostalCode = GetStringFromCell(dRow, "ContractAddressPostCode")
                Customer.CustomerContractAddress.CountryCode = ConvertCountryToCode(GetStringFromCell(dRow, "ContractAddressCountry"))


                Customer.LocalClientIdentifier = GetStringFromCell(dRow, "LocalClientIdentifier")
                Customer.CustomerLocalRating = GetStringFromCell(dRow, "CustomerLocalRating")
                Customer.AssignAbilityStatus = GetStringFromCell(dRow, "AssignAbilityStatus")



                If (Customer.CustomerContractAddress.StreetAddress.ToLower.Contains("29 grange road")) Then
                    Customer.CustomerContractAddress.StreetAddress = "29 GRANGE ROAD"
                End If

                If (Customer.CustomerBillingAddress.StreetAddress.ToLower.Contains("29 grange road")) Then
                    Customer.CustomerBillingAddress.StreetAddress = "29 GRANGE ROAD"
                End If

                '//Dim Invoices = New List(Of OpenInvoiceVersion2OpenInvoice)

                Do While Customer.CustomerId = GetStringFromCell(dRow, "CustomerCode") And iRowCount < dsrows.Length

                    Dim Invoice = New OpenInvoiceVersion2OpenInvoice
                    Invoice.SupplierId = LegalEntity.SupplierId
                    Invoice.LegalEntityCode = LegalEntity.LegalEntityCode
                    Invoice.CustomerId = Customer.CustomerId

                    Invoice.InvoiceId = GetStringFromCell(dRow, "InvoiceCode").Trim
                    Invoice.InvoiceDescription = String.Format("No description available for invoice no. {0}", Invoice.InvoiceId)

                    Invoice.DocumentDate = GetDateFromCell(dRow, "Doc_Date").ToString(dateFormat)

                    Dim _CurrencyCode As String = ConvertCountryToCurrencyCode(GetStringFromCell(dRow, "CountryDesc"))

                    '// original open amount
                    Invoice.OriginalOpenAmount = New AmountType
                    If GetStringFromCell(dRow, "Original_Amount_Currency") = String.Empty Then
                        Invoice.OriginalOpenAmount.currency = _CurrencyCode
                    Else
                        Invoice.OriginalOpenAmount.currency = GetStringFromCell(dRow, "Original_Amount_Currency")
                    End If
                    'Invoice.OriginalOpenAmount.currency = _CurrencyCode
                    Invoice.OriginalOpenAmount.Value = Math.Round(GetDoubleFromCell(dRow, "Original_Amount"), 2)

                    ' original open amount VAT
                    Invoice.OriginalOpenVATAmount = New AmountType
                    If GetStringFromCell(dRow, "Original_Amount_Currency") = String.Empty Then
                        Invoice.OriginalOpenVATAmount.currency = _CurrencyCode
                    Else
                        Invoice.OriginalOpenVATAmount.currency = GetStringFromCell(dRow, "Original_Amount_Currency")
                    End If
                    'Invoice.OriginalOpenVATAmount.currency = _CurrencyCode
                    Invoice.OriginalOpenVATAmount.Value = Math.Round(GetDoubleFromCell(dRow, "OriginalOpenVATAmount"), 2)

                    ' open amount
                    Invoice.OpenInvoiceAmount = New AmountType
                    If GetStringFromCell(dRow, "Open_Invoice_Currency") = String.Empty Then
                        Invoice.OpenInvoiceAmount.currency = _CurrencyCode
                    Else
                        Invoice.OpenInvoiceAmount.currency = GetStringFromCell(dRow, "Open_Invoice_Currency")
                    End If
                    'nvoice.OpenInvoiceAmount.currency = _CurrencyCode
                    Invoice.OpenInvoiceAmount.Value = Math.Round(GetDoubleFromCell(dRow, "Open_Invoice_Amount"), 2)

                    ' open amount VAT
                    Invoice.OpenInvoiceVATAmount = New AmountType
                    If GetStringFromCell(dRow, "Open_Invoice_Currency") = String.Empty Then
                        Invoice.OpenInvoiceVATAmount.currency = _CurrencyCode
                    Else
                        Invoice.OpenInvoiceVATAmount.currency = GetStringFromCell(dRow, "Open_Invoice_Currency")
                    End If
                    Invoice.OpenInvoiceVATAmount.Value = Math.Round(GetDoubleFromCell(dRow, "OpenInvoiceVATAmount"), 2)

                    Invoice.DueDate = GetDateFromCell(dRow, "Due_Date").ToString(dateFormat)

                    Invoice.InDispute = GetBooleanFromCell(dRow, "InDispute")

                    Select Case GetStringFromCell(dRow, "SellInvoice").ToLower
                        Case "yes"
                            Invoice.SellInvoice = SellInvoiceType.economic
                        Case "legal"
                            Invoice.SellInvoice = SellInvoiceType.legal
                        Case Else
                            Invoice.SellInvoice = SellInvoiceType.no
                    End Select

                    'If GetStringFromCell(dRow, "SellInvoice").ToLower = "yes" Then
                    ' Invoice.SellInvoice = SellInvoiceType.economic
                    'Else
                    ' Invoice.SellInvoice = SellInvoiceType.no
                    'End If

                    ' // New requirement ITPR
                    Select Case GetStringFromCell(dRow, "Document_Type").ToLower
                        Case "returns"
                            Invoice.DocumentType = OpenInvoiceDocumentType.return
                        Case "payment"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case "credit notes"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case "debit notes"
                            Invoice.DocumentType = OpenInvoiceDocumentType.payment
                        Case Else
                            Invoice.DocumentType = OpenInvoiceDocumentType.invoice
                    End Select


                    '/If Invoice.OpenInvoiceAmount.Value >= 0 Then
                    'Invoice.DocumentType = OpenInvoiceDocumentType.invoice
                    'Else
                    'Invoice.DocumentType = OpenInvoiceDocumentType.return
                    'End If

                    ' and lets look at the optional fields
                    Invoice.OptionalField1 = GetStringFromCell(dRow, "OptionalField1")
                    Invoice.OptionalField2 = GetStringFromCell(dRow, "OptionalField2")
                    Invoice.OptionalField3 = GetStringFromCell(dRow, "OptionalField3")
                    Invoice.OptionalField4 = GetStringFromCell(dRow, "OptionalField4")
                    Invoice.OptionalField5 = GetStringFromCell(dRow, "OptionalField5")
                    Invoice.OptionalField6 = GetStringFromCell(dRow, "OptionalField6")
                    Invoice.OptionalField7 = GetStringFromCell(dRow, "OptionalField7")
                    Invoice.OptionalField8 = GetStringFromCell(dRow, "OptionalField8")


                    '<ParentGuarantee>ParentGuarantee</ParentGuarantee>
                    '<OptionalField1>OptionalField1</OptionalField1>
                    '<OptionalField2>OptionalField2</OptionalField2>

                    Invoices.Add(Invoice)

                    TotalNoOfOpenInvoices += 1
                    TotalAmountOfOpenInvoices += Invoice.OpenInvoiceAmount.Value


                    iRowCount += 1
                    If iRowCount < dsrows.Length Then
                        dRow = dsrows(iRowCount)
                    End If

                Loop

                Customers.Add(Customer)

            Loop

            'LegalEntity.Customers = Customers.ToArray
            'LegalEntities.Add(LegalEntity)



        Loop

        _OpenInvoice.Customers = Customers
        _OpenInvoice.OpenInvoices = Invoices

        ' version 2 specific structure
        _OpenInvoice.FileNumber = DateAndTime.Now.ToString("yyyyMMddHHmmss")

        _OpenInvoice.Summary = New List(Of OpenInvoiceVersion2Summary)
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "SupplierId", LegalEntity.SupplierId))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "FileNumber", _OpenInvoice.FileNumber))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "MessageId", GetStringFromCell(dRow, "ReferenceMessageID")))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "MessageTimeStamp", GetDateFromCell(dRow, "ReferenceMessageDate").ToString("yyyy-MM-ddTHH:mm:ssZ")))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "OpenInvoiceAmountFormatVersion", "2.2.0"))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "OpenInvoiceAmountDate", GetDateFromCell(dRow, "ReferenceMessageDate").ToString(dateFormat)))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "TotalNoOfOpenInvoices", TotalNoOfOpenInvoices.ToString))
        _OpenInvoice.Summary.Add(New OpenInvoiceVersion2Summary("Message", "TotalAmountOfOpenInvoices", Math.Round(TotalAmountOfOpenInvoices, 2).ToString))


        '  _OpenInvoice.LegalEntities = LegalEntities.ToArray

        ' _OpenInvoice.OpenInvoiceAmountDate = GetDateFromCell(dRow, "ReferenceMessageDate").ToString(dateFormat)
        '_OpenInvoice.OpenInvoiceAmountFormatVersion = "2.0.0"
        '  _OpenInvoice.TotalAmountOfOpenInvoices = Math.Round(TotalAmountOfOpenInvoices, 2)
        ' _OpenInvoice.TotalNoOfOpenInvoices = TotalNoOfOpenInvoices


        ' _OpenInvoice.MessageDetails = New OpenInvoiceAmountTypeMessageDetails
        ' _OpenInvoice.MessageDetails.MessageId = GetStringFromCell(dRow, "ReferenceMessageID")
        ' _OpenInvoice.MessageDetails.MessageTimeStamp = GetDateFromCell(dRow, "ReferenceMessageDate").ToString("u")


        ' we then convert this to xml, 
        'Dim xmlOpenInvoices As String = ConvertObjectToXML(_OpenInvoice)

        ' Naming convention for XML file: {SupplierId}_oia_{DateTime}_{Index}.xml
        '{SupplierId}: Supplier Id, eg. “DE_001”
        '{DateTime}: date and time when the file is created with format “YYYYMMDDHHMISS”, eg. “20170923053757”
        '{Index}: index number of the file, starting from 1 and incremented by 1 for each generated XML file
        'Files name example: DE_001_oia_20170923053757_1.xml, DE_001_oia_20170923053757_2.xml

        '//Dim filename As String = String.Format("{0}\{1}_oia_{2}.xml", "c:\temp", ConvertCountryToSupplierID(GetStringFromCell(dRow, "CountryDesc")), DateAndTime.Now.ToString("yyyyMMddHHmmss"))

        'Dim file As System.IO.StreamWriter
        'file = My.Computer.FileSystem.OpenTextFileWriter(filename, False)
        'file.Write(xmlOpenInvoices)
        'file.Close()

        '_retval = filename

        Return _OpenInvoice

    End Function



    Public Function UpdateOpenInvoiceToOrlando() As String
        Return UpdateOpenInvoiceToOrlando(BOMObjectLibrary("ENTITYXML"))
    End Function

    Public Function UpdateOpenInvoiceToOrlando(EntityXML As String) As String     'string username, string password, string emailaddress, Guid CRMSecurityKey, string hrXMLVersion, int iProfileId)

        '    ' this method uses the more traditional integration process
        Dim _retval As String = String.Empty

        ' do nothing
        Return _retval

    End Function

#End Region

#Region "XML Assistants"

    Private Function CreateEntityIdType(dr As DataRow, columnName As String) As WSOrlandoGateway.EntityIdType

        Dim _EntityIDType As New WSOrlandoGateway.EntityIdType

        Dim _EntityIdTypeIDValueList(0) As WSOrlandoGateway.EntityIdTypeIdValue
        Dim _EntityIdTypeIDValue As New WSOrlandoGateway.EntityIdTypeIdValue

        _EntityIdTypeIDValue.Value = GetStringFromCell(dr, columnName)
        _EntityIdTypeIDValue.name = columnName

        _EntityIdTypeIDValueList(0) = _EntityIdTypeIDValue

        _EntityIDType.IdValue = _EntityIdTypeIDValueList

        Return _EntityIDType

    End Function




    Private Function CreateEntityIdType(dr As DataRow, columnName As String, length As Integer) As WSOrlandoGateway.EntityIdType

        Dim _EntityIDType As New WSOrlandoGateway.EntityIdType

        _EntityIDType = CreateEntityIdType(dr, columnName)

        Dim _CurrentValue As String = _EntityIDType.IdValue(0).Value

        ' now change the value to the length value
        _EntityIDType.IdValue(0).Value = _CurrentValue.Substring(_CurrentValue.Length - 5)

        Return _EntityIDType

    End Function

    Private Function CreateBlankEntityIdType() As WSOrlandoGateway.EntityIdType

        Dim _EntityIDType As New WSOrlandoGateway.EntityIdType

        Dim _EntityIdTypeIDValueList(0) As WSOrlandoGateway.EntityIdTypeIdValue
        Dim _EntityIdTypeIDValue As New WSOrlandoGateway.EntityIdTypeIdValue

        _EntityIdTypeIDValueList(0) = _EntityIdTypeIDValue

        _EntityIDType.IdValue = _EntityIdTypeIDValueList

        Return _EntityIDType

    End Function

    Private Function CreateSimpleEntityIdType(Value As String) As WSOrlandoGateway.EntityIdType

        Dim _EntityIDType As New WSOrlandoGateway.EntityIdType

        Dim _EntityIdTypeIDValueList(0) As WSOrlandoGateway.EntityIdTypeIdValue
        Dim _EntityIdTypeIDValue As New WSOrlandoGateway.EntityIdTypeIdValue

        _EntityIdTypeIDValue.Value = Value

        _EntityIdTypeIDValueList(0) = _EntityIdTypeIDValue

        _EntityIDType.IdValue = _EntityIdTypeIDValueList

        Return _EntityIDType

    End Function

    Private Function CreateSimpleEntityIdType(Value As String, length As Integer) As WSOrlandoGateway.EntityIdType


        Dim _EntityIDType As New WSOrlandoGateway.EntityIdType

        Dim _EntityIdTypeIDValueList(0) As WSOrlandoGateway.EntityIdTypeIdValue
        Dim _EntityIdTypeIDValue As New WSOrlandoGateway.EntityIdTypeIdValue

        ' now change the value to the length value
        _EntityIdTypeIDValue.Value = Value.Substring(Value.Length - 5)

        _EntityIdTypeIDValueList(0) = _EntityIdTypeIDValue

        _EntityIDType.IdValue = _EntityIdTypeIDValueList

        Return _EntityIDType

    End Function

    Private Function CreatePostalAddressType(AddressType As WSOrlandoGateway.PostalAddressTypeType, AddressLine1 As String, AddressLine2 As String, AddressLine3 As String, AddressSuburb As String, AddressState As String, AddressPostCode As String, AddressCountry As String) As WSOrlandoGateway.PostalAddressType

        ' create the bits and assemble at the end
        Dim _PostalAddressType As New WSOrlandoGateway.PostalAddressType
        _PostalAddressType.type = AddressType
        If AddressSuburb.Length > 100 Then
            _PostalAddressType.Municipality = AddressSuburb.Substring(0, 100)            ' there are some that are incorrect 
        Else
            _PostalAddressType.Municipality = AddressSuburb
        End If
        _PostalAddressType.PostalCode = AddressPostCode
        _PostalAddressType.CountryCode = AddressCountry

        Dim _PostalAddressRegion(0) As String
        _PostalAddressRegion(0) = AddressState
        _PostalAddressType.Region = _PostalAddressRegion


        _PostalAddressType.DeliveryAddress = New WSOrlandoGateway.PostalAddressTypeDeliveryAddress
        Dim DeliveryAddressList(3) As String
        DeliveryAddressList(0) = AddressLine1
        DeliveryAddressList(1) = AddressLine2
        DeliveryAddressList(2) = AddressLine3
        _PostalAddressType.DeliveryAddress.AddressLine = DeliveryAddressList


        '            ''.PostalAddress.type = PostalAddressTypeType.streetAddress
        '            '' note its a country code...  2 char...
        '            '.Address.PostalAddress.CountryCode = GetStringFromCell(_AddressDataRow, "CountryCode")
        '            '.PostalAddress.PostalCode = GetStringFromCell(_AddressDataRow, "PostCode")
        '            '.PostalAddress.Region(0) = GetStringFromCell(_AddressDataRow, "Country")
        '            '.PostalAddress.Municipality = GetStringFromCell(_AddressDataRow, "Suburb")

        '            '.PostalAddress.DeliveryAddress.AddressLine(0) = GetStringFromCell(_AddressDataRow, "AddressLine1")
        '            '.PostalAddress.DeliveryAddress.AddressLine(1) = GetStringFromCell(_AddressDataRow, "AddressLine2")
        '            '.PostalAddress.DeliveryAddress.AddressLine(2) = GetStringFromCell(_AddressDataRow, "AddressLine3")

        '            '' now phone and mobile
        '            ''.Telephone 

        '            'For Each _row As DataRow In ds.Tables(2).Rows

        '            '    Select Case GetStringFromCell(_row, "Title").ToLower
        '            '        Case Is = "mobile"
        '            '            .Mobile.Items(0) = GetStringFromCell(_row, "CommunicationValue")
        '            '            .Mobile.ItemsElementName(0) = ItemsChoiceType.FormattedNumber
        '            '        Case Is = "email"
        '            '            .InternetEmailAddress = GetStringFromCell(_row, "CommunicationValue")

        '            '    End Select
        '            'Next


        Return _PostalAddressType

    End Function

    Private Function CreatePersonNameType(FirstName As String, MiddleName As String, Surname As String, KnownAs As String, Salutation As String) As WSOrlandoGateway.PersonNameType

        ' create the bits and assemble at the end
        Dim _PersonNameType As New WSOrlandoGateway.PersonNameType

        Dim _GivenNameList(1) As String
        _GivenNameList(0) = FirstName
        _PersonNameType.GivenName = _GivenNameList

        If MiddleName <> "UNK" Then
            _PersonNameType.MiddleName = MiddleName
        End If

        Dim _SurnameList(1) As WSOrlandoGateway.PersonNameTypeFamilyName
        Dim _Surname As New WSOrlandoGateway.PersonNameTypeFamilyName
        _Surname.Value = Surname

        _SurnameList(0) = _Surname
        _PersonNameType.FamilyName = _SurnameList

        If MiddleName = String.Empty Then
            _PersonNameType.FormattedName = String.Format("{0} {1}", FirstName.Trim, Surname)
        Else
            If MiddleName <> "UNK" Then
                _PersonNameType.FormattedName = String.Format("{0} {1} {2}", FirstName.Trim, MiddleName, Surname)
            Else
                _PersonNameType.FormattedName = String.Format("{0} {1}", FirstName.Trim, Surname)
            End If
        End If

        _PersonNameType.PreferredGivenName = KnownAs
        _PersonNameType.LegalName = String.Format("{0} {1}", FirstName.Trim, Surname)

        Dim _PersonNameNameTypeAffixList(1) As WSOrlandoGateway.PersonNameTypeAffix
        Dim _PersonNameNameTypeAffix = New WSOrlandoGateway.PersonNameTypeAffix
        _PersonNameNameTypeAffix.Value = Salutation
        _PersonNameNameTypeAffix.type = WSOrlandoGateway.PersonNameTypeAffixType.formOfAddress
        _PersonNameNameTypeAffixList(0) = _PersonNameNameTypeAffix
        _PersonNameType.Affix = _PersonNameNameTypeAffixList

        Return _PersonNameType

    End Function

    Private Function CreateContactMethodType(EmailAddress As String, PhoneNumber As String, MobileNumber As String) As WSOrlandoGateway.EntityContactInfoType

        ' create the bits and assemble at the end
        Dim _EntityContactMethodType As New WSOrlandoGateway.EntityContactInfoType

        Dim _ContactMethodTypeList(0) As WSOrlandoGateway.ContactMethodType
        Dim _ContactMethodType As New WSOrlandoGateway.ContactMethodType

        _EntityContactMethodType.EntityName = ""
        _ContactMethodType.InternetEmailAddress = EmailAddress

        _ContactMethodType.Telephone = New WSOrlandoGateway.TelcomNumberType
        Dim _TelcomNumberTypeItemsList(0) As String
        Dim _TelephoneItemsChoiceTypeList(0) As WSOrlandoGateway.ItemsChoiceType
        _TelcomNumberTypeItemsList(0) = PhoneNumber
        _TelephoneItemsChoiceTypeList(0) = WSOrlandoGateway.ItemsChoiceType.FormattedNumber
        _ContactMethodType.Telephone.Items = _TelcomNumberTypeItemsList
        _ContactMethodType.Telephone.ItemsElementName = _TelephoneItemsChoiceTypeList

        _ContactMethodType.Mobile = New WSOrlandoGateway.MobileTelcomNumberType
        Dim _MobileNumberTypeItemsList(0) As String
        Dim _MobileItemsChoiceTypeList(0) As WSOrlandoGateway.ItemsChoiceType
        _MobileNumberTypeItemsList(0) = MobileNumber
        _MobileItemsChoiceTypeList(0) = WSOrlandoGateway.ItemsChoiceType.FormattedNumber
        _ContactMethodType.Mobile.Items = _MobileNumberTypeItemsList
        _ContactMethodType.Mobile.ItemsElementName = _MobileItemsChoiceTypeList

        _ContactMethodTypeList(0) = _ContactMethodType
        _EntityContactMethodType.ContactMethod = _ContactMethodTypeList


        Return _EntityContactMethodType

    End Function

    Private Function CreateRateType(RateType As String, Currency As String, RatePeriod As String, RateAmount As Double, RateTypeClass As String, EffectiveDate As Date) As WSOrlandoGateway.RatesType
        Dim _RateType As New WSOrlandoGateway.RatesType

        _RateType.rateType = RateType
        _RateType.rateStatus = "agreed"

        _RateType.Amount = New WSOrlandoGateway.RatesTypeAmount
        _RateType.Amount.currency = Currency
        _RateType.Amount.rateAmountPeriod = RatePeriod
        _RateType.Amount.Value = RateAmount

        _RateType.Multiplier = New WSOrlandoGateway.RatesTypeMultiplier
        _RateType.Multiplier.Value = 100
        _RateType.Multiplier.percentIndicator = False
        _RateType.Multiplier.percentIndicatorSpecified = True

        _RateType.Class = RateTypeClass

        _RateType.StartDate = EffectiveDate.ToString("yyyy-MM-ddZ")

        _RateType.CustomerRateClassification = CreateSimpleEntityIdType("Regular")

        Return _RateType

    End Function

    Private Function CreateReportedTimeIntervalType(IDValue As String, IntervalType As String, StartDate As Date, Duration As String, OrlandoProductCategory As String) As WSOrlandoGateway.TimeCardTypeReportedTimeTimeInterval

        '<TimeInterval type="Overtime">
        '<Id>
        '    <IdValue>String</IdValue>
        '</Id>
        '<StartDate>2013-10-03Z</StartDate>
        '<Duration>4</Duration>
        '</TimeInterval>

        Dim _ReportedTimeIntervalType As New WSOrlandoGateway.TimeCardTypeReportedTimeTimeInterval

        Select Case OrlandoProductCategory.ToLower
            Case Is = "regular"
                _ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Regular
            Case Is = "overtime"
                _ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Overtime
            Case Else
                _ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Shift
        End Select


        '        _ReportedTimeIntervalType.type = GetClassificationEnum(IntervalType)

        ' overtime codes contain 1.5, 2.0 and 2.5
        '        If IntervalType.Contains("1.5") Or IntervalType.Contains("2.0") Or IntervalType.Contains("2.5") Then
        '_ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Overtime ' = WSOrlandoGateway .TimeCardTypeReportedTimeTimeIntervalType.Overtime
        'End If

        'If IntervalType = "A1.0" Or IntervalType = "E1.0" Or IntervalType = "N1.0" Or IntervalType = "M1.0" Then
        '_ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Shift '  WSOrlandoGateway.TimeCardTypeReportedTimeTimeIntervalType.Shift
        'End If

        'If IntervalType = "T1.0" Or IntervalType = "H1.0" Or IntervalType = "D1.0" Then
        '_ReportedTimeIntervalType.type = WSOrlandoGateway.TimeIntervalType.Regular  '  WSOrlandoGateway.TimeCardTypeReportedTimeTimeIntervalType.Shift
        'End If

        If Not IsNothing(_ReportedTimeIntervalType.type) Then
            _ReportedTimeIntervalType.Id = CreateSimpleEntityIdType(IDValue, 5)

            _ReportedTimeIntervalType.StartDate = StartDate
            _ReportedTimeIntervalType.Duration = Duration
        End If

        '_ReportedTimeType.Items.rateType = RateType
        '_RateType.rateStatus = "agreed"

        '_RateType.Amount = New WSOrlandoGateway.RatesTypeAmount
        '_RateType.Amount.currency = Currency
        '_RateType.Amount.rateAmountPeriod = RatePeriod
        '_RateType.Amount.Value = RateAmount

        '_RateType.Multiplier = New WSOrlandoGateway.RatesTypeMultiplier
        '_RateType.Multiplier.Value = 100

        '_RateType.Class = RateTypeClass

        '_RateType.StartDate = EffectiveDate.ToString("yyyy-MM-ddZ")

        '_RateType.CustomerRateClassification = CreateSimpleEntityIdType("Regular")

        Return _ReportedTimeIntervalType

    End Function

    Private Function GetClassificationEnum(ProductCode As String) As WSOrlandoGateway.TimeIntervalType

        Dim _retval As WSOrlandoGateway.TimeIntervalType = Nothing ' WSOrlandoGateway.TimeIntervalType.Shift

        ' overtime codes contain 1.5, 2.0 and 2.5
        If ProductCode.Contains("1.5") Or ProductCode.Contains("2.0") Or ProductCode.Contains("2.5") Then
            _retval = WSOrlandoGateway.TimeIntervalType.Overtime ' = WSOrlandoGateway .TimeCardTypeReportedTimeTimeIntervalType.Overtime
        End If

        If ProductCode = "A1.0" Or ProductCode = "E1.0" Or ProductCode = "N1.0" Or ProductCode = "M1.0" Then
            _retval = WSOrlandoGateway.TimeIntervalType.Shift '  WSOrlandoGateway.TimeCardTypeReportedTimeTimeIntervalType.Shift
        End If

        If ProductCode = "T1.0" Or ProductCode = "H1.0" Or ProductCode = "D1.0" Or ProductCode = "MONTH1.0" Then
            _retval = WSOrlandoGateway.TimeIntervalType.Regular  '  WSOrlandoGateway.TimeCardTypeReportedTimeTimeIntervalType.Shift
        End If

        Return _retval

    End Function

    Private Function ConvertFromXml(Of T As Class)(ByRef str As String) As T

        Dim serializer As XmlSerializer = New XmlSerializer(GetType(T))
        Dim reader As StringReader = New StringReader(str)
        Dim c As T = TryCast(serializer.Deserialize(reader), T)
        Return c

    End Function

    Private Function ConvertObjectToXML(UserObject As Object) As String

        '     public string CreateXML(Object YourClassObject){    
        Dim xmlDoc As New Xml.XmlDocument '();   //Represents an XML document, 
        '// Initializes a new instance of the XmlDocument class.          
        Dim xmlSerializer = New XmlSerializer(UserObject.GetType())
        '// Creates a stream whose backing store is memory. 
        Using xmlStream As New MemoryStream
            xmlSerializer.Serialize(xmlStream, UserObject)
            xmlStream.Position = 0
            '//Loads the XML document from the specified string.
            xmlDoc.Load(xmlStream)
            Return xmlDoc.InnerXml
        End Using

    End Function

#End Region

#Region "General Methods"

    Private Function GetSystemSettings(SystemTypeName As String) As Dictionary(Of String, String)

        Dim _SystemSettings = New Dictionary(Of String, String)

        If _ApplicationSystemSettings.Count > 0 Then
            _SystemSettings = _ApplicationSystemSettings
        Else
            _SystemSettings = BOMObjectLibrary(SystemTypeName)
        End If

        Return _SystemSettings

    End Function

    Private Function ConnectToiOrlandoService(SystemSettings As System.Collections.Generic.Dictionary(Of String, String), UserName As String, Password As String) As WSOrlandoGateway.OrlandoSoapService

        '    // now we can connect to the candidate service 

        ' If ServicePointManager.SecurityProtocol <> Tls12 Then
        'Const _Tls12 As System.Security.Authentication.SslProtocols = DirectCast(&HC00, SslProtocols)
        'Const Tls12 As SecurityProtocolType = DirectCast(_Tls12, SecurityProtocolType)
        'ServicePointManager.SecurityProtocol = Tls12
        'End If
        ServicePointManager.SecurityProtocol = Tls12

        Dim RandstadOrlandoGateway As WSOrlandoGateway.OrlandoSoapService = New WSOrlandoGateway.OrlandoSoapService
        RandstadOrlandoGateway.Credentials = New System.Net.NetworkCredential(UserName, Password, Nothing)
        RandstadOrlandoGateway.Url = SystemSettings("OrlandoGateway") + SystemSettings("OrlandoSoapService")

        Return RandstadOrlandoGateway


    End Function

    Private Function ConnectToiOrlandoService(SystemSettings As System.Collections.Generic.Dictionary(Of String, String)) As WSOrlandoGateway.OrlandoSoapService

        ' If ServicePointManager.SecurityProtocol <> Tls12 Then
        ' Const _Tls12 As System.Security.Authentication.SslProtocols = DirectCast(&HC00, System.Security.Authentication.SslProtocols)
        ' Const Tls12 As SecurityProtocolType = DirectCast(_Tls12, SecurityProtocolType)
        ServicePointManager.SecurityProtocol = Tls12
        'End If


        '    // now we can connect to the candidate service 
        Dim RandstadOrlandoGateway As WSOrlandoGateway.OrlandoSoapService = New WSOrlandoGateway.OrlandoSoapService
        RandstadOrlandoGateway.Credentials = New System.Net.NetworkCredential(SystemSettings("OrlandoUserName"), SystemSettings("OrlandoPassword"), Nothing)
        RandstadOrlandoGateway.Url = SystemSettings("OrlandoGateway") + SystemSettings("OrlandoSoapService")

        Return RandstadOrlandoGateway

    End Function

    Private Function ConvertTextToRTF(TextToConvert As String) As String

        Dim _retVal As String = String.Empty

        If TextToConvert.TrimEnd.Length > 0 Then
            Dim _txtctrl As New System.Windows.Forms.RichTextBox
            _txtctrl.Text = TextToConvert
            _retVal = _txtctrl.Rtf
            _txtctrl = Nothing
        End If

        Return _retVal

    End Function

    Private Function ConvertRTFoText(TextToConvert As String) As String

        'Regex.Replace(rtfString, @"\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?", "");

        Dim _retVal As String = String.Empty

        If TextToConvert.TrimEnd.Length > 0 Then
            Dim _txtctrl As New System.Windows.Forms.RichTextBox
            _txtctrl.Rtf = TextToConvert
            _retVal = _txtctrl.Text
            _txtctrl = Nothing
        End If

        Return _retVal

    End Function

    Public Function ConvertCountryToSupplierID(Country As String) As String

        '-- according to the BS team this is always 001
        Return String.Concat(ConvertCountryToCode(Country).Trim, "_001")

    End Function

    Private Function ConvertCountryToCode(Country As String) As String

        Dim retval As String = String.Empty



        Select Case Country.ToLower
            Case Is = "australia"
                retval = "AU"
            Case Is = "new zealand"
                retval = "NZ"
            Case Is = "malaysia"
                retval = "MY"
            Case Is = "singapore"
                retval = "SG"
            Case Is = "hong hong"
                retval = "HK"
            Case Else  ' just return the value
                retval = Country
        End Select


        Return retval

    End Function

    Private Function ConvertCountryToCurrencyCode(Country As String) As String

        Dim retval As String = String.Empty

        Select Case Country.ToLower
            Case Is = "australia"
                retval = "AUD"
            Case Is = "new zealand"
                retval = "NZD"
            Case Is = "malaysia"
                retval = "MYR"
            Case Is = "singapore"
                retval = "SGD"
            Case Is = "Hong Kong"
                retval = "HKD"
            Case Else
                retval = "AUD"
        End Select

        ' need to be added
        '        Hong Kong
        '       Singapore()

        Return retval

    End Function

    Private Function ConvertCountryToCurrencyEnum(Country As String) As WSOrlandoGateway.CurrencyEnumType

        Dim retval As String = String.Empty

        Select Case Country.ToLower
            Case Is = "australia"
                retval = WSOrlandoGateway.CurrencyEnumType.AUD
            Case Is = "new zealand"
                retval = WSOrlandoGateway.CurrencyEnumType.NZD
            Case Is = "malaysia"
                retval = WSOrlandoGateway.CurrencyEnumType.MYR
            Case Is = "singapore"
                retval = WSOrlandoGateway.CurrencyEnumType.SGD
            Case Is = "Hong Kong"
                retval = WSOrlandoGateway.CurrencyEnumType.HKD
            Case Else
                retval = WSOrlandoGateway.CurrencyEnumType.AUD
        End Select

        ' need to be added
        '        Hong Kong
        '       Singapore()

        Return retval

    End Function

    Private Function FormatOrlandoDate(DateToFormat As Date) As String

        Return DateToFormat.ToString("s")  ' "yyyy-MM-ddThh:mm:ss+10:00")

    End Function

    Private Function GetConceptArea(OrganisationUnitReference As String) As WSOrlandoGateway.ServiceConceptType

        ' question is it meant to be searchandselction if perm

        Dim retval As WSOrlandoGateway.ServiceConceptType

        Select Case OrganisationUnitReference.ToLower
            Case Is = "staffing"
                retval = WSOrlandoGateway.ServiceConceptType.Staffing

                '.UserArea.ReferenceInformationAdditional.ServiceConcept = randstadorlando.ServiceConceptType.SearchSelection
                '.UserArea.ReferenceInformationAdditional.ServiceConcept = randstadorlando.ServiceConceptType.Professionals
                '.UserArea.ReferenceInformationAdditional.ServiceConcept = randstadorlando.ServiceConceptType.InhouseServices
            Case Is = "hr solutions"
                retval = WSOrlandoGateway.ServiceConceptType.HRSolutions
            Case Is = "professional"
                retval = WSOrlandoGateway.ServiceConceptType.Professionals
            Case Else

                retval = WSOrlandoGateway.ServiceConceptType.Staffing
        End Select


        '   Director()
        '   Education()
        '   Sales & Marketing
        '   HR Solutions
        '   Pharma()
        '   Legal()
        '   Resources()
        '   OHS & Risk Management
        '   Executive()
        '   HR Partners
        '   Supply Chain
        '   Professional()
        '   Client Bid Solutions
        '   Logistics & Supply Chain
        '   IT()
        '   NULL()
        '   Sourcing Centre
        '   Managed Solutions
        '   Infrastructure()
        '   Care()
        '   Commerce()
        '   UNK()
        '   HR Staffing
        '   Sourceright()
        '   Call Centre()
        '   Offset()
        '   Staffing()
        '   Business Support
        '   HR Professional
        '   Industrial()
        '   Mining()
        '   CPE()
        '   HR Consulting
        '   Retail()
        '   Banking & Finance
        '   Accounting()
        '   Sales, Marketing & Communications

        Return retval

    End Function

#End Region

#Region "Dataset Assistants"

    Private Function GetDoubleFromCell(row As DataRow, value As String) As Double
        Dim _Retval As Double = 0
        If Not IsDBNull(row(value)) Then
            _Retval = Convert.ToDouble(row(value))
        End If
        Return _Retval
    End Function

    Private Function GetLongFromCell(row As DataRow, value As String) As Long
        Dim _Retval As Long = 0
        If Not IsDBNull(row(value)) Then
            _Retval = Convert.ToInt64(row(value))
        End If
        Return _Retval
    End Function

    Private Function GetStringFromCell(row As DataRow, value As String) As String
        Dim _Retval As String = String.Empty
        If row.Table.Columns.Contains(value) Then
            If Not IsDBNull(row(value)) Then
            _Retval = row(value).ToString

            ' we cant send newline or carriage return characters into orlnado so we need to strip them out
            ' we can either IF ENDIF check or simply remove it...
            _Retval = _Retval.Replace(vbCr, String.Empty).Replace(vbLf, String.Empty)

        End If
        End If

        Return _Retval
    End Function

    Private Function GetBooleanFromCell(row As DataRow, value As String) As Boolean
        Dim _Retval As Boolean = False
        Try
            If Not IsDBNull(row(value)) Then
                If row(value).ToString.ToLower = "yes" Then
                    _Retval = True
                ElseIf row(value).ToString.ToLower = "no" Then
                    _Retval = False
                Else
                    _Retval = Convert.ToBoolean(row(value))
                End If
            End If
        Catch ex As Exception
            ' DO NOTHING...
        End Try

        Return _Retval
    End Function

    Private Function GetDateFromCell(row As DataRow, value As String) As DateTime
        Dim _Retval As DateTime = DateTime.MinValue
        If Not IsDBNull(row(value)) Then
            _Retval = Convert.ToDateTime(row(value))
            'Else
            '    _Retval = Nothing
        End If
        Return _Retval
    End Function

    Private Function IsCellValueSet(row As DataRow, value As String) As Boolean
        Dim _Retval As Boolean = False
        If Not IsDBNull(row(value)) Then
            If IsDate(row(value)) Then
                _Retval = True
            Else
                If row(value).ToString <> "0" And row(value).ToString.Length > 0 Then
                    _Retval = True
                End If
            End If

        End If
        Return _Retval
    End Function

    Private Function GetStringFromObject(objectitem As Object) As String
        Dim _Retval As String = String.Empty
        If Not IsNothing(objectitem) Then
            _Retval = objectitem.ToString
        End If
        Return _Retval
    End Function

#End Region


End Class


