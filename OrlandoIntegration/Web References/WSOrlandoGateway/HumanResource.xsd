<?xml version="1.0" encoding="utf-8"?>
<xsd:schema xmlns="http://ns.hr-xml.org/2007-04-15" elementFormDefault="qualified" targetNamespace="http://ns.hr-xml.org/2007-04-15" version="2007-04-15" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <xsd:import schemaLocation="../../W3C/xml.xsd" namespace="http://www.w3.org/XML/1998/namespace" />
  <xsd:include schemaLocation="../SEP/Resume.xsd" />
  <xsd:include schemaLocation="../SIDES/Rates.xsd" />
  <xsd:include schemaLocation="../SIDES/ContactInfo.xsd" />
  <xsd:include schemaLocation="../SIDES/ResourceScreening.xsd" />
  <xsd:include schemaLocation="../SIDES/StaffingPositionHeader.xsd" />
  <xsd:include schemaLocation="../SIDES/Localizations.xsd" />
  <xsd:include schemaLocation="UserArea-HumanResource.xsd" />
  <xsd:include schemaLocation="UserArea-ReferenceInformation.xsd" />
  <xsd:annotation>
    <xsd:documentation>
            "Copyright The HR-XML Consortium. All Rights Reserved. http://www.hr-xml.org"

            Name: HumanResource.xsd
            Status: Recommendation
            Date this version: 2007-04-15
            Purpose: Defines the HumanResource Schema
            Author(s): SIDES Workgroup
            Documentation: HumanResource.html

            2006-July:
            - Added Reference Id's to match other SIDES schema: MasterOrderId, TimeCardId, InvoiceId, and
            BillToEntityId.
            - Added staffing information to PositionHeader.
            - Added Social Insurance structure.

            2004-June: The AvailabilityDate was corrected to remove an ambiguous content model. The unbounded xsd:choice
            was replaced with a simple xsd:sequence. In order to allow for multiple AvailabilityStartDate and
            AvailabilityEndDate, the parent element itself, AvailabilityDate, was made repeatable.

            This is a technically non-backwards compatible bug fix. However, if all three elements are used with no more
            than a single occurrence, then it is backwardly compatible. It is only with multiple occurrences that
            compatibility becomes a problem.

            Previous structure with ambiguous content model:

            xsd:element name="AvailabilityDate" minOccurs="0"
            xsd:complexType
            xsd:choice maxOccurs="unbounded"
            xsd:element name="AvailabilityStartDate" type="AnyDateTimeNkType"/
            xsd:element name="AvailabilityEndDate" type="AnyDateTimeNkType"/
            /xsd:choice
            /xsd:complexType
            /xsd:element

            New structure:
            xsd:element name="AvailabilityDate" minOccurs="0" maxOccurs="unbounded"
            xsd:complexType
            xsd:sequence
            xsd:element name="AvailabilityStartDate" type="AnyDateTimeNkType"/
            xsd:element name="AvailabilityEndDate" type="AnyDateTimeNkType" minOccurs="0"/
            /xsd:sequence
            /xsd:complexType
            /xsd:element

            2004-May: Made changes and additions based on global SIDES feedback. See scope section in HumanResource.doc
            for details.

            Terms of license can be found in license.txt.

        </xsd:documentation>
  </xsd:annotation>
  <xsd:complexType name="HumanResourceType">
    <xsd:sequence>
      <xsd:element name="HumanResourceId" type="EntityIdType">
        <xsd:annotation>
          <xsd:documentation>Unique identifier of the human resource within your company.</xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="HumanResourceStatus">
        <xsd:complexType>
          <xsd:attribute name="status" type="StatusType" use="required">
            <xsd:annotation>
              <xsd:documentation>Several statuses allowed, check the Orlando documentation for when to use what status.</xsd:documentation>
            </xsd:annotation>
          </xsd:attribute>
          <xsd:attribute name="statusChangeReason">
            <xsd:annotation>
              <xsd:documentation>Select the right status change reason</xsd:documentation>
            </xsd:annotation>
            <xsd:simpleType>
              <xsd:restriction base="xsd:string">
                <xsd:enumeration value="Tasks on assignment not satisfying" />
                <xsd:enumeration value="Type of contract not satisfying" />
                <xsd:enumeration value="No Interest in temporary staffing" />
                <xsd:enumeration value="Salary not satisfying" />
                <xsd:enumeration value="Alternative job offer" />
                <xsd:enumeration value="Commuting distance" />
                <xsd:enumeration value="Other" />
              </xsd:restriction>
            </xsd:simpleType>
          </xsd:attribute>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name="ReferenceInformation">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element name="StaffingSupplierId" type="EntityIdType">
              <xsd:annotation>
                <xsd:documentation>The code assigned to your Operating Company by Miami. In some countries this code will be given to a technical party responsible for delivering data for several labels. See appendix referenceinformation.
                                </xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element name="StaffingCustomerId" type="EntityIdType">
              <xsd:annotation>
                <xsd:documentation>Unique identifier for the customer within your company.
                                </xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element minOccurs="0" name="OrderId" type="EntityIdType">
              <xsd:annotation>
                <xsd:documentation>Ordernumber of StaffingOrder type RFQ.</xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element name="StaffingSupplierOrgUnitId" type="EntityIdType">
              <xsd:annotation>
                <xsd:documentation>The divisioncode you are sending to Miami. See appendix referenceinformation. </xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element minOccurs="0" name="StaffingCustomerOrgUnitId" type="EntityIdType">
              <xsd:annotation>
                <xsd:documentation>Unique identifier for the organization unit of the customer within your company.</xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element name="UserArea" type="UserAreaReferenceInformationType" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name="ResourceInformation">
        <xsd:complexType>
          <xsd:sequence>
            <xsd:element minOccurs="0" name="ResourceType">
              <xsd:complexType>
                <xsd:attribute name="independentContractor" type="xsd:boolean" use="required" />
                <xsd:attribute name="payrolledEmployee" type="xsd:boolean" use="required" />
              </xsd:complexType>
            </xsd:element>
            <xsd:element ref="PersonName" />
            <xsd:element ref="EntityContactInfo">
              <xsd:annotation>
                <xsd:documentation>Leave empty. For project Orlando it is not needed but must be there according to HR-XML.
                                </xsd:documentation>
              </xsd:annotation>
            </xsd:element>
            <xsd:element ref="PostalAddress" />
          </xsd:sequence>
        </xsd:complexType>
      </xsd:element>
      <xsd:element name="Profile" type="xsd:string">
        <xsd:annotation>
          <xsd:documentation>Leave empty. For project Orlando it is not needed but must be there according to HR-XML.
                    </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="Preferences" type="xsd:string">
        <xsd:annotation>
          <xsd:documentation>Leave empty. For project Orlando it is not needed but must be there according to HR-XML.
                    </xsd:documentation>
        </xsd:annotation>
      </xsd:element>
      <xsd:element name="UserArea" type="UserAreaHumanResourceType" />
    </xsd:sequence>
    <xsd:attribute ref="xml:lang" />
  </xsd:complexType>
  <xsd:element name="HumanResource" type="HumanResourceType" />
  <xsd:simpleType name="KnownStatusType">
    <xsd:restriction base="xsd:string">
      <xsd:enumeration value="new">
        <xsd:annotation>
          <xsd:documentation>A human resource that is offered on a Staffing Order.</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="revised">
        <xsd:annotation>
          <xsd:documentation>A change to the details of a human resource.</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="rejected">
        <xsd:annotation>
          <xsd:documentation>A human resource that is being rejected by the customer of a Staffing Order.</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="accepted">
        <xsd:annotation>
          <xsd:documentation>A human resource that is being accepted by the customer of a Staffing Order.</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="withdrawn">
        <xsd:annotation>
          <xsd:documentation>A human resource who withdraws from a Staffing Order.</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="x:application">
        <xsd:annotation>
          <xsd:documentation>A human resource who applies to Randstad on a Staffing Order</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
      <xsd:enumeration value="x:assigned">
        <xsd:annotation>
          <xsd:documentation>A human resource is assign to the Staffing Order</xsd:documentation>
        </xsd:annotation>
      </xsd:enumeration>
    </xsd:restriction>
  </xsd:simpleType>
  <xsd:simpleType name="StatusType">
    <xsd:union memberTypes="KnownStatusType" />
  </xsd:simpleType>
</xsd:schema>