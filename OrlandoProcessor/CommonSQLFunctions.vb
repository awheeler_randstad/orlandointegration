﻿Imports System.Data.SqlClient
Imports System.Xml.Serialization
Imports System.IO

Module CommonSQLFunctions

    Public _DataWarehouseConnection As SqlConnection
    Public _IntegrationConnection As SqlConnection
  

#Region "SQL Methods"

    Public Function GetSqlCommand(DBConnection As SqlConnection, SQLCommand As String, CommandType As CommandType) As SqlClient.SqlCommand

        ' first lets see if there any search items to be checked
        Dim _cmd As SqlClient.SqlCommand = DBConnection.CreateCommand()
        _cmd.CommandType = CommandType
        _cmd.CommandText = SQLCommand

        Return _cmd

    End Function

    Public Function ExecuteProc(ByRef cmd As SqlClient.SqlCommand) As Integer

        Dim _RetVal As Integer = 0
        Try

            If cmd.Connection.State <> ConnectionState.Open Then
                cmd.Connection.Open()
            End If

            _RetVal = cmd.ExecuteNonQuery()

            cmd.Connection.Close()

        Catch ex As Exception

            Throw New Exception(String.Concat("Error in method: ExecuteProc(", cmd.CommandText, ")"), ex)

        Finally
            'Nothing to do here...
            'The using should close the connection and command
            If cmd.Connection.State = ConnectionState.Open Then
                cmd.Connection.Close()
            End If

        End Try

        Return _RetVal

    End Function

    Public Function GetDataSet(cmd As SqlClient.SqlCommand) As DataSet

        Dim _RetDs As New DataSet

        Try
            Using da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter(cmd)

                If cmd.Connection.State <> ConnectionState.Open Then
                    cmd.Connection.Open()
                End If

                da.Fill(_RetDs)

                cmd.Connection.Close()

            End Using

        Catch ex As Exception

            Throw New Exception(String.Concat("Error in method: GetDataSet(", cmd.CommandText, ")"), ex)

        Finally

            If cmd.Connection.State = ConnectionState.Open Then
                cmd.Connection.Close()
            End If

        End Try

        Return _RetDs

    End Function

    Public Function GetDataSet(DBConnection As SqlConnection, sCommand As String, sType As CommandType, sParmName As String, sParmValue As String) As DataSet
        Dim cmd As SqlCommand

        cmd = DBConnection.CreateCommand()
        Dim da As New SqlDataAdapter(cmd)
        Dim ds As New DataSet()
        If DBConnection.State <> ConnectionState.Open Then
            DBConnection.Open()
        End If


        cmd.CommandType = sType
        cmd.CommandText = sCommand

        If sParmName.Length > 0 Then
            cmd.Parameters.AddWithValue(sParmName, sParmValue)
        End If

        da.Fill(ds)
        Return ds

        DBConnection.Close()

    End Function

    Public Function GetSqlXmlFromObject(thisObject As Object) As System.Data.SqlTypes.SqlXml

        Dim factory As New XmlSerializerFactory
        Dim xmlString As String
        Using twriter As TextWriter = New StringWriter
            Dim xs As XmlSerializer = factory.CreateSerializer(thisObject.GetType)
            xs.Serialize(twriter, thisObject)
            xmlString = twriter.ToString
        End Using

        Return New SqlTypes.SqlXml(New Xml.XmlTextReader(xmlString, Xml.XmlNodeType.Document, Nothing))

    End Function

    Public Function SplitDataTable(ByVal tableData As DataTable, ByVal max As Integer) As DataSet

        Dim newDs As DataSet = New DataSet()

        Dim i As Integer = 0
        Dim j As Integer = 1

        Dim newtablename As String = tableData.TableName

        If (newtablename = String.Empty) Then
            newtablename = "newTable"
        End If

        Dim countOfRows As Integer = tableData.Rows.Count
        Dim newDt As DataTable = tableData.Clone()





        newDt.TableName = newtablename & "_" & j.ToString
        newDt.Clear()

        For Each row As DataRow In tableData.Rows
            Dim newRow As DataRow = newDt.NewRow()
            newRow.ItemArray = row.ItemArray
            newDt.Rows.Add(newRow)
            i += 1
            countOfRows -= 1

            If i = max Then
                newDs.Tables.Add(newDt)
                j += 1
                newDt = tableData.Clone()
                newDt.TableName = newtablename & "_" + j.ToString
                newDt.Clear()
                i = 0
            End If

            If countOfRows = 0 AndAlso i < max Then
                newDs.Tables.Add(newDt)
                j += 1
                newDt = tableData.Clone()
                newDt.TableName = newtablename & "_" + j.ToString
                newDt.Clear()
                i = 0
            End If
        Next

        Return newDs

    End Function
#End Region


End Module

