﻿Public Class Form1


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

#Region "Staffing order"

    Function UILoadVacancy(VacancyID As Long) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetOrderRequestDetails", CommandType.StoredProcedure, "@Vacancy_SK", VacancyID)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdateVacancyToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadVacancy: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView1.SelectedRows(0)

        Dim dr As Long = Convert.ToInt64(selectedRow.Cells(0).Value)

        UILoadVacancy(dr)

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from dbo.Orlando_OrderRequest WHERE isPending = 1", CommandType.Text, "", "")
        DataGridView1.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from EDW.dbo.D_Vacancy WHERE Vacancy_SK = @Vacancy_SK", CommandType.Text, "@Vacancy_SK", TextBox1.Text)
        DataGridView1.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click

        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView1.SelectedRows(0)

        Dim dr As String = Convert.ToInt64(selectedRow.Cells(0).Value)

        Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)
        _cmd2.Parameters.AddWithValue("@EntityID", dr)
        _cmd2.Parameters.AddWithValue("@EntityName", "DWVacancy")

        ExecuteProc(_cmd2)

    End Sub
#End Region

#Region "Booking"

    Function UILoadBooking(Vacancy_SK As Long) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetBookingDetails", CommandType.StoredProcedure, "@Vacancy_SK", Vacancy_SK)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdateBookingToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadCandidate: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Function

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from Orlando.dbo.Orlando_Assignment WHERE Vacancy_SK = @Vacancy_SK", CommandType.Text, "@Vacancy_SK", TextBox2.Text)
        DataGridView3.DataSource = ds.Tables(0)

    End Sub

#End Region
  

#Region "Candidate"


    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click


        Dim ds = GetDataSet(_DataWarehouseConnection, "select top 10 * from Orlando.dbo.Orlando_SuccessfulApplicant WHERE isPending = 1", CommandType.Text, "", "")
        DataGridView2.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView2.SelectedRows(0)

        Dim dr As Long = Convert.ToInt64(selectedRow.Cells(0).Value)

        UILoadCandidate(dr)

    End Sub

    Function UILoadCandidate(Vacancy_SK As Long) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetSuccessfulCandidateDetails", CommandType.StoredProcedure, "@Vacancy_SK", Vacancy_SK)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdateHumanResourceToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadCandidate: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function

    Function UILoadInvoice(InvoiceNumber As String) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetInvoiceDetails", CommandType.StoredProcedure, "@InvoiceNumber", InvoiceNumber)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdateInvoiceToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try


    End Function


#End Region




    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from Orlando.dbo.Orlando_Assignment WHERE IsPending = 1 AND WorkType = 'TEMP'", CommandType.Text, "", "")
        DataGridView3.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click

        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView3.SelectedRows(0)

        Dim dr As String = Convert.ToInt64(selectedRow.Cells(0).Value)

        UILoadBooking(dr)

    End Sub


#Region "Production Tab"

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click

        Dim retval As Long = 0

        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0
        Dim sUpdate As String = ""

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetOrderRequestsUI", CommandType.StoredProcedure)
        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration
            iRowcount = ds.Tables(0).Rows.Count
            For Each _Row In ds.Tables(0).Rows
                Try
                    icounter += 1
                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetOrderRequestDetails", CommandType.StoredProcedure, "@Vacancy_SK", _Row("Vacancy_SK").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try
                        Dim _String As String = _OrlandoGateway.UpdateVacancyToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        '      MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If


    End Sub

#End Region





#Region "Placements"

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from Orlando.dbo.Orlando_Assignment WHERE IsPending = 1 AND WorkType = 'PERM'", CommandType.Text, "", "")
        DataGridView4.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from EDW.dbo.D_Vacancy WHERE Vacancy_SK = @Vacancy_SK", CommandType.Text, "@Vacancy_SK", TextBox3.Text)
        DataGridView4.DataSource = ds.Tables(0)

    End Sub

    Function UILoadPlacement(Vacancy_SK As Long) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetPlacementDetails", CommandType.StoredProcedure, "@Vacancy_SK", Vacancy_SK)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdatePlacementToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadPlacement: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Function


    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click

        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView4.SelectedRows(0)

        Dim dr As String = Convert.ToInt64(selectedRow.Cells(0).Value)

        UILoadPlacement(dr)

    End Sub

#End Region

  
 


  

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click


        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView4.SelectedRows(0)

        Dim dr As String = Convert.ToInt64(selectedRow.Cells(0).Value)

        Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)
        _cmd2.Parameters.AddWithValue("@EntityID", dr)
        _cmd2.Parameters.AddWithValue("@EntityName", "DWPlacement")

        ExecuteProc(_cmd2)

    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click



        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView3.SelectedRows(0)

        Dim dr As String = Convert.ToInt64(selectedRow.Cells(0).Value)

        Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)
        _cmd2.Parameters.AddWithValue("@EntityID", dr)
        _cmd2.Parameters.AddWithValue("@EntityName", "DWBooking")

        ExecuteProc(_cmd2)

    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click


        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from Orlando.dbo.Orlando_SuccessfulApplicant WHERE Vacancy_SK = @Vacancy_SK", CommandType.Text, "@Vacancy_SK", TextBox4.Text)
        DataGridView2.DataSource = ds.Tables(0)

    End Sub

#Region "Timesheets"

    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from Orlando.dbo.Orlando_Timesheet WHERE IsPending = 1 AND WorkType = 'TEMP'", CommandType.Text, "", "")
        DataGridView5.DataSource = ds.Tables(0)


    End Sub
    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click


        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView5.SelectedRows(0)

        Dim dr As String = selectedRow.Cells("TimesheetNo").Value.ToString

        UILoadTimesheet(dr)

    End Sub

    Function UILoadTimesheet(TimesheetCode As String) As String

        Dim ds = GetDataSet(_DataWarehouseConnection, "dbo.spGetTimesheetDetails", CommandType.StoredProcedure, "@TimesheetCode", TimesheetCode)

        Dim _StringWriter = New IO.StringWriter()
        ds.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
        Dim _EntityXML As String = _StringWriter.ToString()

        Try
            Dim _String As String = _OrlandoGateway.UpdateTimecardToOrlando(_EntityXML)
            MessageBox.Show(String.Format("UILoadTimesheet: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Function

#End Region

  
    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "  select * from edw.dbo.F_Timesheet_Product_Summary where TimesheetNo = @TimesheetCode", CommandType.Text, "@TimesheetCode", TextBox5.Text)
        DataGridView5.DataSource = ds.Tables(0)

    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click


        Dim selectedRow As DataGridViewRow
        selectedRow = DataGridView6.SelectedRows(0)

        Dim dr As String = selectedRow.Cells(0).Value.ToString

        UILoadInvoice(dr)


    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "  select top 1000 * from Orlando.dbo.Orlando_Invoice where IntegrationDate is null", CommandType.Text, "", "")
        DataGridView6.DataSource = ds.Tables(0)


    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click

        Dim ds = GetDataSet(_DataWarehouseConnection, "select * from dbo.Orlando_Invoice where InvoiceCode = @InvoiceCode", CommandType.Text, "@InvoiceCode", TextBox6.Text)
        DataGridView6.DataSource = ds.Tables(0)


    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click

        ProcessOpenInvoiceData()

    End Sub

    Private Sub ProcessInvoices()

        Dim retval As Long = 0
        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0

        LogMessage("Starting... ProcessInvoices")

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetInvoicesUI", CommandType.StoredProcedure)
        _cmd.Parameters.AddWithValue("@FromDate", DateTimePickerFrom.Value)
        _cmd.Parameters.AddWithValue("@ToDate", DateTimePickerTo.Value)

        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration

            iRowcount = ds.Tables(0).Rows.Count
            ProgressBar1.Maximum = iRowcount
            ProgressBar1.Minimum = 1
            LogMessage(String.Format("Processing... {0} records", iRowcount.ToString))

            For Each _Row In ds.Tables(0).Rows
                Try
                    icounter += 1
                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetInvoiceDetails", CommandType.StoredProcedure, "@InvoiceNumber", _Row("InvoiceCode").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try

                        Dim _String As String = _OrlandoGateway.UpdateInvoiceToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        ListView1.Items.Add(String.Format("{0}:{1}", _Row("InvoiceCode").ToString, ex.Message))
                        LogMessage(String.Format("{0}:{1}", _Row("InvoiceCode").ToString, ex.Message))


                        Application.DoEvents()
                    End Try

                    If (icounter Mod 10) = 0 Then
                        ProgressBar1.Value = icounter
                        LabelProgress.Text = String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString)
                        LogMessage(String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString))

                        Application.DoEvents()
                    End If

                Catch ex As Exception
                    LogMessage(String.Format("{0}:{1}", _Row("InvoiceCode").ToString, ex.Message))
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If

        MessageBox.Show("done")

    End Sub

    Private Sub ProcessTimesheets()

        Dim retval As Long = 0
        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0

        LogMessage("Starting... ProcessTimesheets")

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetTimesheetsUI", CommandType.StoredProcedure)
        _cmd.Parameters.AddWithValue("@FromDate", DateTimePickerFrom.Value)
        _cmd.Parameters.AddWithValue("@ToDate", DateTimePickerTo.Value)

        LabelProgress.Text = "processing..."
        Application.DoEvents()

        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration

            iRowcount = ds.Tables(0).Rows.Count
            ProgressBar1.Maximum = iRowcount
            ProgressBar1.Minimum = 1
            LogMessage(String.Format("Processing... {0} records", iRowcount.ToString))



            For Each _Row In ds.Tables(0).Rows

                Try
                    icounter += 1

                    LabelProgress.Text = String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString)
                    LogMessage(String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString))

                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetTimesheetDetails", CommandType.StoredProcedure, "@TimesheetCode", _Row("TimesheetCode").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try

                        Dim _String As String = _OrlandoGateway.UpdateTimecardToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        ListView1.Items.Add(String.Format("{0}:{1}", _Row("TimesheetCode").ToString, ex.Message))
                        LogMessage(String.Format("{0}:{1}", _Row("TimesheetCode").ToString, ex.Message))


                        Application.DoEvents()
                    End Try

                    If (icounter Mod 10) = 0 Then
                        ProgressBar1.Value = icounter
                        LabelProgress.Text = String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString)
                        LogMessage(String.Format("{0} of {1}", icounter.ToString, iRowcount.ToString))

                        Application.DoEvents()
                    End If

                Catch ex As Exception
                    LogMessage(String.Format("{0}:{1}", _Row("TimesheetCode").ToString, ex.Message))
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If

        MessageBox.Show("done")

    End Sub


    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click

        Dim retval As Long = 0

        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0
        Dim sUpdate As String = ""

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetBookingsUI", CommandType.StoredProcedure)
        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration
            iRowcount = ds.Tables(0).Rows.Count
            For Each _Row In ds.Tables(0).Rows
                Try
                    icounter += 1
                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetBookingDetails", CommandType.StoredProcedure, "@Vacancy_SK", _Row("Vacancy_SK").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try
                        Dim _String As String = _OrlandoGateway.UpdateBookingToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format ("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        '      MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If



    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click

        Dim retval As Long = 0

        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0
        Dim sUpdate As String = ""

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetPlacementsUI", CommandType.StoredProcedure)
        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration
            iRowcount = ds.Tables(0).Rows.Count
            For Each _Row In ds.Tables(0).Rows
                Try
                    icounter += 1
                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetPlacementDetails", CommandType.StoredProcedure, "@Vacancy_SK", _Row("Vacancy_SK").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try
                        Dim _String As String = _OrlandoGateway.UpdatePlacementToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format ("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        '      MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click

        Dim retval As Long = 0

        Dim icounter As Int64 = 0
        Dim iRowcount As Int64 = 0
        Dim sUpdate As String = ""

        ' get the order requests 
        Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetTimesheetsUI", CommandType.StoredProcedure)
        Dim ds As DataSet = GetDataSet(_cmd)

        If ds.Tables(0).Rows.Count > 0 Then
            ' lets send it to integration
            iRowcount = ds.Tables(0).Rows.Count
            For Each _Row In ds.Tables(0).Rows
                Try
                    icounter += 1
                    Dim ds2 = GetDataSet(_DataWarehouseConnection, "dbo.spGetTimesheetDetails", CommandType.StoredProcedure, "@TimesheetCode", _Row("TimesheetCode").ToString)

                    Dim _StringWriter = New IO.StringWriter()
                    ds2.WriteXml(_StringWriter, XmlWriteMode.WriteSchema)
                    Dim _EntityXML As String = _StringWriter.ToString()

                    Try
                        Dim _String As String = _OrlandoGateway.UpdateTimecardToOrlando(_EntityXML)
                        '                 MessageBox.Show(String.Format ("UILoadInvoice: Successful.  Message: {0}", _String), "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch ex As Exception
                        '      MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)


                End Try
            Next

        End If


    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs) Handles Button26.Click

    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs) Handles Button30.Click

        Select Case ComboBoxType.Text
            Case Is = "Invoices"
                ProcessInvoices()
            Case Is = "Timesheets"
                ProcessTimesheets()

            Case Else

                MessageBox.Show("Not implemented")
        End Select



    End Sub
End Class
