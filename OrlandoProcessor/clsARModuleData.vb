﻿Public Class clsARModuleData


    Public OpenInvoiceAmountFormatVersion As String
    Public OpenInvoiceAmountDate As String
    Public TotalNoOfOpenInvoices As String
    Public TotalAmountOfOpenInvoices As String
    Public SupplierId As String
    Public LegalEntityCode As String
    Public LegalEntity As String
    Public CustomerId As String
    Public CustomerName As String
    Public CustomerBillingAddress_CountryCode As String
    Public CustomerBillingAddress_StateProvince As String
    Public CustomerBillingAddress_PostalCode As String
    Public CustomerBillingAddress_Municipality As String
    Public CustomerBillingAddress_BuildingNumber As String
    Public CustomerBillingAddress_StreetAddress As String
    Public CustomerContractAddress_CountryCode As String
    Public CustomerContractAddress_StateProvince As String
    Public CustomerContractAddress_PostalCode As String
    Public CustomerContractAddress_Municipality As String
    Public CustomerContractAddress_BuildingNumber As String
    Public CustomerContractAddress_StreetAddress As String
    Public CustomerTelephoneNumber As String
    Public CustomerMobileTelephoneNumber As String
    Public CustomerEmailAddress As String
    Public LocalClientIdentifier As String
    Public CustomerLocalRating As String
    Public AssignAbilityStatus As String
    Public PaymentTerm As String
    Public PaymentMethod As String
    Public CustomerLanguageCode As String
    Public CustomerDunningFlow As String
    Public CustomerNote As String
    Public CustomerDetailNote As String
    Public CustomerAdditionalField As String
    Public InvoiceId As String
    Public InvoiceDescription As String
    Public DocumentDate As String
    Public OriginalOpenAmount As String
    Public OriginalOpenAmount_AttributeCurrency As String
    Public OriginalOpenVATAmount As String
    Public OriginalOpenVATAmount_AttributeCurrency As String
    Public OpenInvoiceAmount As String
    Public OpenInvoiceAmount_AttributeCurrency As String
    Public OpenInvoiceVATAmount As String
    Public OpenInvoiceVATAmount_AttributeCurrency As String
    Public DueDate As String
    Public InDispute As String
    Public SellInvoice As String
    Public DocumentType As String
    Public ParentGuarentee As String
    Public InvoiceImageFileName As String
    Public OptionalField1 As String
    Public OptionalField2 As String
    Public OptionalField3 As String
    Public OptionalField4 As String
    Public OptionalField5 As String
    Public OptionalField6 As String
    Public OptionalField7 As String
    Public OptionalField8 As String
    Public MessageId As String
    Public MessageDateTimeStamp As String



End Class
