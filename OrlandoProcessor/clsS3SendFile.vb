﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports Amazon
Imports Amazon.S3
Imports Amazon.S3.Transfer
Imports Amazon.S3.Model

Public Class AmazonUploader

    Public Sub sendMyFileToS3(localFilePath As String, s3BucketAndPath As String, fileNameInS3 As String)

        Dim client = New AmazonS3Client()

        Try
            Dim putRequest = New PutObjectRequest()
            putRequest.FilePath = localFilePath
            putRequest.ContentType = "text/plain"
            putRequest.Key = fileNameInS3
            putRequest.BucketName = String.Format(s3BucketAndPath)

            Dim response As PutObjectResponse = client.PutObject(putRequest)

        Catch amazonS3Exception As AmazonS3Exception
            Throw New Exception("Error occurred: " + amazonS3Exception.Message)
        End Try

    End Sub


End Class


