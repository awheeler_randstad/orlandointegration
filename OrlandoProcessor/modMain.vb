﻿Imports System.Xml.Serialization
Imports System.IO
Imports System.Text
Imports ICSharpCode.SharpZipLib.Zip
Imports ICSharpCode.SharpZipLib.Core
Imports System.Data.SqlClient

Module modMain

    Public _OrlandoGateway As OrlandoBO.OrlandoBO

    Dim _OrlandoSystemSettings As Dictionary(Of String, String)

    Public myLogListener As TextWriterTraceListener
    Public _Logging As Boolean = False


    Sub Main()

        ' get the command line args...
        Dim CommandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String) = My.Application.CommandLineArgs

        _Logging = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("Logging"))
        LogMessage("Application Started...")

        _DataWarehouseConnection = New SqlClient.SqlConnection(My.Settings.DatawWarehouseConn) 'System.Configuration.ConfigurationManager.ConnectionStrings("DataWarehouseConn").ConnectionString)
        _IntegrationConnection = New SqlClient.SqlConnection(My.Settings.IntegrationConn)

        '  connstringbuilder.ConnectionString) 
        ' System.Configuration.ConfigurationManager.ConnectionStrings("iProfileDB").ConnectionString)
        LogMessage(String.Format("Connected to DataWarehouse: {0}", _DataWarehouseConnection.DataSource))
        LogMessage(String.Format("Connected to Integration Database: {0}", _IntegrationConnection.DataSource))

        If CommandLineArgs.Count > 0 Then
            If CommandLineArgs.Contains("/c") Then
                ' we are running in console mode
                ' to be completed...   run the process

                RunProcess()
            End If

            If CommandLineArgs.Contains("/o") Then
                ' we are running in console mode
                ' to be completed...   run the process

                _OrlandoSystemSettings = New Dictionary(Of String, String)

                _OrlandoSystemSettings.Add("OrlandoGateway", System.Configuration.ConfigurationManager.AppSettings("OrlandoGateway"))
                _OrlandoSystemSettings.Add("OrlandoSoapService", System.Configuration.ConfigurationManager.AppSettings("OrlandoSoapService"))
                _OrlandoSystemSettings.Add("OrlandoUserName", System.Configuration.ConfigurationManager.AppSettings("OrlandoUserName"))
                _OrlandoSystemSettings.Add("OrlandoPassword", System.Configuration.ConfigurationManager.AppSettings("OrlandoPassword"))

                _OrlandoGateway = New OrlandoBO.OrlandoBO(_OrlandoSystemSettings)

                ProcessOpenInvoiceData()
            End If

            If CommandLineArgs.Contains("/i") Then
                ' we are running in console mode
                ' to be completed...   run the process

                _OrlandoSystemSettings = New Dictionary(Of String, String)

                _OrlandoSystemSettings.Add("OrlandoGateway", System.Configuration.ConfigurationManager.AppSettings("OrlandoGateway"))
                _OrlandoSystemSettings.Add("OrlandoSoapService", System.Configuration.ConfigurationManager.AppSettings("OrlandoSoapService"))
                _OrlandoSystemSettings.Add("OrlandoUserName", System.Configuration.ConfigurationManager.AppSettings("OrlandoUserName"))
                _OrlandoSystemSettings.Add("OrlandoPassword", System.Configuration.ConfigurationManager.AppSettings("OrlandoPassword"))

                _OrlandoGateway = New OrlandoBO.OrlandoBO(_OrlandoSystemSettings)

                ProcessOpenInvoiceITPRData()

                'ProcessOpenInvoiceDataITPR()
            End If

            If CommandLineArgs.Contains("/s") Then
                ' we are running in console mode
                ' to be completed...   run the process

                _OrlandoSystemSettings = New Dictionary(Of String, String)

                _OrlandoSystemSettings.Add("OrlandoGateway", System.Configuration.ConfigurationManager.AppSettings("OrlandoGateway"))
                _OrlandoSystemSettings.Add("OrlandoSoapService", System.Configuration.ConfigurationManager.AppSettings("OrlandoSoapService"))
                _OrlandoSystemSettings.Add("OrlandoUserName", System.Configuration.ConfigurationManager.AppSettings("OrlandoUserName"))
                _OrlandoSystemSettings.Add("OrlandoPassword", System.Configuration.ConfigurationManager.AppSettings("OrlandoPassword"))

                _OrlandoGateway = New OrlandoBO.OrlandoBO(_OrlandoSystemSettings)

                ProcessOpenInvoiceSAPData()

                'ProcessOpenInvoiceDataITPR()
            End If

        Else

            _OrlandoSystemSettings = New Dictionary(Of String, String)

            _OrlandoSystemSettings.Add("OrlandoGateway", System.Configuration.ConfigurationManager.AppSettings("OrlandoGateway"))
            _OrlandoSystemSettings.Add("OrlandoSoapService", System.Configuration.ConfigurationManager.AppSettings("OrlandoSoapService"))
            _OrlandoSystemSettings.Add("OrlandoUserName", System.Configuration.ConfigurationManager.AppSettings("OrlandoUserName"))
            _OrlandoSystemSettings.Add("OrlandoPassword", System.Configuration.ConfigurationManager.AppSettings("OrlandoPassword"))

            _OrlandoGateway = New OrlandoBO.OrlandoBO(_OrlandoSystemSettings)

            Dim _frm As New Form1
            Application.EnableVisualStyles()
            Application.Run(_frm)

        End If

        'Else


        '_OrlandoSystemSettings = New Dictionary(Of String, String)

        '_OrlandoSystemSettings.Add("OrlandoGateway", System.Configuration.ConfigurationManager.AppSettings("OrlandoGateway"))
        '_OrlandoSystemSettings.Add("OrlandoSoapService", System.Configuration.ConfigurationManager.AppSettings("OrlandoSoapService"))
        '_OrlandoSystemSettings.Add("OrlandoUserName", System.Configuration.ConfigurationManager.AppSettings("OrlandoUserName"))
        '_OrlandoSystemSettings.Add("OrlandoPassword", System.Configuration.ConfigurationManager.AppSettings("OrlandoPassword"))

        '_OrlandoGateway = New OrlandoBO.OrlandoBO(_OrlandoSystemSettings)

        'Dim _frm As New Form1
        'Application.EnableVisualStyles()
        'Application.Run(_frm)

        'End If


        LogMessage("Application Closing...")

        Environment.ExitCode = 0

    End Sub

#Region "Main process"

    Sub RunProcess()

        ' first do the staffing orders
        GetOrderRequestsForIntegration()

        ' get the applicant records for placments
        GetSuccessfulApplicantsIntegration()

        ' get the placements
        GetPlacementsForIntegration()

        ' get the bookings
        GetBookingsIntegration()

        ' get the timesheets
        GetTimesheetsIntegration()

        ' get the timesheets
        GetInvoicesIntegration()


    End Sub

    Function GetOrderRequestsForIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetOrderRequestsForIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetOrderRequests", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from GetOrderRequestsForIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("Vacancy_SK"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWVacancy")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetOrderRequestForIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetOrderRequestForIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetOrderRequestsForIntegration: Completed...")

        Return retval

    End Function

    Function GetPlacementsForIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetPlacementsForIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetPlacements", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from spGetPlacementsForIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("Vacancy_SK"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWPlacement")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetPlacementsForIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetPlacementsForIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetPlacementsForIntegration: Completed...")

        Return retval

    End Function

    Function GetBookingsIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetBookingsIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetBookings", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from GetBookingsIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("Vacancy_SK"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWBooking")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetBookingsIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetBookingsIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetBookingsIntegration: Completed...")

        Return retval

    End Function

    Function GetSuccessfulApplicantsIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetSuccessfulApplicantsIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetSuccessfulApplicants", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from GetSuccessfulApplicantsIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("Vacancy_SK"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWSuccessfulApplcnt")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetSuccessfulApplicantsIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetSuccessfulApplicantsIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetSuccessfulApplicantsIntegration: Completed...")

        Return retval

    End Function

    Function GetTimesheetsIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetTimesheetsIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetTimesheets", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from GetTimesheetsIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("TimesheetCode"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWTimesheet")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetTimesheetsIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetTimesheetsIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetTimesheetsIntegration: Completed...")

        Return retval

    End Function

    Function GetInvoicesIntegration() As Long

        Dim retval As Long = 0

        LogMessage("GetInvoicesIntegration: Starting...")

        Try

            ' get the order requests 
            Dim _cmd As SqlClient.SqlCommand = GetSqlCommand(_DataWarehouseConnection, "spIntegration_GetInvoices", CommandType.StoredProcedure)
            Dim ds As DataSet = GetDataSet(_cmd)

            LogMessage(String.Format("Rows Returned from GetInvoicesIntegration: {0}", ds.Tables(0).Rows.Count.ToString))

            If ds.Tables(0).Rows.Count > 0 Then
                ' lets send it to integration
                Dim _cmd2 As SqlClient.SqlCommand = GetSqlCommand(_IntegrationConnection, "InsertOrlandoIntegrationEvent", CommandType.StoredProcedure)

                For Each _Row In ds.Tables(0).Rows
                    Try

                        _cmd2.Parameters.Clear()
                        _cmd2.Parameters.AddWithValue("@EntityID", _Row("InvoiceCode"))
                        _cmd2.Parameters.AddWithValue("@EntityName", "DWInvoice")

                        ExecuteProc(_cmd2)

                        retval += 1

                    Catch ex As Exception

                        LogMessage(String.Format("Exception thrown in GetInvoicesIntegration while adding row.  Message: {0}", ex.Message))

                    End Try
                Next

            End If

        Catch ex As Exception

            LogMessage(String.Format("Exception thrown in GetInvoicesIntegration.  Message: {0}", ex.Message))

        End Try

        LogMessage("GetInvoicesIntegration: Completed...")

        Return retval

    End Function


#End Region

#Region "DSO"

    Sub ProcessOpenInvoiceData()

        LogMessage("ProcessOpenInvoiceData: Starting...")


        ' get the data
        Dim cmd As New SqlClient.SqlCommand("spGetOpenInvoices", _DataWarehouseConnection)
        Dim ds As DataSet = GetDataSet(cmd)

        LogMessage(String.Format("Rows to process {0}", ds.Tables(0).Rows.Count.ToString))

        Dim _ClientAgencyCode As String = String.Empty
        Dim _StaffingSupplierOrgUnitID As String = String.Empty
        Dim _UserAreaLabel As String = String.Empty
        Dim _UserAreaServiceConcept As String = String.Empty

        Dim _Counter As Integer = 0
        Dim _Counter2 As Integer = 0

        ' the data set has the following fileds 
        '            ClientCode	ClientAgencyCode	StaffingSupplierOrgUnitID	UserAreaLabel	UserAreaServiceConcept	
        '           004283	    854283	            85	                        Randstad	    Staffing	

        ' so we loop for when the ClientCode,StaffingSupplier,UserAreaLabel and  UserAreasErviceConcept are the same... 

        For Each _Row As DataRow In ds.Tables(0).Rows
            If (_ClientAgencyCode <> _Row("ClientAgencyCode").ToString Or _StaffingSupplierOrgUnitID <> _Row("StaffingSupplierOrgUnitID").ToString _
                Or _UserAreaLabel <> _Row("UserAreaLabel").ToString Or _UserAreaServiceConcept <> _Row("UserAreaServiceConcept").ToString) Then
                ' we have a new prcess to call
                _ClientAgencyCode = _Row("ClientAgencyCode").ToString
                _StaffingSupplierOrgUnitID = _Row("StaffingSupplierOrgUnitID").ToString
                _UserAreaLabel = _Row("UserAreaLabel").ToString
                _UserAreaServiceConcept = _Row("UserAreaServiceConcept").ToString

                Dim dsfilter As String = String.Format("ClientAgencyCode='{0}' and StaffingSupplierOrgUnitID = '{1}' and " &
                                                        "UserAreaLabel = '{2}' and UserAreaServiceConcept = '{3}'",
                                                        _ClientAgencyCode, _StaffingSupplierOrgUnitID, _UserAreaLabel, _UserAreaServiceConcept)

                Dim _Rows As DataRow() = ds.Tables(0).Select(dsfilter)

                Try
                    _OrlandoGateway.ProcessOpenInvoiceToOrlando(_Rows)
                Catch ex As Exception
                    LogMessage(String.Format("Error in ProcessOpenInvoiceToOrlando. {0}", ex.Message))
                End Try

                _Counter += 1

                ' record a process record
                If _Counter Mod 100 = 0 Then
                    LogMessage(String.Format("Progress... Files Uploaded: {0}. Records Proceessed: {1}", _Counter.ToString, (_Counter2 + _Counter).ToString))
                End If


            Else
                ' they must be the same!!!
                ' so do nothing
                _Counter2 += 1


            End If

        Next

        LogMessage(String.Format("Total Files Uploaded: {0}, Total Rows Processed: {1}  ", _Counter.ToString, (_Counter + _Counter2).ToString))

        LogMessage("ProcessOpenInvoiceData: Ended..")


    End Sub

    Sub ProcessOpenInvoiceDataITPR()

        LogMessage("ProcessOpenInvoiceDataITPR: Starting...")


        ' get the data
        Dim cmd As New SqlClient.SqlCommand("spGetITPROpenInvoiceDetails", _DataWarehouseConnection)
        cmd.CommandTimeout = 120
        Dim ds As DataSet = GetDataSet(cmd)

        LogMessage(String.Format("Rows to process {0}", ds.Tables(0).Rows.Count.ToString))

        Dim _Counter As Integer = 0
        'Dim _Counter2 As Integer = 0

        Dim ListOfFilters As New List(Of String)

        Dim s3Uploader As New AmazonUploader()


        'AU  DOPL
        'AU  HREXL
        'AU  SKTAU
        'AU  SSSAA
        'HK  RHKL
        'My  MAFOI
        'My  SAMSB
        'NZ  HCNZ
        'NZ  SKTNZ
        'SG  SBSPL

        ' this can come from a db setting or alternative option
        'Dim filterList() = "SSSAA,HCNZ,SBSPL,RHKL,SAMSB".Split(",")

        'For Each filter As String In filterList
        '    ListOfFilters.Add(filter)
        'Next
        Dim filterList() = System.Configuration.ConfigurationManager.AppSettings("ITPR_DatabaseList").Split(",")
        ' Dim filterList() = "AU,NZ,MY".Split(",")  ' SG HK.. to be added
        ' Dim filterList() = "HK".Split(",")  ' SG to be added

        For Each filter As String In filterList
            ListOfFilters.Add(filter)
        Next

        For Each filter As String In ListOfFilters
            Try

                Dim dsfilter As String = String.Format("Database_ID='{0}'", filter)
                'Dim dsfilter As String = String.Format("Country_Code='{0}'", filter)

                Dim _Rows As DataRow() = ds.Tables(0).Select(dsfilter)
                Dim SupplierID As String = _OrlandoGateway.ConvertCountryToSupplierID(_Rows(0).Item("CountryDesc"))
                Dim s3Bucket As String = System.Configuration.ConfigurationManager.AppSettings("s3BucketName")
                Dim tempDirectory As String = Path.GetTempPath

                Dim xmlOpenInvoices As String = _OrlandoGateway.ProcessITPROpenInvoiceToOrlando(_Rows)


                ' ok so new we need to save the file
                Dim xmlFileName As String = String.Format("{0}{1}_oia_{2}_1.xml", tempDirectory, SupplierID, DateAndTime.Now.ToString("yyyyMMddHHmmss"))
                Dim zipFileName As String = String.Format("{0}{1}_oia_{2}.zip", tempDirectory, SupplierID, DateAndTime.Now.ToString("yyyyMMddHHmmss"))


                Dim file As System.IO.StreamWriter
                file = My.Computer.FileSystem.OpenTextFileWriter(xmlFileName, False)
                file.Write(xmlOpenInvoices)
                file.Close()


                ' now zip the files
                ''Dim fileToCompress As FileInfo = New FileInfo(xmlFileName)
                '//Dim originalFileStream() As Byte = System.IO.File.ReadAllBytes(xmlFileName)

                '        1.5.4.1 Naming convention of the archive file
                'Naming convention for archive file: {SupplierId}_oia_{DateTime}.{Extension}
                '{SupplierId}: Supplier Id, for eg. “DE_001”
                '{DateTime}: date and time when the file is created with format “YYYYMMDDHHMISS”, eg. “20170923053757”
                '{Extension}: zip, gz or tar.gz
                'File name example: DE_001_oia_20170923053757.tar.gz
                '// Dim compressedFileName As String = String.Format("{0}.gz", xmlFileName)  '.Replace(".xml", ".gz")

                'Using compressedFileStream As FileStream = System.IO.File.Create(compressedFileName)
                '    Using compressionStream As New System.IO.Compression.GZipStream(compressedFileStream, System.IO.Compression.CompressionMode.Compress)
                '        compressionStream.Write(originalFileStream, 0, originalFileStream.Length)
                '        compressionStream.Close()
                '        compressedFileStream.Close()
                '    End Using
                'End Using


                Dim compressedFileStream As FileStream = System.IO.File.Create(zipFileName)
                Dim zipStream = New ZipOutputStream(compressedFileStream)
                Dim fi As New FileInfo(xmlFileName)

                Dim entryName As String = fi.Name '.Substring(folderOffset)  ' Makes the name in zip based on the folder
                entryName = ZipEntry.CleanName(entryName)       ' Removes drive from name and fixes slash direction
                Dim newEntry As New ZipEntry(entryName)
                newEntry.DateTime = fi.LastWriteTime            ' Note the zip format stores 2 second granularity

                ' Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                '   newEntry.AESKeySize = 256;

                ' To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                ' you need to do one of the following: Specify UseZip64.Off, or set the Size.
                ' If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                ' but the zip will be in Zip64 format which not all utilities can understand.
                '   zipStream.UseZip64 = UseZip64.Off;
                newEntry.Size = fi.Length

                zipStream.PutNextEntry(newEntry)

                ' Zip the file in buffered chunks
                ' the "using" will close the stream even if an exception occurs
                Dim buffer As Byte() = New Byte(4095) {}
                Using streamReader As FileStream = System.IO.File.OpenRead(xmlFileName)
                    StreamUtils.Copy(streamReader, zipStream, buffer)
                End Using
                zipStream.CloseEntry()

                zipStream.IsStreamOwner = True
                ' Makes the Close also Close the underlying stream
                zipStream.Close()




                ' and somewhere here is the S3 update
                s3Uploader.sendMyFileToS3(zipFileName, String.Format("{0}/{1}/upload", s3Bucket, SupplierID), zipFileName.Replace(Path.GetTempPath, ""))


                LogMessage(String.Format("Progress... File Uploaded: {0}.", zipFileName))
                _Counter += 1
            Catch ex As Exception
                LogMessage(String.Format("Error in ProcessOpenInvoiceDataITPR. {0}", ex.Message))
            End Try

            'LogMessage(String.Format("Progress... Files Uploaded: {0}. Records Proceessed: {1}", _Counter.ToString, (_Counter2 + _Counter).ToString))
            '       End If
        Next


        LogMessage(String.Format("Total Files Uploaded: {0}.", _Counter.ToString))


        LogMessage("ProcessOpenInvoiceDataITPR: Ended..")


    End Sub

    Sub ProcessOpenInvoiceSAPData()

        LogMessage("ProcessOpenInvoiceSAPData: Starting...")

        ' firstly we need to transfer the data from the SAP transfer directory
        Dim SAPDataTransferDir As String = System.Configuration.ConfigurationManager.AppSettings("SAPImportDirectory")
        Dim SAPFilePattern As String = System.Configuration.ConfigurationManager.AppSettings("SAPFilePattern")

        '// move the file from the location to database
        TransferSAPFileData(SAPDataTransferDir, SAPFilePattern)

        ' ok so new we can run the query to return the dataset
        '// for testing
        'Dim _test As New SqlConnection(My.Settings.DatawWarehouseConnTest)

        ' get the data
        Dim cmd As New SqlClient.SqlCommand("spGetSAPOpenInvoiceDetails", _DataWarehouseConnection)
        cmd.CommandTimeout = 120
        Dim ds As DataSet = GetDataSet(cmd)

        Dim filterList() = System.Configuration.ConfigurationManager.AppSettings("SAP_DatabaseList").Split(",")
        Dim ListOfFilters As New List(Of String)
        For Each filter As String In filterList
            ListOfFilters.Add(filter)
        Next

        Dim supplierList() = System.Configuration.ConfigurationManager.AppSettings("SAP_SupplierList").Split(",")
        Dim ListOfSuppliers As New List(Of String)
        For Each supplier As String In supplierList
            ListOfSuppliers.Add(supplier)
        Next

        LogMessage(String.Format("Total Rows to process {0}", ds.Tables(0).Rows.Count.ToString))

        ProcessOpenInvoiceDataVersion2(ds, ListOfFilters, ListOfSuppliers)

        '// and do a final cleanup
        SAPFileDataCleanup(SAPDataTransferDir)

        LogMessage("ProcessOpenInvoiceSAPData: Ended")


    End Sub



    Sub ProcessOpenInvoiceITPRData()

        LogMessage("ProcessOpenInvoiceITPRData: Starting...")

        ' get the data
        Dim cmd As New SqlClient.SqlCommand("spGetITPROpenInvoiceDetails", _DataWarehouseConnection)
        cmd.CommandTimeout = 120
        Dim ds As DataSet = GetDataSet(cmd)

        LogMessage(String.Format("Total Rows to process {0}", ds.Tables(0).Rows.Count.ToString))

        Dim filterList() = System.Configuration.ConfigurationManager.AppSettings("ITPR_DatabaseList").Split(",")
        Dim ListOfFilters As New List(Of String)
        For Each filter As String In filterList
            ListOfFilters.Add(filter)
        Next

        Dim supplierList() = System.Configuration.ConfigurationManager.AppSettings("ITPR_SupplierList").Split(",")
        Dim ListOfSuppliers As New List(Of String)
        For Each supplier As String In supplierList
            ListOfSuppliers.Add(supplier)
        Next

        ProcessOpenInvoiceDataVersion2(ds, ListOfFilters, ListOfSuppliers)


        LogMessage("ProcessOpenInvoiceITPRData: Ended")


    End Sub

    Sub ProcessOpenInvoiceDataVersion2(ds As DataSet, ListOfFilters As List(Of String), ListOfSupplierIDs As List(Of String))

        LogMessage("ProcessOpenInvoiceDataVersion2: Starting...")

        Dim _Counter As Integer = 0
        'Dim ListOfFilters As New List(Of String)
        Dim s3Uploader As New AmazonUploader()

        For icounter = 0 To ListOfFilters.Count - 1 Step 1
            Try

                Dim dsfilter As String = String.Format("Database_ID='{0}'", ListOfFilters(icounter))

                Dim _Rows As DataRow() = ds.Tables(0).Select(dsfilter)
                LogMessage(String.Format("ProcessOpenInvoiceDataVersion2: Filter:{0}, Rows: {1}", ListOfFilters(icounter), _Rows.Count.ToString()))

                Dim s3Bucket As String = System.Configuration.ConfigurationManager.AppSettings("s3BucketName")
                Dim tempDirectory As String = Path.GetTempPath

                Dim SupplierID As String = ListOfSupplierIDs(icounter)

                Dim OpenInvoiceData As OrlandoBO.OpenInvoiceVersion2 = _OrlandoGateway.ProcessOpenInvoiceToOrlandoVersion2(_Rows, SupplierID)

                ' i need to do this for testing 
                Dim S3FolderID As String = "AU_001"


                ' ok so new we need to save the file(s)
                Dim csvLegalEntityFileName As String = String.Format("{0}{1}_oia2_legal_entity_{2}.csv", tempDirectory, SupplierID, OpenInvoiceData.FileNumber)
                Dim csvCustomersFileName As String = String.Format("{0}{1}_oia2_customer_{2}.csv", tempDirectory, SupplierID, OpenInvoiceData.FileNumber)
                Dim csvOpenInvoicesFileName As String = String.Format("{0}{1}_oia2_open_invoice_{2}_1.csv", tempDirectory, SupplierID, OpenInvoiceData.FileNumber)
                Dim csvSummaryFileName As String = String.Format("{0}{1}_oia2_summary_{2}.csv", tempDirectory, SupplierID, OpenInvoiceData.FileNumber)

                Dim zipFileName As String = String.Format("{0}{1}_oia2_{2}.zip", tempDirectory, SupplierID, OpenInvoiceData.FileNumber)

                ' lets write out the legal entity file
                Dim fileLegalEntity As System.IO.StreamWriter
                fileLegalEntity = My.Computer.FileSystem.OpenTextFileWriter(csvLegalEntityFileName, True)
                fileLegalEntity.WriteLine(OpenInvoiceData.LegalEntity.GetHeaderRecord)
                fileLegalEntity.WriteLine(OpenInvoiceData.LegalEntity.GetDelimetedRecord)
                fileLegalEntity.Close()

                ' lets write out the customer file
                Dim fileCustomer As System.IO.StreamWriter
                fileCustomer = My.Computer.FileSystem.OpenTextFileWriter(csvCustomersFileName, True)
                fileCustomer.WriteLine(OpenInvoiceData.Customers(0).GetHeaderRecord)
                For Each customer As OrlandoBO.OpenInvoiceVersion2Customer In OpenInvoiceData.Customers
                    fileCustomer.WriteLine(customer.GetDelimetedRecord)
                Next
                fileCustomer.Close()

                ' lets write out the open invoice file
                Dim fileOpenInvoice As System.IO.StreamWriter
                fileOpenInvoice = My.Computer.FileSystem.OpenTextFileWriter(csvOpenInvoicesFileName, True)
                fileOpenInvoice.WriteLine(OpenInvoiceData.OpenInvoices(0).GetHeaderRecord)
                For Each openInvoice As OrlandoBO.OpenInvoiceVersion2OpenInvoice In OpenInvoiceData.OpenInvoices
                    fileOpenInvoice.WriteLine(openInvoice.GetDelimetedRecord)
                Next
                fileOpenInvoice.Close()

                ' lets write out the summary file
                Dim fileSummary As System.IO.StreamWriter
                fileSummary = My.Computer.FileSystem.OpenTextFileWriter(csvSummaryFileName, True)
                fileSummary.WriteLine(OpenInvoiceData.Summary(0).GetHeaderRecord)
                For Each summaryrecord As OrlandoBO.OpenInvoiceVersion2Summary In OpenInvoiceData.Summary
                    fileSummary.WriteLine(summaryrecord.GetDelimetedRecord)
                Next
                fileSummary.Close()

                ' now zip the files
                ''Dim fileToCompress As FileInfo = New FileInfo(xmlFileName)
                '//Dim originalFileStream() As Byte = System.IO.File.ReadAllBytes(xmlFileName)

                '        1.5.4.1 Naming convention of the archive file
                'Naming convention for archive file: {SupplierId}_oia_{DateTime}.{Extension}
                '{SupplierId}: Supplier Id, for eg. “DE_001”
                '{DateTime}: date and time when the file is created with format “YYYYMMDDHHMISS”, eg. “20170923053757”
                '{Extension}: zip, gz or tar.gz
                'File name example: DE_001_oia_20170923053757.tar.gz
                '// Dim compressedFileName As String = String.Format("{0}.gz", xmlFileName)  '.Replace(".xml", ".gz")

                'Using compressedFileStream As FileStream = System.IO.File.Create(compressedFileName)
                '    Using compressionStream As New System.IO.Compression.GZipStream(compressedFileStream, System.IO.Compression.CompressionMode.Compress)
                '        compressionStream.Write(originalFileStream, 0, originalFileStream.Length)
                '        compressionStream.Close()
                '        compressedFileStream.Close()
                '    End Using
                'End Using


                Dim compressedFileStream As FileStream = System.IO.File.Create(zipFileName)
                Dim zipStream = New ZipOutputStream(compressedFileStream)

                ' Specifying the AESKeySize triggers AES encryption. Allowable values are 0 (off), 128 or 256.
                '   newEntry.AESKeySize = 256;

                ' To permit the zip to be unpacked by built-in extractor in WinXP and Server2003, WinZip 8, Java, and other older code,
                ' you need to do one of the following: Specify UseZip64.Off, or set the Size.
                ' If the file may be bigger than 4GB, or you do not need WinXP built-in compatibility, you do not need either,
                ' but the zip will be in Zip64 format which not all utilities can understand.
                '   zipStream.UseZip64 = UseZip64.Off;

                ' add the legal entity file
                Dim fiLegalEntity As New FileInfo(csvLegalEntityFileName)
                Dim entryNameLegalEntity As String = fiLegalEntity.Name '.Substring(folderOffset)  ' Makes the name in zip based on the folder
                entryNameLegalEntity = ZipEntry.CleanName(entryNameLegalEntity)       ' Removes drive from name and fixes slash direction
                Dim newEntryLegalEntity As New ZipEntry(entryNameLegalEntity)
                newEntryLegalEntity.DateTime = fiLegalEntity.LastWriteTime            ' Note the zip format stores 2 second granularity
                newEntryLegalEntity.Size = fiLegalEntity.Length
                zipStream.PutNextEntry(newEntryLegalEntity)
                Dim bufferLegaEntity As Byte() = New Byte(4095) {}
                Using streamReader As FileStream = System.IO.File.OpenRead(csvLegalEntityFileName)
                    StreamUtils.Copy(streamReader, zipStream, bufferLegaEntity)
                End Using
                zipStream.CloseEntry()

                ' add the customers file
                Dim fiCustomers As New FileInfo(csvCustomersFileName)
                Dim entryNameCustomers As String = fiCustomers.Name '.Substring(folderOffset)  ' Makes the name in zip based on the folder
                entryNameCustomers = ZipEntry.CleanName(entryNameCustomers)       ' Removes drive from name and fixes slash direction
                Dim newEntryCustomers As New ZipEntry(entryNameCustomers)
                newEntryCustomers.DateTime = fiCustomers.LastWriteTime            ' Note the zip format stores 2 second granularity
                newEntryCustomers.Size = fiCustomers.Length
                zipStream.PutNextEntry(newEntryCustomers)
                Dim buffer As Byte() = New Byte(4095) {}
                Using streamReader As FileStream = System.IO.File.OpenRead(csvCustomersFileName)
                    StreamUtils.Copy(streamReader, zipStream, buffer)
                End Using
                zipStream.CloseEntry()

                ' add the openinvoice file
                Dim fiOpenInvoices As New FileInfo(csvOpenInvoicesFileName)
                Dim entryNameOpenInvoices As String = fiOpenInvoices.Name '.Substring(folderOffset)  ' Makes the name in zip based on the folder
                entryNameOpenInvoices = ZipEntry.CleanName(entryNameOpenInvoices)       ' Removes drive from name and fixes slash direction
                Dim newEntryOpenInvoices As New ZipEntry(entryNameOpenInvoices)
                newEntryOpenInvoices.DateTime = fiOpenInvoices.LastWriteTime            ' Note the zip format stores 2 second granularity
                newEntryOpenInvoices.Size = fiOpenInvoices.Length
                zipStream.PutNextEntry(newEntryOpenInvoices)
                Dim bufferOpenInvoices As Byte() = New Byte(4095) {}
                Using streamReader As FileStream = System.IO.File.OpenRead(csvOpenInvoicesFileName)
                    StreamUtils.Copy(streamReader, zipStream, bufferOpenInvoices)
                End Using
                zipStream.CloseEntry()

                ' add the summary file
                Dim fiSummary As New FileInfo(csvSummaryFileName)
                Dim entryNameSummary As String = fiSummary.Name '.Substring(folderOffset)  ' Makes the name in zip based on the folder
                entryNameSummary = ZipEntry.CleanName(entryNameSummary)       ' Removes drive from name and fixes slash direction
                Dim newEntrySummary As New ZipEntry(entryNameSummary)
                newEntrySummary.DateTime = fiSummary.LastWriteTime            ' Note the zip format stores 2 second granularity
                newEntrySummary.Size = fiSummary.Length
                zipStream.PutNextEntry(newEntrySummary)
                Dim bufferSummary As Byte() = New Byte(4095) {}
                Using streamReader As FileStream = System.IO.File.OpenRead(csvSummaryFileName)
                    StreamUtils.Copy(streamReader, zipStream, bufferSummary)
                End Using

                zipStream.CloseEntry()
                zipStream.IsStreamOwner = True
                ' Makes the Close also Close the underlying stream
                zipStream.Close()

                ' and somewhere here is the S3 update
                s3Uploader.sendMyFileToS3(zipFileName, String.Format("{0}/{1}/upload", s3Bucket, S3FolderID), zipFileName.Replace(Path.GetTempPath, ""))


                LogMessage(String.Format("Progress... File Uploaded: {0}.", zipFileName))
                _Counter += 1
            Catch ex As Exception
                LogMessage(String.Format("Error in ProcessOpenInvoiceDataVersion2. {0}", ex.Message))
            End Try

            'LogMessage(String.Format("Progress... Files Uploaded: {0}. Records Proceessed: {1}", _Counter.ToString, (_Counter2 + _Counter).ToString))
            '       End If
        Next


        LogMessage(String.Format("Total Files Uploaded: {0}.", _Counter.ToString))


        LogMessage("ProcessOpenInvoiceDataVersion2: Ended..")


    End Sub

    Private Sub TransferSAPFileData(SAPDataTransferDir As String, SAPFilePattern As String)

        LogMessage("TransferSAPFileData: Starting...")

        Dim x As New SAPConnector.SAPConnection

        ' // now lets get all the file in this directory that have the prefix
        Dim SAPFileList As String() = System.IO.Directory.GetFiles(SAPDataTransferDir, SAPFilePattern)
        System.Array.Sort(SAPFileList)

        For Each SAPFilename As String In SAPFileList
            ' i want to put it into the database in Orlando... for subsequent processing
            Dim ds As DataSet = x.GetSAPCSVData(SAPFilename)

            'validation
            Dim totalRows As Integer = ds.Tables(0).Rows.Count
            Dim counterRows As Integer = 0

            '// for testing
            'Dim _test As New SqlConnection(My.Settings.DatawWarehouseConnTest)

            '// now if the process failed one day we have the situation of possible muliple files in the directory... 
            ' so Im going to process it in the same way
            ' and if multiple it will just be removed and replaced
            Dim cmdDelete As SqlCommand = New SqlCommand("DELETE FROM dbo.SAP_OpenInvoiceData", _DataWarehouseConnection)
            cmdDelete.CommandType = CommandType.Text
            ExecuteProc(cmdDelete)

            For Each dt As DataTable In ds.Tables


                Using cmd As SqlCommand = New SqlCommand("PUT_SAP_OpenInvoiceData", _DataWarehouseConnection)
                    cmd.CommandType = CommandType.StoredProcedure

                    For Each dr As DataRow In dt.Rows
                        cmd.Parameters.Clear()

                        cmd.Parameters.AddWithValue("@SAPFileName", SAPFilename.Replace(SAPDataTransferDir, String.Empty))
                        cmd.Parameters.AddWithValue("@LegalEntityCode", dr("LegalEntityCode").ToString)
                        cmd.Parameters.AddWithValue("@LegalEntityName", dr("LegalEntityName").ToString)
                        cmd.Parameters.AddWithValue("@CustomerId", dr("CustomerId").ToString)
                        cmd.Parameters.AddWithValue("@CustomerName", dr("CustomerName").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_CountryCode", dr("CustomerBillingAddress_CountryCode").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_StateProvince", dr("CustomerBillingAddress_StateProvince").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_PostalCode", dr("CustomerBillingAddress_PostalCode").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_Municipality", dr("CustomerBillingAddress_Municipality").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_BuildingNumber", dr("CustomerBillingAddress_BuildingNumber").ToString)
                        cmd.Parameters.AddWithValue("@CustomerBillingAddress_StreetAddress", dr("CustomerBillingAddress_StreetAddress").ToString)
                        cmd.Parameters.AddWithValue("@CustomerTelephoneNumber", dr("CustomerTelephoneNumber").ToString)
                        cmd.Parameters.AddWithValue("@CustomerMobileTelephoneNumber", dr("CustomerMobileTelephoneNumber").ToString)
                        cmd.Parameters.AddWithValue("@CustomerEmailAddress", dr("CustomerEmailAddress").ToString)
                        cmd.Parameters.AddWithValue("@LocalClientIdentifier", dr("LocalClientIdentifier").ToString)
                        cmd.Parameters.AddWithValue("@CustomerDetailNote", dr("CustomerDetailNote").ToString)
                        cmd.Parameters.AddWithValue("@InvoiceId", dr("InvoiceId").ToString)
                        cmd.Parameters.AddWithValue("@DocumentDate", DateTime.Parse(dr("DocumentDate").ToString))
                        cmd.Parameters.AddWithValue("@OriginalOpenAmount", Convert.ToDouble(dr("OriginalOpenAmount").ToString.Replace(",", ".")))
                        cmd.Parameters.AddWithValue("@AttributeCurrencyOrg", dr("AttributeCurrencyOrg").ToString)
                        cmd.Parameters.AddWithValue("@OpenInvoiceAmount", Convert.ToDouble(dr("OpenInvoiceAmount").ToString.Replace(",", ".")))
                        cmd.Parameters.AddWithValue("@AttributeCurrencyOpen", dr("AttributeCurrencyOpen").ToString)
                        cmd.Parameters.AddWithValue("@DueDate", DateTime.Parse(dr("DueDate").ToString))
                        cmd.Parameters.AddWithValue("@DocumentType", dr("DocumentType").ToString)

                        Try
                            counterRows += ExecuteProc(cmd)
                        Catch ex As Exception
                            ' do nothing we keep going
                        End Try

                    Next  'next row

                End Using

            Next  ' next table

            If (totalRows = counterRows) Then
                ' then we can delete the file
                LogMessage(String.Format("TransferSAPFileData:{0} Rows:{1}", SAPFilename.Replace(SAPDataTransferDir, String.Empty), totalRows.ToString))
                System.IO.File.Move(SAPFilename, GetNextFileName(SAPFilename.Replace("OI", "Uploaded")))
            Else
                LogMessage(String.Format("TransferSAPFileData Error:{0} Rows:{1} Expected:{2}", SAPFilename.Replace(SAPDataTransferDir, String.Empty), totalRows.ToString, counterRows.ToString))
                System.IO.File.Move(SAPFilename, GetNextFileName(SAPFilename.Replace("OI", "Exception"))) ' SAPFilename.Replace("OI", "Exception"))
            End If

                ' and archive the current set of data
                Dim cmdArchive As SqlCommand = New SqlCommand("spDatawarehouse_GetSAPOpenInvoiceDetails", _DataWarehouseConnection)
                cmdArchive.CommandType = CommandType.StoredProcedure
                ExecuteProc(cmdArchive)


        Next '  file In directory

        LogMessage("TransferSAPFileData: Ended")


    End Sub

    Private Function GetNextFileName(NewFileName As String) As String
        Dim retval As String = NewFileName

        'If (SAPFileList.Count > 0) Then
        '    retval = NewFileName.Replace(".", String.Format("{0}.", SAPFileList.Count.ToString))
        'End If
        'Return retval

        Dim extension As String = Path.GetExtension(NewFileName)
        Dim pathName As String = Path.GetDirectoryName(NewFileName)
        Dim fileNameOnly As String = Path.GetFileNameWithoutExtension(NewFileName)
        Dim i As Integer = 0

        '// If the file exists, keep trying until it doesn't
        Do While (File.Exists(Path.Combine(pathName, retval)))
            i += 1
            retval = String.Format("{0}({1}){2}", fileNameOnly, i, extension)
        Loop

        Return Path.Combine(pathName, retval)


    End Function




    Private Sub SAPFileDataCleanup(SAPDataTransferDir As String)

        ' we want to keep only the last 14 days
        Dim dirFileList As String() = System.IO.Directory.GetFiles(SAPDataTransferDir)

        For Each fileName As String In dirFileList
            If FileSystem.FileDateTime(fileName) < DateAndTime.Today.AddDays(-7) Then
                System.IO.File.Delete(fileName)
            End If
        Next

    End Sub

#End Region

#Region "logging"

    Sub LogException(LogEx As Exception)

        LogException(LogEx, 5)

    End Sub

    Sub LogException(LogEx As Exception, Severity As Integer)

        ' first lets see if there any search items to be checked
        Dim cmd As SqlClient.SqlCommand = _DataWarehouseConnection.CreateCommand()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "[sp_LogMessage]"

        cmd.Parameters.AddWithValue("@application", "Orlando Integration")
        cmd.Parameters.AddWithValue("@severity", Severity)
        cmd.Parameters.AddWithValue("@message", LogEx.Message)
        If Not LogEx.InnerException Is Nothing Then
            cmd.Parameters.AddWithValue("@additional_data", LogEx.InnerException.Message)
        Else
            cmd.Parameters.AddWithValue("@additional_data", "")
        End If

        Try

            cmd.ExecuteNonQuery()

        Catch ex As Exception

            ' we cant even log an error... just clear it and move on
            ex = Nothing

        End Try

    End Sub

    Sub LogMessage(ByVal Message As String)

        LogMessage(Message, _Logging)

    End Sub

    Sub LogMessage(ByVal Message As String, ByVal OverrideLoggingState As Boolean)

        If OverrideLoggingState Then
            Dim _Filename As String = String.Concat(My.Application.Info.DirectoryPath, "\OrlandoProcessor.Log")

            If IsNothing(myLogListener) Then
                myLogListener = New TextWriterTraceListener(_Filename, "myListener")
            Else
                If Not FileIO.FileSystem.FileExists(_Filename) Then
                    myLogListener = New TextWriterTraceListener(_Filename, "myListener")
                End If
            End If

            myLogListener.WriteLine(String.Concat(String.Format(DateAndTime.Now, "dd/MM/yyyy HH:MM:SS TT"), ",", Message))

            ' You must close or flush the trace listener to empty the output buffer.
            myLogListener.Flush()
        End If

    End Sub

#End Region

End Module
