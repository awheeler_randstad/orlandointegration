﻿using SharpSapRfc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;


namespace SAPConnector
{
    public class SAPConnection : SAPConnectClass
    {

        public override SapRfcConnection GetConnection(string Connection)
        {
            throw new NotImplementedException();
        }

        public List<ZCustomer> GetSAPData(string Connection)
        {

            List<ZCustomer> retval = new List<ZCustomer>();
            using (SapRfcConnection conn = GetConnection(Connection))
            {
                try
                {
                    var result = conn.ExecuteFunction("Z_SSRT_GET_ALL_CUSTOMERS");
                    var customers = result.GetTable<ZCustomer>("t_customers");
                    foreach (ZCustomer cust in customers)
                    {
                        retval.Add(cust); 
                        //Do anything.
                    }

                }
                catch (Exception ex)
                {
                    ex = null;
                    // handle it 
                    //cleif (ex.Key == "CARR_NOT_FOUND")
                    //    Console.WriteLine("Não foi encontrado uma Cia. Aérea com o código informado.");
                }
            }

            return retval;

        }

        public DataSet GetSAPCSVData(string CSVFileName)
        {

            return GetSAPCSVData(CSVFileName, 0);
        }
        
        public DataSet GetSAPCSVData(string CSVFileName,int SplitResultIntoChunksSize)
        {

            DataSet ds = new DataSet();
            DataTable datatable = new DataTable();
            datatable.TableName = "SAPData";

            int colOriginalOpenAmount = 0;
            int colOpenInvoiceAmount =0;


            StreamReader streamreader = new StreamReader(CSVFileName); // @"\\austE:\temp\SAPData\ORLANDO_20190616_OI.csv");
            char[] delimiter = new char[] { ';' };
            string[] columnheaders = streamreader.ReadLine().Split(delimiter);
            foreach (string columnheader in columnheaders)
            {
                datatable.Columns.Add(columnheader.Replace('/', '_')); // I've added the column headers here.

                if(columnheader == "OriginalOpenAmount")
                { colOriginalOpenAmount = datatable.Columns.Count - 1; }

                if (columnheader == "OpenInvoiceAmount")
                { colOpenInvoiceAmount = datatable.Columns.Count - 1; }


            }

            while (streamreader.Peek() > 0)
            {
                DataRow datarow = datatable.NewRow();
                //datarow.ItemArray = streamreader.ReadLine().Split(delimiter);
                datarow.ItemArray = streamreader.ReadLine().Replace("\"", "").Split(delimiter);

                if(datarow.Field<string>(colOriginalOpenAmount).Contains("-"))
                {
                    datarow.SetField(colOriginalOpenAmount, string.Format("-{0}", datarow.Field<string>(colOriginalOpenAmount).Replace("-", "").Replace(",",".").Trim()));   
                }

                if (datarow.Field<string>(colOpenInvoiceAmount).Contains("-"))
                {
                    datarow.SetField(colOpenInvoiceAmount, string.Format("-{0}", datarow.Field<string>(colOpenInvoiceAmount).Replace("-", "").Replace(",", ".").Trim()));
                }


                datatable.Rows.Add(datarow);
            }

            streamreader.Close();
                 

            if(SplitResultIntoChunksSize > 0)
            {
                ds = SplitDataTable(datatable, SplitResultIntoChunksSize);
            }
            else
            {
                ds.Tables.Add(datatable); 
            }

            return ds;

        }

        private DataSet SplitDataTable(DataTable tableData, int max)
        {
            int i = 0;
            int j = 1;
            int countOfRows = tableData.Rows.Count;
            DataSet newDs = new DataSet();
            DataTable newDt = tableData.Clone();
            newDt.TableName = tableData.TableName + "_" + j;
            newDt.Clear();
            foreach (DataRow row in tableData.Rows)
            {
                DataRow newRow = newDt.NewRow();
                newRow.ItemArray = row.ItemArray;

                newDt.Rows.Add(newRow);
                i++;

                countOfRows--;

                if (i == max)
                {
                    newDs.Tables.Add(newDt);
                    j++;
                    newDt = tableData.Clone();
                    newDt.TableName = tableData.TableName + "_" + j;
                    newDt.Clear();
                    i = 0;
                }

                if (countOfRows == 0 && i < max)
                {
                    newDs.Tables.Add(newDt);
                    j++;
                    newDt = tableData.Clone();
                    newDt.TableName = tableData.TableName + "_" + j;
                    newDt.Clear();
                    i = 0;
                }
            }
            return newDs;
        }

    }

       
    public class ZCustomer
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [RfcStructureField("ACTIVE")]
        public bool IsActive { get; set; }

        [RfcStructureField("DATUM", "UZEIT")]
        public DateTime DateTime { get; set; }

        public int Age { get; set; }
    }

    public abstract class SAPConnectClass
    {
        public abstract SapRfcConnection GetConnection(string Connection);

    } 
    
        
}
