﻿'Namespace WSOrlandoGateway

'    '''<remarks/>
'    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440"), _
'     System.SerializableAttribute(), _
'     System.Diagnostics.DebuggerStepThroughAttribute(), _
'     System.ComponentModel.DesignerCategoryAttribute("code"), _
'     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True, [Namespace]:="http://ns.hr-xml.org/2007-04-15")> _
'    Partial Public Class TimeCardTypeReportedTimeOverride

'        Private idField As EntityIdType

'        Private periodStartDateField As Date

'        Private periodEndDateField As Date

'        Private itemsField() As Object

'        Private statusField As String

'        Public Property Id() As EntityIdType
'            Get
'                Return Me.idField
'            End Get
'            Set(value As EntityIdType)
'                Me.idField = value
'            End Set
'        End Property

'        '''<remarks/>
'        <System.Xml.Serialization.XmlElementAttribute()> _
'        Public Property PeriodStartDate() As String
'            Get
'                Return Me.periodStartDateField.ToString("yyyy-MM-ddZ")
'            End Get
'            Set(value As String)
'                Me.periodStartDateField = Convert.ToDateTime(value)
'            End Set
'        End Property

'        '''<remarks/>
'        <System.Xml.Serialization.XmlElementAttribute()> _
'        Public Property PeriodEndDate() As String
'            Get
'                Return Me.periodEndDateField.ToString("yyyy-MM-ddZ")
'            End Get
'            Set(value As String)
'                Me.periodEndDateField = Convert.ToDateTime(value)
'            End Set
'        End Property

'        '''<remarks/>
'        <System.Xml.Serialization.XmlElementAttribute("Allowance", GetType(TimeCardTypeReported TimeAllowance)), _
'         System.Xml.Serialization.XmlElementAttribute("TimeInterval", GetType(TimeCardTypeReportedTimeTimeInterval))> _
'        Public Property Items() As Object()
'            Get
'                Return Me.itemsField
'            End Get
'            Set(value As Object())
'                Me.itemsField = value
'            End Set
'        End Property

'        '''<remarks/>
'        <System.Xml.Serialization.XmlAttributeAttribute()> _
'        Public Property status() As String
'            Get
'                Return Me.statusField
'            End Get
'            Set(value As String)
'                Me.statusField = value
'            End Set
'        End Property
'    End Class



'    '   System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True, [Namespace]:="http://ns.hr-xml.org/2007-04-15")> _

'    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440"), _
'       System.SerializableAttribute(), _
'       System.Diagnostics.DebuggerStepThroughAttribute(), _
'       System.ComponentModel.DesignerCategoryAttribute("code"), _
'       System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
'    Partial Public Class TimeCardTypeReportedTimeTimeIntervalOverride
'        Private idField As EntityIdType

'        Private startDateField As Date

'        Private durationField As Decimal

'        Private typeField As String

'        '       Private dayAssignmentField As String

'        'Private billableField As Boolean

'        'Private billableFieldSpecified As Boolean

'        '''<remarks/>
'        Public Property Id() As EntityIdType
'            Get
'                Return Me.idField
'            End Get
'            Set(value As EntityIdType)
'                Me.idField = value
'            End Set
'        End Property

'        '''<remarks/>
'        Public Property StartDate() As String
'            Get
'                Return Me.startDateField.ToString("yyyy-MM-ddZ")
'            End Get
'            Set(value As String)
'                Me.startDateField = Convert.ToDateTime(value)
'            End Set
'        End Property

'        '''<remarks/>
'        Public Property Duration() As Decimal
'            Get
'                Return Me.durationField
'            End Get
'            Set(value As Decimal)
'                Me.durationField = value
'            End Set
'        End Property

'        '''<remarks/>
'        <System.Xml.Serialization.XmlAttributeAttribute()> _
'        Public Property type() As TimeCardTypeReportedTimeTimeIntervalType
'            Get
'                Return Me.typeField
'            End Get
'            Set(value As TimeCardTypeReportedTimeTimeIntervalType)
'                Me.typeField = value
'            End Set
'        End Property


'        ' '''<remarks/>
'        '<System.Xml.Serialization.XmlElementAttribute("RateOrAmount")> _
'        'Public Property RateOrAmount() As TimeCardTypeReportedTimeTimeIntervalRateOrAmount()
'        '    Get
'        '        Return Me.rateOrAmountField
'        '    End Get
'        '    Set(value As TimeCardTypeReportedTimeTimeIntervalRateOrAmount())
'        '        Me.rateOrAmountField = value
'        '    End Set
'        'End Property


'        ' '''<remarks/>
'        '<System.Xml.Serialization.XmlAttributeAttribute()> _
'        'Public Property dayAssignment() As String
'        '    Get
'        '        Return Me.dayAssignmentField
'        '    End Get
'        '    Set(value As String)
'        '        Me.dayAssignmentField = value
'        '    End Set
'        'End Property

'        ' '''<remarks/>
'        '<System.Xml.Serialization.XmlAttributeAttribute()> _
'        'Public Property billable() As Boolean
'        '    Get
'        '        Return Me.billableField
'        '    End Get
'        '    Set(value As Boolean)
'        '        Me.billableField = value
'        '    End Set
'        'End Property

'        ' '''<remarks/>
'        '<System.Xml.Serialization.XmlIgnoreAttribute()> _
'        'Public Property billableSpecified() As Boolean
'        '    Get
'        '        Return Me.billableFieldSpecified
'        '    End Get
'        '    Set(value As Boolean)
'        '        Me.billableFieldSpecified = value
'        '    End Set
'        'End Property
'    End Class

'    '''<remarks/>
'    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440"), _
'     System.SerializableAttribute(), _
'     System.Xml.Serialization.XmlTypeAttribute(AnonymousType:=True)> _
'    Public Enum TimeCardTypeReportedTimeTimeIntervalType

'        '''<remarks/>
'        Regular

'        '''<remarks/>
'        Overtime

'        '''<remarks/>
'        Shift
'    End Enum




'End Namespace